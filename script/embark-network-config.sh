#!/bin/sh

# embarks network config in app
# allows to start the app with fresh storage with known network config even if the remote endpoint is not available
curl https://git.duniter.org/nodes/networks/-/raw/master/gdev.json -o public/gdev.json
curl https://git.duniter.org/nodes/networks/-/raw/master/gtest.json -o public/gtest.json
curl https://git.duniter.org/nodes/networks/-/raw/master/g1.json -o public/g1.json