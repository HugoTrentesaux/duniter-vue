using Pipe

# coordinates of a star

θ = range(0, 2π, 11) .- π/2
r = [repeat([1, 0.5], 5)..., 1]

@pipe r.*exp.(1im .* θ) .* 100 |>
    reim |>
    zip(_[1], _[2]) |>
    collect |>
    map(x->Int.(round.(x)), _) |>
    join(_, " L ")