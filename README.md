# duniter-vue

Duniter Panel, application en Vue pour explorer les fonctionnalités de Duniter v2 et de l'écosystème associé.

## Développer

Installer les dépendances et lancer l'app localement en mode dev.

```sh
pnpm install
pnpm dev
```

## Mettre à jour les API

Pour mettre à jour les schémas graphql et les métadonnées du runtime.

```sh
# get schema and generate types
pnpm exec get-graphql-schema https://${SUBSQUID_DOMAIN}/v1/graphql > src/squid/schema.graphql
pnpm exec get-graphql-schema https://${DATAPOD_DOMAIN}/v1/graphql > src/datapod/schema.graphql
pnpm run generate:graphql:squid
pnpm run generate:graphql:datapod
# get metadata and use them to generate types
curl -H "Content-Type: application/json" -d '{"id":"1", "jsonrpc":"2.0", "method": "state_getMetadata", "params":[]}' http://localhost:9944 -o rsrc/metadata.json
./node_modules/.bin/polkadot-types-from-defs --endpoint ./rsrc/metadata.json --input ./interfaces --package duniter-vue
./node_modules/.bin/polkadot-types-from-chain --endpoint ./rsrc/metadata.json --output ./interfaces --package duniter-vue
```
