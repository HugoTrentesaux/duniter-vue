import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import AboutView from '@/views/AboutView.vue'
import NotFound from '@/views/NotFound.vue'
import SettingsView from '@/views/SettingsView.vue'

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      component: AboutView
    },
    {
      path: '/settings',
      name: 'settings',
      component: SettingsView
    },
    {
      path: '/g1',
      name: 'currency',
      component: () => import('@/views/CurrencyView.vue')
    },
    {
      path: '/g1/:id',
      name: 'account',
      component: () => import('@/views/AccountView.vue')
    },
    {
      path: '/wot',
      name: 'web_of_trust',
      component: () => import('@/views/WotView.vue')
    },
    {
      path: '/idty/:id',
      name: 'identity',
      component: () => import('@/views/IdtyItem.vue')
    },
    {
      path: '/cert/:source/:target',
      name: 'cert',
      component: () => import('@/views/CertItem.vue')
    },
    {
      path: '/data',
      name: 'datapod',
      component: () => import('@/views/DatapodView.vue')
    },
    {
      path: '/edit',
      name: 'profile_edit',
      component: () => import('@/views/DatapodEdit.vue')
    },
    {
      path: '/data/:id',
      name: 'profile',
      component: () => import('@/views/DatapodItem.vue')
    },
    {
      path: '/smith',
      name: 'smith_list',
      component: () => import('@/views/SmithView.vue')
    },
    {
      path: '/smith/:id',
      name: 'smith',
      component: () => import('@/views/SmithItem.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/LoginView.vue')
    },
    {
      path: '/settings/datapod',
      name: 'datapod_settings',
      component: () => import('@/views/DatapodSettingsView.vue')
    },
    {
      path: '/settings/network-scan',
      name: 'network_scan',
      component: () => import('@/views/NetworkScanView.vue')
    },
    { path: '/:pathMatch(.*)*', name: 'NotFound', component: NotFound }
  ]
})

export default router
