import type { ApiPromise } from '@polkadot/api'
import { web3FromAddress } from '@polkadot/extension-dapp'
import type { Vec, u32 } from '@polkadot/types-codec'
import type { ISubmittableResult, RegistryError } from '@polkadot/types/types'
import type { Ref } from 'vue'
import { loginAccount } from './global'
import type { SubmittableExtrinsic } from '@polkadot/api/promise/types'
import type { AugmentedSubmittable } from '@polkadot/api-base/types'

/// fetch authorities
export function fetchAuthorities(
  api: ApiPromise,
  online: Ref<Vec<u32> | null>,
  incoming: Ref<Vec<u32> | null>,
  outgoing: Ref<Vec<u32> | null>
) {
  api.queryMulti<[Vec<u32>, Vec<u32>, Vec<u32>]>(
    [
      api.query.authorityMembers.onlineAuthorities,
      api.query.authorityMembers.incomingAuthorities,
      api.query.authorityMembers.outgoingAuthorities
    ],
    ([r_online, r_incoming, r_outgoing]) => {
      online.value = r_online
      incoming.value = r_incoming
      outgoing.value = r_outgoing
    }
  )
}

// signer function
export async function signAndSend(
  api: ApiPromise,
  tx: SubmittableExtrinsic,
  notif: Ref<string>,
  successCallback: () => void
) {
  const address = loginAccount.value!.address
  const injector = await web3FromAddress(address)
  try {
    await tx.signAndSend(
      address,
      { signer: injector.signer },
      notifier(api, notif, successCallback)
    )
  } catch (e) {
    notif.value = (e as Error).toString()
  }
}

// notifier
const notifier = (api: ApiPromise, notif: Ref<string>, successCallback: () => void) =>
  function handler({ events, status, dispatchError }: ISubmittableResult) {
    console.log('Transaction status:', status.type)
    if (status.isBroadcast) {
      notif.value += 'sent, waiting... '
    } else if (status.isInBlock) {
      if (dispatchError) {
        if (dispatchError.isModule) {
          const decoded: RegistryError = api.registry.findMetaError(dispatchError.asModule)
          const { docs, name, section } = decoded
          const errorMsg = `${section}.${name}: ${docs.join(' ')}`
          console.log(errorMsg)
          console.log(dispatchError.asModule)
          notif.value = errorMsg
        } else {
          // Other, CannotLookup, BadOrigin, no extra info
          console.log(dispatchError.toString())
        }
      } else {
        notif.value += 'in block... '
        console.log('Included at block hash', status.asInBlock.toHex())
        console.log('Events:')
        events.forEach(({ event: { data, method, section }, phase }) => {
          console.log('\t', phase.toString(), `: ${section}.${method}`, data.toString())
        })
        successCallback()
      }
    } else if (status.isFinalized) {
      notif.value = ''
    }
  }
