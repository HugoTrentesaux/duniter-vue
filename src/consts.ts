// TODO remove GDEV_*_endpoints from this file and use the ones from config.json instead

// rpc
export const LOCAL_DUNITER_ENDPOINT = 'ws://localhost:9944'
export const GDEV_DUNITER_ENDPOINTS = [
  'wss://gdev.coinduf.eu',
  'wss://gdev.gyroi.de',
  'wss://gdev.cgeek.fr',
  'wss://gdev.p2p.legal/ws'
]

// squid
export const LOCAL_SQUID_ENDPOINT = 'http://localhost:8080/v1/graphql'
export const GDEV_SQUID_ENDPOINTS = [
  'https://squid.gdev.coinduf.eu/v1/graphql',
  'https://squid.gdev.gyroi.de/v1/graphql'
]

// datapod
export const LOCAL_DATAPOD_ENDPOINT = 'http://localhost:8081/v1/graphql'
export const GDEV_DATAPOD_ENDPOINTS = [
  'https://datapod.coinduf.eu/v1/graphql',
  'https://datapod.gyroi.de/v1/graphql'
]

// ipfs gateway
export const LOCAL_IPFS_GATEWAY = 'http://localhost:8080'
export const PUBLIC_IPFS_GATEWAY = 'https://pagu.re'
export const KUBO_PUBLIC_RPC = 'https://rpc.datapod.gyroi.de'
// TODO make kubo rpc available in config
// and allow to use it instead of helia (for instance to use system-wide ipfs node)

// pubsub topic
export const DATAPOD_PUBSUB_TOPIC = 'ddd'
