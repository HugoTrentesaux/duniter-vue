import type { ApiPromise } from '@polkadot/api'
import { computed, ref } from 'vue'
import type { Ref } from 'vue'
import type { InjectedAccountWithMeta } from '@polkadot/extension-inject/types'
import { formatBalance } from '@polkadot/util'
import type { FirstBlockQuery, SmithStatusEnum } from '@/generated/squid'
import type {
  FrameSystemAccountInfo,
  PalletCertificationIdtyCertMeta,
  PalletIdentityIdtyValue,
  SpMembershipMembershipData
} from '@polkadot/types/lookup'
import { getItem, sk } from './storage'
import type { ApolloClient, FetchResult, NormalizedCacheObject } from '@apollo/client'
import { tryConnectAccount } from './login'
import type { GenesisHashQuery, LatestBlockSubscription } from '@/generated/squid'
import { LatestBlock, GenesisHash, FirstBlock } from '@/squid/meta.gql'

// app state
export const initialized = ref(false)

// endpoints
export const duniterEndpoint: Ref<string> = ref('')
export const squidEndpoint: Ref<string> = ref('')
export const squidWsEndpoint = computed(() => squidEndpoint.value.replace('http', 'ws'))
export const datapodEndpoint: Ref<string> = ref('')
export const ipfsGateway: Ref<string> = ref('')

// genesis time
export const genHash: Ref<string | null> = ref(null)
export const genTimestamp: Ref<null | number> = ref(null)
export const tokenSymbol: Ref<string | null> = ref(null)
export const tokenDecimals: Ref<number> = ref(0)
export const currentBlockHeight = ref(0)

// squid global vars
export const squidGenHash: Ref<string | null> = ref(null)
export const squidCurrentBlockHeight = ref(0)
export const onSameNetwork = computed(() => squidGenHash.value == genHash.value)
export const squidHeightDiff = computed(
  () => currentBlockHeight.value - squidCurrentBlockHeight.value
)
export const squidWellSync = computed(() => onSameNetwork.value && squidHeightDiff.value <= 1)

// wip glob
export const wipFirstBlock = ref(0)

// initialize global state from storage
// use user-defined values or default if null
export function initStateFromStorage() {
  duniterEndpoint.value = getItem(sk.userRpcEndpoint) ?? getItem(sk.defaultRpcEndpoint)
  squidEndpoint.value = getItem(sk.userSquidEndpoint) ?? getItem(sk.defaultSquidEndpoint)
  datapodEndpoint.value = getItem(sk.userDatapodEndpoint) ?? getItem(sk.defaultDatapodEndpoint)
  ipfsGateway.value = getItem(sk.userIpfsGateway) ?? getItem(sk.defaultIpfsGateway)
}

// initialize global state from api
export async function initStateFromRpc(api: ApiPromise): Promise<void> {
  // get genesis time
  async function getGenTime() {
    const block1hash = await api.rpc.chain.getBlockHash(1)
    const block1 = await api.rpc.chain.getBlock(block1hash)
    const timestamp = block1.block.extrinsics[0].args[0].toPrimitive() as unknown as number
    return timestamp
  }
  getGenTime().then((v) => (genTimestamp.value = v))
  // genesis hash
  genHash.value = api.genesisHash.toHex()
  // get system properties (token symbol and decimal)
  api.rpc.system.properties().then((p) => {
    if (p.tokenDecimals.isSome && p.tokenSymbol.isSome) {
      tokenDecimals.value = p.tokenDecimals.unwrap()[0].toNumber()
      tokenSymbol.value = p.tokenSymbol.unwrap()[0].toString()
      formatBalance.setDefaults({ decimals: tokenDecimals.value, unit: tokenSymbol.value })
    }
    initialized.value = true
  })
  // current block height
  api.rpc.chain.subscribeNewHeads((header) => {
    currentBlockHeight.value = header.number.toNumber()
  })
}

// initialize global state from squid
async function initStateFromSquid(apollo: ApolloClient<NormalizedCacheObject>) {
  function newBlockCallback(data: FetchResult<LatestBlockSubscription>) {
    squidCurrentBlockHeight.value = data.data?.block.at(0)?.height || 0
  }
  function genesisCallback(hash: string) {
    squidGenHash.value = hash
  }
  squidBlockchainInfo(apollo, genesisCallback, newBlockCallback, () => {})
}

// get blockchain info from squid and run callbacks
export async function squidBlockchainInfo(
  apollo: ApolloClient<NormalizedCacheObject>,
  gotGenesisHash: (r: string) => void,
  gotLatestBlock: (r: FetchResult<LatestBlockSubscription>) => void,
  errHandler: (err: any) => void
) {
  function complete() {
    console.log('blockchain is not supposed to end')
  }
  try {
    // subscribe to new heads
    apollo
      .subscribe<LatestBlockSubscription>({ query: LatestBlock })
      .subscribe(gotLatestBlock, errHandler, complete)
    // get genesis information
    apollo
      .query<GenesisHashQuery>({
        query: GenesisHash
      })
      .then((r) => {
        if (r.error) {
          errHandler(r.errors)
        }
        const hash = r.data.block.at(0)?.hash
        const hash0 = hash.replace('\\', '0')
        gotGenesisHash(hash0)
      })
      .catch(errHandler)
    // get first v1 block info
    // TODO this can be done statically since it never changes after network launch
    // but at the moment it is more convenient to fetch it on start
    apollo
      .query<FirstBlockQuery>({
        query: FirstBlock
      })
      .then((r) => {
        if (r.error) {
          errHandler(r.errors)
        }
        wipFirstBlock.value = r.data.block.at(0)?.height ?? 0
      })
      .catch(errHandler)
  } catch (err) {
    errHandler(err)
  }
}

// initialize global state from api and apollo
export async function initStateFromApis(
  api: ApiPromise,
  apolloSquid: ApolloClient<NormalizedCacheObject>
): Promise<void> {
  initStateFromSquid(apolloSquid)
  await tryConnectAccount(api, apolloSquid)
}

// account selected by user
export const loginAccount: Ref<InjectedAccountWithMeta | null> = ref(null)
// identity selected by user
export const loginIdtyId: Ref<number | null> = ref(null)
export const loginIdtyName: Ref<string | null> = ref(null)
export const loginIdtyVal: Ref<PalletIdentityIdtyValue | null> = ref(null)
export const loginMshipVal: Ref<SpMembershipMembershipData | null> = ref(null)
export const loginCertVal: Ref<PalletCertificationIdtyCertMeta | null> = ref(null)
export const loginAsSmith: Ref<SmithStatusEnum | null> = ref(null)
export const loginAccountInfo: Ref<FrameSystemAccountInfo | null> = ref(null)
