import { CID } from 'multiformats'
import { getItem, setItem, sk, updateItem } from './storage'
import { PUBLIC_IPFS_GATEWAY } from './consts'

interface Config {
  // default duniter endpoint (network unknown)
  default_duniter_endpoint?: string
  // list of duniter endpoints expected to be on gdev network
  gdev_duniter_endpoints?: Array<string>
  // default squid endpoint (network unknown)
  default_squid_endpoint?: string
  // list of squid endpoints expected to be on gdev network
  gdev_squid_endpoints?: Array<string>
  default_ipfs_gateway?: string
  known_ipfs_gateways?: Array<string>
  default_datapod_endpoint?: string
  datapod_known_endpoints?: Array<string>
  datapod_known_peers?: any // should be able to convert to AddrInfo[]
}

interface NetworkConfig {
  // tells if the network is active or not
  active: boolean
  // hexadecimal of the genesis hash
  genesis_hash?: string
  // unix timestamp of the genesis
  genesis_timestamp?: number
  // v1 block number if the network imported g1 data
  last_g1_v1_block_number?: number
  // urls of duniter rpc endpoints that should be available and well synchronized on this network
  rpc?: string[]
  // urls of duniter-squid graphql endpoints that should be available and well sychronized on this network
  squid?: string[]
}

const CONFIG_PATH = '/config.json'
const LOCAL_NETWORK_CONF = (network: string) => `/${network}.json`
const REMOTE_NETWORK_CONF = (network: string) =>
  `https://git.duniter.org/nodes/networks/-/raw/master/${network}.json`

// reads config file from assets
// this file can be modified after build if someone wants to serve duniter panel with a custom config
async function getConfigFromAssets(): Promise<Config> {
  const [config, gdev, _gtest, _g1] = (await Promise.all([
    fetch(CONFIG_PATH).then((v) => v.json()),
    fetch(LOCAL_NETWORK_CONF('gdev')).then((v) => v.json()),
    fetch(LOCAL_NETWORK_CONF('gtest')).then((v) => v.json()),
    fetch(LOCAL_NETWORK_CONF('g1')).then((v) => v.json())
  ])) as [Config, NetworkConfig, NetworkConfig, NetworkConfig]
  // only have gdev endpoints at the moment, ignore gtest and g1
  // WARN when loading from assets, the gdev known endpoints are overwritten
  // TODO also add other network endpoints
  // TODO move ipfs and datapods to shared conf
  config.gdev_duniter_endpoints = gdev.rpc
  config.gdev_squid_endpoints = gdev.squid
  return config
}

// allow to fetch network config from default remote endpoint
export async function getRemoteNetworkConfig(remote: string): Promise<NetworkConfig> {
  return fetch(remote).then((v) => v.json())
}

// update local storage from config file
export function updateStorage(config: Config) {
  // replace default endpoints
  config.default_duniter_endpoint
    ? setItem(sk.defaultRpcEndpoint, config.default_duniter_endpoint)
    : null
  config.default_squid_endpoint
    ? setItem(sk.defaultSquidEndpoint, config.default_squid_endpoint)
    : null
  config.default_datapod_endpoint
    ? setItem(sk.defaultDatapodEndpoint, config.default_datapod_endpoint)
    : null
  config.default_ipfs_gateway ? setItem(sk.defaultIpfsGateway, config.default_ipfs_gateway) : null
  // append to known list
  if (config.gdev_duniter_endpoints) {
    updateItem<Array<string>>(sk.rpcEndpoints, (v) => {
      return Array.from(new Set(v.concat(config.gdev_duniter_endpoints!)))
    })
  }
  if (config.gdev_squid_endpoints) {
    updateItem<Array<string>>(sk.squidEndpoints, (v) => {
      return Array.from(new Set(v.concat(config.gdev_squid_endpoints!)))
    })
  }
  if (config.datapod_known_endpoints) {
    updateItem<Array<string>>(sk.datapodEndpoints, (v) => {
      return Array.from(new Set(v.concat(config.datapod_known_endpoints!)))
    })
  }
  if (config.known_ipfs_gateways) {
    updateItem<Array<string>>(sk.ipfsGateways, (v) => {
      return Array.from(new Set(v.concat(config.known_ipfs_gateways!)))
    })
  }
  // merge multiaddresses of peers
  if (config.datapod_known_peers) {
    updateItem<any>(sk.datapodKnownPeers, (v) => {
      for (const [p, l] of Object.entries(config.datapod_known_peers)) {
        if (v[p]) {
          v[p] = Array.from(new Set(v[p].concat(l)))
        } else {
          v[p] = l
        }
      }
      return v
    })
  }
}

// extract user localstorage to a config object
export function extractConfig(): Config {
  const conf: Config = {
    default_duniter_endpoint: getItem(sk.userRpcEndpoint) ?? getItem(sk.defaultRpcEndpoint),
    gdev_duniter_endpoints: getItem(sk.rpcEndpoints),
    default_squid_endpoint: getItem(sk.userSquidEndpoint) ?? getItem(sk.defaultSquidEndpoint),
    gdev_squid_endpoints: getItem(sk.squidEndpoints),
    default_ipfs_gateway: getItem(sk.userIpfsGateway) ?? getItem(sk.defaultIpfsGateway),
    known_ipfs_gateways: getItem(sk.ipfsGateways),
    default_datapod_endpoint: getItem(sk.defaultDatapodEndpoint),
    datapod_known_endpoints: getItem(sk.datapodEndpoints),
    datapod_known_peers: getItem(sk.datapodKnownPeers)
  }
  return conf
}

// awaited at the beginning of main
export async function updateStorageFromConfigAsset() {
  const config = await getConfigFromAssets()
  updateStorage(config)
}

// load endpoints known by remote config
export async function updateStorageFromDefaultRemoteNetworkConfig(network: string) {
  const conf = await getRemoteNetworkConfig(REMOTE_NETWORK_CONF(network))
  updateStorageFromNetworkConfig(network, conf)
}

function updateStorageFromNetworkConfig(network: string, config: NetworkConfig) {
  const conf: Config = {}
  switch (network) {
    case 'gdev':
      conf.gdev_duniter_endpoints = config.rpc
      conf.gdev_squid_endpoints = config.squid
      break
    default:
      // TODO support other networks
      console.log('network not handled yet', network)
  }
  updateStorage(conf)
}

export async function updateStorageFromConfigQueryString(config: string) {
  const conf_file = await getConfigFile(config)
  if (conf_file) {
    try {
      const conf = JSON.parse(conf_file)
      updateStorage(conf)
    } catch (e) {
      console.error('failed to parse conf_file as json', conf_file)
    }
  }
}

// try to get config file from what the string is (cid, url...)
async function getConfigFile(config: string): Promise<string | null> {
  let config_url = null
  // first try to parse config as CID
  try {
    const _cid = CID.parse(config)
    // TODO p2p alternative to http gateway
    const gateway =
      getItem(sk.userIpfsGateway) ?? getItem(sk.defaultIpfsGateway) ?? PUBLIC_IPFS_GATEWAY
    config_url = gateway + '/ipfs/' + config
  } catch {
    console.log('config is not a cid', config)
    // then try to parse as URL
    try {
      const _url = new URL(config)
      config_url = config
    } catch {
      console.log('config is not an URL', config)
    }
  }
  if (config_url) {
    console.log('fetching config at', config_url)
    return fetch(config_url).then((v) => v.text())
  }
  return null
}
