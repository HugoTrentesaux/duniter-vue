import { ApolloClient, createHttpLink, InMemoryCache, split } from '@apollo/client/core'
import { type NormalizedCacheObject } from '@apollo/client/core'
import {
  duniterEndpoint,
  initStateFromRpc,
  squidEndpoint,
  squidWsEndpoint,
  datapodEndpoint
} from './global'
import { ApiPromise, WsProvider } from '@polkadot/api'
import { GraphQLWsLink } from '@apollo/client/link/subscriptions'
import { createClient } from 'graphql-ws'
import { getMainDefinition } from '@apollo/client/utilities'
import { getIpfsStack, type DatapodIpfsStack } from './ipfs'
import { KUBO_PUBLIC_RPC } from './consts'
import type { KuboRPCClient } from 'kubo-rpc-client'
import { create } from 'kubo-rpc-client'

// global var for rpc api
let duniterRpc: ApiPromise | null = null
// global var for squid api
let apollo: ApolloClient<NormalizedCacheObject> | null = null
// global var for datapod api
let apolloDatapod: ApolloClient<NormalizedCacheObject> | null = null
// global var for p2p
let p2p: DatapodIpfsStack | null = null

/// initialize app squid from global endpoint values
export async function initSquid(): Promise<ApolloClient<NormalizedCacheObject>> {
  // these values react to global endpoint update
  return initApollo(
    () => squidEndpoint.value,
    () => squidWsEndpoint.value
  )
}

/// initialize squid endpoint from given values
export async function initApollo(
  httpEnd: () => string,
  wsEnd: () => string
): Promise<ApolloClient<NormalizedCacheObject>> {
  const httpLink = createHttpLink({
    uri: httpEnd
  })
  const wsLink = new GraphQLWsLink(
    createClient({
      url: wsEnd
    })
  )
  // Using split to direct subscriptions to WebSocket and other queries to HTTP
  const link = split(
    ({ query }) => {
      const definition = getMainDefinition(query)
      return definition.kind === 'OperationDefinition' && definition.operation === 'subscription'
    },
    wsLink,
    httpLink
  )
  const cache = new InMemoryCache()
  apollo = new ApolloClient({
    link: link,
    cache
  })
  return apollo
}

// update squid endpoint (not needed since apollo can react to endpoint change)
export async function updateSquid(): Promise<ApolloClient<NormalizedCacheObject>> {
  if (!apollo) {
    return initSquid()
  }
  return apollo
}

/// initialize datapod
export async function initDatapod(): Promise<ApolloClient<NormalizedCacheObject>> {
  const httpLinkDatapod = createHttpLink({
    uri: () => datapodEndpoint.value
  })
  const cache = new InMemoryCache()
  apolloDatapod = new ApolloClient({
    link: httpLinkDatapod,
    cache
  })
  return apolloDatapod
}

/// initialize RPC
export async function initRpc(): Promise<ApiPromise> {
  const wsProvider = new WsProvider(duniterEndpoint.value)
  duniterRpc = await ApiPromise.create({ provider: wsProvider })
  // initialize global state
  initStateFromRpc(duniterRpc)
  return duniterRpc
}

/// update app RPC
export async function updateAppRpc(): Promise<ApiPromise> {
  if (duniterRpc) {
    await duniterRpc.disconnect()
  }
  return initRpc()
}

/// initialize p2p stack
export async function initp2p(): Promise<DatapodIpfsStack> {
  p2p = await getIpfsStack()
  return p2p
}

/// initialize kubo rpc
export function initKuboRpc(): KuboRPCClient {
  const kubo: KuboRPCClient = create({
    url: new URL(KUBO_PUBLIC_RPC)
  })
  return kubo
}
