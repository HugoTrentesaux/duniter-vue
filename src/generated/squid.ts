export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  bytea: { input: any; output: any; }
  identity_scalar: { input: any; output: any; }
  jsonb: { input: any; output: any; }
  numeric: { input: any; output: any; }
  timestamptz: { input: any; output: any; }
};

/** columns and relationships of "account" */
export type Account = {
  __typename?: 'Account';
  /** An array relationship */
  commentsIssued: Array<TxComment>;
  /** An aggregate relationship */
  commentsIssuedAggregate: TxCommentAggregate;
  createdOn: Scalars['Int']['output'];
  id: Scalars['String']['output'];
  /** An object relationship */
  identity?: Maybe<Identity>;
  isActive: Scalars['Boolean']['output'];
  /** An object relationship */
  linkedIdentity?: Maybe<Identity>;
  linkedIdentityId?: Maybe<Scalars['String']['output']>;
  /** An array relationship */
  removedIdentities: Array<Identity>;
  /** An aggregate relationship */
  removedIdentitiesAggregate: IdentityAggregate;
  /** An array relationship */
  transfersIssued: Array<Transfer>;
  /** An aggregate relationship */
  transfersIssuedAggregate: TransferAggregate;
  /** An array relationship */
  transfersReceived: Array<Transfer>;
  /** An aggregate relationship */
  transfersReceivedAggregate: TransferAggregate;
  /** An array relationship */
  wasIdentity: Array<ChangeOwnerKey>;
  /** An aggregate relationship */
  wasIdentityAggregate: ChangeOwnerKeyAggregate;
};


/** columns and relationships of "account" */
export type AccountCommentsIssuedArgs = {
  distinctOn?: InputMaybe<Array<TxCommentSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<TxCommentOrderBy>>;
  where?: InputMaybe<TxCommentBoolExp>;
};


/** columns and relationships of "account" */
export type AccountCommentsIssuedAggregateArgs = {
  distinctOn?: InputMaybe<Array<TxCommentSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<TxCommentOrderBy>>;
  where?: InputMaybe<TxCommentBoolExp>;
};


/** columns and relationships of "account" */
export type AccountRemovedIdentitiesArgs = {
  distinctOn?: InputMaybe<Array<IdentitySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<IdentityOrderBy>>;
  where?: InputMaybe<IdentityBoolExp>;
};


/** columns and relationships of "account" */
export type AccountRemovedIdentitiesAggregateArgs = {
  distinctOn?: InputMaybe<Array<IdentitySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<IdentityOrderBy>>;
  where?: InputMaybe<IdentityBoolExp>;
};


/** columns and relationships of "account" */
export type AccountTransfersIssuedArgs = {
  distinctOn?: InputMaybe<Array<TransferSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<TransferOrderBy>>;
  where?: InputMaybe<TransferBoolExp>;
};


/** columns and relationships of "account" */
export type AccountTransfersIssuedAggregateArgs = {
  distinctOn?: InputMaybe<Array<TransferSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<TransferOrderBy>>;
  where?: InputMaybe<TransferBoolExp>;
};


/** columns and relationships of "account" */
export type AccountTransfersReceivedArgs = {
  distinctOn?: InputMaybe<Array<TransferSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<TransferOrderBy>>;
  where?: InputMaybe<TransferBoolExp>;
};


/** columns and relationships of "account" */
export type AccountTransfersReceivedAggregateArgs = {
  distinctOn?: InputMaybe<Array<TransferSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<TransferOrderBy>>;
  where?: InputMaybe<TransferBoolExp>;
};


/** columns and relationships of "account" */
export type AccountWasIdentityArgs = {
  distinctOn?: InputMaybe<Array<ChangeOwnerKeySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ChangeOwnerKeyOrderBy>>;
  where?: InputMaybe<ChangeOwnerKeyBoolExp>;
};


/** columns and relationships of "account" */
export type AccountWasIdentityAggregateArgs = {
  distinctOn?: InputMaybe<Array<ChangeOwnerKeySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ChangeOwnerKeyOrderBy>>;
  where?: InputMaybe<ChangeOwnerKeyBoolExp>;
};

/** aggregated selection of "account" */
export type AccountAggregate = {
  __typename?: 'AccountAggregate';
  aggregate?: Maybe<AccountAggregateFields>;
  nodes: Array<Account>;
};

export type AccountAggregateBoolExp = {
  bool_and?: InputMaybe<AccountAggregateBoolExpBool_And>;
  bool_or?: InputMaybe<AccountAggregateBoolExpBool_Or>;
  count?: InputMaybe<AccountAggregateBoolExpCount>;
};

/** aggregate fields of "account" */
export type AccountAggregateFields = {
  __typename?: 'AccountAggregateFields';
  avg?: Maybe<AccountAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<AccountMaxFields>;
  min?: Maybe<AccountMinFields>;
  stddev?: Maybe<AccountStddevFields>;
  stddevPop?: Maybe<AccountStddevPopFields>;
  stddevSamp?: Maybe<AccountStddevSampFields>;
  sum?: Maybe<AccountSumFields>;
  varPop?: Maybe<AccountVarPopFields>;
  varSamp?: Maybe<AccountVarSampFields>;
  variance?: Maybe<AccountVarianceFields>;
};


/** aggregate fields of "account" */
export type AccountAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<AccountSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "account" */
export type AccountAggregateOrderBy = {
  avg?: InputMaybe<AccountAvgOrderBy>;
  count?: InputMaybe<OrderBy>;
  max?: InputMaybe<AccountMaxOrderBy>;
  min?: InputMaybe<AccountMinOrderBy>;
  stddev?: InputMaybe<AccountStddevOrderBy>;
  stddevPop?: InputMaybe<AccountStddevPopOrderBy>;
  stddevSamp?: InputMaybe<AccountStddevSampOrderBy>;
  sum?: InputMaybe<AccountSumOrderBy>;
  varPop?: InputMaybe<AccountVarPopOrderBy>;
  varSamp?: InputMaybe<AccountVarSampOrderBy>;
  variance?: InputMaybe<AccountVarianceOrderBy>;
};

/** aggregate avg on columns */
export type AccountAvgFields = {
  __typename?: 'AccountAvgFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "account" */
export type AccountAvgOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "account". All fields are combined with a logical 'AND'. */
export type AccountBoolExp = {
  _and?: InputMaybe<Array<AccountBoolExp>>;
  _not?: InputMaybe<AccountBoolExp>;
  _or?: InputMaybe<Array<AccountBoolExp>>;
  commentsIssued?: InputMaybe<TxCommentBoolExp>;
  commentsIssuedAggregate?: InputMaybe<TxCommentAggregateBoolExp>;
  createdOn?: InputMaybe<IntComparisonExp>;
  id?: InputMaybe<StringComparisonExp>;
  identity?: InputMaybe<IdentityBoolExp>;
  isActive?: InputMaybe<BooleanComparisonExp>;
  linkedIdentity?: InputMaybe<IdentityBoolExp>;
  linkedIdentityId?: InputMaybe<StringComparisonExp>;
  removedIdentities?: InputMaybe<IdentityBoolExp>;
  removedIdentitiesAggregate?: InputMaybe<IdentityAggregateBoolExp>;
  transfersIssued?: InputMaybe<TransferBoolExp>;
  transfersIssuedAggregate?: InputMaybe<TransferAggregateBoolExp>;
  transfersReceived?: InputMaybe<TransferBoolExp>;
  transfersReceivedAggregate?: InputMaybe<TransferAggregateBoolExp>;
  wasIdentity?: InputMaybe<ChangeOwnerKeyBoolExp>;
  wasIdentityAggregate?: InputMaybe<ChangeOwnerKeyAggregateBoolExp>;
};

/** aggregate max on columns */
export type AccountMaxFields = {
  __typename?: 'AccountMaxFields';
  createdOn?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  linkedIdentityId?: Maybe<Scalars['String']['output']>;
};

/** order by max() on columns of table "account" */
export type AccountMaxOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  linkedIdentityId?: InputMaybe<OrderBy>;
};

/** aggregate min on columns */
export type AccountMinFields = {
  __typename?: 'AccountMinFields';
  createdOn?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  linkedIdentityId?: Maybe<Scalars['String']['output']>;
};

/** order by min() on columns of table "account" */
export type AccountMinOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  linkedIdentityId?: InputMaybe<OrderBy>;
};

/** Ordering options when selecting data from "account". */
export type AccountOrderBy = {
  commentsIssuedAggregate?: InputMaybe<TxCommentAggregateOrderBy>;
  createdOn?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  identity?: InputMaybe<IdentityOrderBy>;
  isActive?: InputMaybe<OrderBy>;
  linkedIdentity?: InputMaybe<IdentityOrderBy>;
  linkedIdentityId?: InputMaybe<OrderBy>;
  removedIdentitiesAggregate?: InputMaybe<IdentityAggregateOrderBy>;
  transfersIssuedAggregate?: InputMaybe<TransferAggregateOrderBy>;
  transfersReceivedAggregate?: InputMaybe<TransferAggregateOrderBy>;
  wasIdentityAggregate?: InputMaybe<ChangeOwnerKeyAggregateOrderBy>;
};

/** select columns of table "account" */
export enum AccountSelectColumn {
  /** column name */
  CreatedOn = 'createdOn',
  /** column name */
  Id = 'id',
  /** column name */
  IsActive = 'isActive',
  /** column name */
  LinkedIdentityId = 'linkedIdentityId'
}

/** select "accountAggregateBoolExpBool_andArgumentsColumns" columns of table "account" */
export enum AccountSelectColumnAccountAggregateBoolExpBool_AndArgumentsColumns {
  /** column name */
  IsActive = 'isActive'
}

/** select "accountAggregateBoolExpBool_orArgumentsColumns" columns of table "account" */
export enum AccountSelectColumnAccountAggregateBoolExpBool_OrArgumentsColumns {
  /** column name */
  IsActive = 'isActive'
}

/** aggregate stddev on columns */
export type AccountStddevFields = {
  __typename?: 'AccountStddevFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "account" */
export type AccountStddevOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
};

/** aggregate stddevPop on columns */
export type AccountStddevPopFields = {
  __typename?: 'AccountStddevPopFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevPop() on columns of table "account" */
export type AccountStddevPopOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
};

/** aggregate stddevSamp on columns */
export type AccountStddevSampFields = {
  __typename?: 'AccountStddevSampFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevSamp() on columns of table "account" */
export type AccountStddevSampOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
};

/** Streaming cursor of the table "account" */
export type AccountStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: AccountStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type AccountStreamCursorValueInput = {
  createdOn?: InputMaybe<Scalars['Int']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  isActive?: InputMaybe<Scalars['Boolean']['input']>;
  linkedIdentityId?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate sum on columns */
export type AccountSumFields = {
  __typename?: 'AccountSumFields';
  createdOn?: Maybe<Scalars['Int']['output']>;
};

/** order by sum() on columns of table "account" */
export type AccountSumOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
};

/** aggregate varPop on columns */
export type AccountVarPopFields = {
  __typename?: 'AccountVarPopFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
};

/** order by varPop() on columns of table "account" */
export type AccountVarPopOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
};

/** aggregate varSamp on columns */
export type AccountVarSampFields = {
  __typename?: 'AccountVarSampFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
};

/** order by varSamp() on columns of table "account" */
export type AccountVarSampOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
};

/** aggregate variance on columns */
export type AccountVarianceFields = {
  __typename?: 'AccountVarianceFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "account" */
export type AccountVarianceOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
};

/** columns and relationships of "block" */
export type Block = {
  __typename?: 'Block';
  /** An array relationship */
  calls: Array<Call>;
  /** An aggregate relationship */
  callsAggregate: CallAggregate;
  callsCount: Scalars['Int']['output'];
  /** An array relationship */
  events: Array<Event>;
  /** An aggregate relationship */
  eventsAggregate: EventAggregate;
  eventsCount: Scalars['Int']['output'];
  /** An array relationship */
  extrinsics: Array<Extrinsic>;
  /** An aggregate relationship */
  extrinsicsAggregate: ExtrinsicAggregate;
  extrinsicsCount: Scalars['Int']['output'];
  extrinsicsicRoot: Scalars['bytea']['output'];
  hash: Scalars['bytea']['output'];
  height: Scalars['Int']['output'];
  id: Scalars['String']['output'];
  implName: Scalars['String']['output'];
  implVersion: Scalars['Int']['output'];
  parentHash: Scalars['bytea']['output'];
  specName: Scalars['String']['output'];
  specVersion: Scalars['Int']['output'];
  stateRoot: Scalars['bytea']['output'];
  timestamp: Scalars['timestamptz']['output'];
  validator?: Maybe<Scalars['bytea']['output']>;
};


/** columns and relationships of "block" */
export type BlockCallsArgs = {
  distinctOn?: InputMaybe<Array<CallSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CallOrderBy>>;
  where?: InputMaybe<CallBoolExp>;
};


/** columns and relationships of "block" */
export type BlockCallsAggregateArgs = {
  distinctOn?: InputMaybe<Array<CallSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CallOrderBy>>;
  where?: InputMaybe<CallBoolExp>;
};


/** columns and relationships of "block" */
export type BlockEventsArgs = {
  distinctOn?: InputMaybe<Array<EventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<EventOrderBy>>;
  where?: InputMaybe<EventBoolExp>;
};


/** columns and relationships of "block" */
export type BlockEventsAggregateArgs = {
  distinctOn?: InputMaybe<Array<EventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<EventOrderBy>>;
  where?: InputMaybe<EventBoolExp>;
};


/** columns and relationships of "block" */
export type BlockExtrinsicsArgs = {
  distinctOn?: InputMaybe<Array<ExtrinsicSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ExtrinsicOrderBy>>;
  where?: InputMaybe<ExtrinsicBoolExp>;
};


/** columns and relationships of "block" */
export type BlockExtrinsicsAggregateArgs = {
  distinctOn?: InputMaybe<Array<ExtrinsicSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ExtrinsicOrderBy>>;
  where?: InputMaybe<ExtrinsicBoolExp>;
};

/** aggregated selection of "block" */
export type BlockAggregate = {
  __typename?: 'BlockAggregate';
  aggregate?: Maybe<BlockAggregateFields>;
  nodes: Array<Block>;
};

/** aggregate fields of "block" */
export type BlockAggregateFields = {
  __typename?: 'BlockAggregateFields';
  avg?: Maybe<BlockAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<BlockMaxFields>;
  min?: Maybe<BlockMinFields>;
  stddev?: Maybe<BlockStddevFields>;
  stddevPop?: Maybe<BlockStddevPopFields>;
  stddevSamp?: Maybe<BlockStddevSampFields>;
  sum?: Maybe<BlockSumFields>;
  varPop?: Maybe<BlockVarPopFields>;
  varSamp?: Maybe<BlockVarSampFields>;
  variance?: Maybe<BlockVarianceFields>;
};


/** aggregate fields of "block" */
export type BlockAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<BlockSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** aggregate avg on columns */
export type BlockAvgFields = {
  __typename?: 'BlockAvgFields';
  callsCount?: Maybe<Scalars['Float']['output']>;
  eventsCount?: Maybe<Scalars['Float']['output']>;
  extrinsicsCount?: Maybe<Scalars['Float']['output']>;
  height?: Maybe<Scalars['Float']['output']>;
  implVersion?: Maybe<Scalars['Float']['output']>;
  specVersion?: Maybe<Scalars['Float']['output']>;
};

/** Boolean expression to filter rows from the table "block". All fields are combined with a logical 'AND'. */
export type BlockBoolExp = {
  _and?: InputMaybe<Array<BlockBoolExp>>;
  _not?: InputMaybe<BlockBoolExp>;
  _or?: InputMaybe<Array<BlockBoolExp>>;
  calls?: InputMaybe<CallBoolExp>;
  callsAggregate?: InputMaybe<CallAggregateBoolExp>;
  callsCount?: InputMaybe<IntComparisonExp>;
  events?: InputMaybe<EventBoolExp>;
  eventsAggregate?: InputMaybe<EventAggregateBoolExp>;
  eventsCount?: InputMaybe<IntComparisonExp>;
  extrinsics?: InputMaybe<ExtrinsicBoolExp>;
  extrinsicsAggregate?: InputMaybe<ExtrinsicAggregateBoolExp>;
  extrinsicsCount?: InputMaybe<IntComparisonExp>;
  extrinsicsicRoot?: InputMaybe<ByteaComparisonExp>;
  hash?: InputMaybe<ByteaComparisonExp>;
  height?: InputMaybe<IntComparisonExp>;
  id?: InputMaybe<StringComparisonExp>;
  implName?: InputMaybe<StringComparisonExp>;
  implVersion?: InputMaybe<IntComparisonExp>;
  parentHash?: InputMaybe<ByteaComparisonExp>;
  specName?: InputMaybe<StringComparisonExp>;
  specVersion?: InputMaybe<IntComparisonExp>;
  stateRoot?: InputMaybe<ByteaComparisonExp>;
  timestamp?: InputMaybe<TimestamptzComparisonExp>;
  validator?: InputMaybe<ByteaComparisonExp>;
};

/** aggregate max on columns */
export type BlockMaxFields = {
  __typename?: 'BlockMaxFields';
  callsCount?: Maybe<Scalars['Int']['output']>;
  eventsCount?: Maybe<Scalars['Int']['output']>;
  extrinsicsCount?: Maybe<Scalars['Int']['output']>;
  height?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  implName?: Maybe<Scalars['String']['output']>;
  implVersion?: Maybe<Scalars['Int']['output']>;
  specName?: Maybe<Scalars['String']['output']>;
  specVersion?: Maybe<Scalars['Int']['output']>;
  timestamp?: Maybe<Scalars['timestamptz']['output']>;
};

/** aggregate min on columns */
export type BlockMinFields = {
  __typename?: 'BlockMinFields';
  callsCount?: Maybe<Scalars['Int']['output']>;
  eventsCount?: Maybe<Scalars['Int']['output']>;
  extrinsicsCount?: Maybe<Scalars['Int']['output']>;
  height?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  implName?: Maybe<Scalars['String']['output']>;
  implVersion?: Maybe<Scalars['Int']['output']>;
  specName?: Maybe<Scalars['String']['output']>;
  specVersion?: Maybe<Scalars['Int']['output']>;
  timestamp?: Maybe<Scalars['timestamptz']['output']>;
};

/** Ordering options when selecting data from "block". */
export type BlockOrderBy = {
  callsAggregate?: InputMaybe<CallAggregateOrderBy>;
  callsCount?: InputMaybe<OrderBy>;
  eventsAggregate?: InputMaybe<EventAggregateOrderBy>;
  eventsCount?: InputMaybe<OrderBy>;
  extrinsicsAggregate?: InputMaybe<ExtrinsicAggregateOrderBy>;
  extrinsicsCount?: InputMaybe<OrderBy>;
  extrinsicsicRoot?: InputMaybe<OrderBy>;
  hash?: InputMaybe<OrderBy>;
  height?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  implName?: InputMaybe<OrderBy>;
  implVersion?: InputMaybe<OrderBy>;
  parentHash?: InputMaybe<OrderBy>;
  specName?: InputMaybe<OrderBy>;
  specVersion?: InputMaybe<OrderBy>;
  stateRoot?: InputMaybe<OrderBy>;
  timestamp?: InputMaybe<OrderBy>;
  validator?: InputMaybe<OrderBy>;
};

/** select columns of table "block" */
export enum BlockSelectColumn {
  /** column name */
  CallsCount = 'callsCount',
  /** column name */
  EventsCount = 'eventsCount',
  /** column name */
  ExtrinsicsCount = 'extrinsicsCount',
  /** column name */
  ExtrinsicsicRoot = 'extrinsicsicRoot',
  /** column name */
  Hash = 'hash',
  /** column name */
  Height = 'height',
  /** column name */
  Id = 'id',
  /** column name */
  ImplName = 'implName',
  /** column name */
  ImplVersion = 'implVersion',
  /** column name */
  ParentHash = 'parentHash',
  /** column name */
  SpecName = 'specName',
  /** column name */
  SpecVersion = 'specVersion',
  /** column name */
  StateRoot = 'stateRoot',
  /** column name */
  Timestamp = 'timestamp',
  /** column name */
  Validator = 'validator'
}

/** aggregate stddev on columns */
export type BlockStddevFields = {
  __typename?: 'BlockStddevFields';
  callsCount?: Maybe<Scalars['Float']['output']>;
  eventsCount?: Maybe<Scalars['Float']['output']>;
  extrinsicsCount?: Maybe<Scalars['Float']['output']>;
  height?: Maybe<Scalars['Float']['output']>;
  implVersion?: Maybe<Scalars['Float']['output']>;
  specVersion?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddevPop on columns */
export type BlockStddevPopFields = {
  __typename?: 'BlockStddevPopFields';
  callsCount?: Maybe<Scalars['Float']['output']>;
  eventsCount?: Maybe<Scalars['Float']['output']>;
  extrinsicsCount?: Maybe<Scalars['Float']['output']>;
  height?: Maybe<Scalars['Float']['output']>;
  implVersion?: Maybe<Scalars['Float']['output']>;
  specVersion?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddevSamp on columns */
export type BlockStddevSampFields = {
  __typename?: 'BlockStddevSampFields';
  callsCount?: Maybe<Scalars['Float']['output']>;
  eventsCount?: Maybe<Scalars['Float']['output']>;
  extrinsicsCount?: Maybe<Scalars['Float']['output']>;
  height?: Maybe<Scalars['Float']['output']>;
  implVersion?: Maybe<Scalars['Float']['output']>;
  specVersion?: Maybe<Scalars['Float']['output']>;
};

/** Streaming cursor of the table "block" */
export type BlockStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: BlockStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type BlockStreamCursorValueInput = {
  callsCount?: InputMaybe<Scalars['Int']['input']>;
  eventsCount?: InputMaybe<Scalars['Int']['input']>;
  extrinsicsCount?: InputMaybe<Scalars['Int']['input']>;
  extrinsicsicRoot?: InputMaybe<Scalars['bytea']['input']>;
  hash?: InputMaybe<Scalars['bytea']['input']>;
  height?: InputMaybe<Scalars['Int']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  implName?: InputMaybe<Scalars['String']['input']>;
  implVersion?: InputMaybe<Scalars['Int']['input']>;
  parentHash?: InputMaybe<Scalars['bytea']['input']>;
  specName?: InputMaybe<Scalars['String']['input']>;
  specVersion?: InputMaybe<Scalars['Int']['input']>;
  stateRoot?: InputMaybe<Scalars['bytea']['input']>;
  timestamp?: InputMaybe<Scalars['timestamptz']['input']>;
  validator?: InputMaybe<Scalars['bytea']['input']>;
};

/** aggregate sum on columns */
export type BlockSumFields = {
  __typename?: 'BlockSumFields';
  callsCount?: Maybe<Scalars['Int']['output']>;
  eventsCount?: Maybe<Scalars['Int']['output']>;
  extrinsicsCount?: Maybe<Scalars['Int']['output']>;
  height?: Maybe<Scalars['Int']['output']>;
  implVersion?: Maybe<Scalars['Int']['output']>;
  specVersion?: Maybe<Scalars['Int']['output']>;
};

/** aggregate varPop on columns */
export type BlockVarPopFields = {
  __typename?: 'BlockVarPopFields';
  callsCount?: Maybe<Scalars['Float']['output']>;
  eventsCount?: Maybe<Scalars['Float']['output']>;
  extrinsicsCount?: Maybe<Scalars['Float']['output']>;
  height?: Maybe<Scalars['Float']['output']>;
  implVersion?: Maybe<Scalars['Float']['output']>;
  specVersion?: Maybe<Scalars['Float']['output']>;
};

/** aggregate varSamp on columns */
export type BlockVarSampFields = {
  __typename?: 'BlockVarSampFields';
  callsCount?: Maybe<Scalars['Float']['output']>;
  eventsCount?: Maybe<Scalars['Float']['output']>;
  extrinsicsCount?: Maybe<Scalars['Float']['output']>;
  height?: Maybe<Scalars['Float']['output']>;
  implVersion?: Maybe<Scalars['Float']['output']>;
  specVersion?: Maybe<Scalars['Float']['output']>;
};

/** aggregate variance on columns */
export type BlockVarianceFields = {
  __typename?: 'BlockVarianceFields';
  callsCount?: Maybe<Scalars['Float']['output']>;
  eventsCount?: Maybe<Scalars['Float']['output']>;
  extrinsicsCount?: Maybe<Scalars['Float']['output']>;
  height?: Maybe<Scalars['Float']['output']>;
  implVersion?: Maybe<Scalars['Float']['output']>;
  specVersion?: Maybe<Scalars['Float']['output']>;
};

/** Boolean expression to compare columns of type "Boolean". All fields are combined with logical 'AND'. */
export type BooleanComparisonExp = {
  _eq?: InputMaybe<Scalars['Boolean']['input']>;
  _gt?: InputMaybe<Scalars['Boolean']['input']>;
  _gte?: InputMaybe<Scalars['Boolean']['input']>;
  _in?: InputMaybe<Array<Scalars['Boolean']['input']>>;
  _isNull?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['Boolean']['input']>;
  _lte?: InputMaybe<Scalars['Boolean']['input']>;
  _neq?: InputMaybe<Scalars['Boolean']['input']>;
  _nin?: InputMaybe<Array<Scalars['Boolean']['input']>>;
};

/** Boolean expression to compare columns of type "bytea". All fields are combined with logical 'AND'. */
export type ByteaComparisonExp = {
  _eq?: InputMaybe<Scalars['bytea']['input']>;
  _gt?: InputMaybe<Scalars['bytea']['input']>;
  _gte?: InputMaybe<Scalars['bytea']['input']>;
  _in?: InputMaybe<Array<Scalars['bytea']['input']>>;
  _isNull?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['bytea']['input']>;
  _lte?: InputMaybe<Scalars['bytea']['input']>;
  _neq?: InputMaybe<Scalars['bytea']['input']>;
  _nin?: InputMaybe<Array<Scalars['bytea']['input']>>;
};

/** columns and relationships of "call" */
export type Call = {
  __typename?: 'Call';
  address: Array<Scalars['Int']['output']>;
  args?: Maybe<Scalars['jsonb']['output']>;
  argsStr?: Maybe<Array<Scalars['String']['output']>>;
  /** An object relationship */
  block?: Maybe<Block>;
  blockId?: Maybe<Scalars['String']['output']>;
  error?: Maybe<Scalars['jsonb']['output']>;
  /** An array relationship */
  events: Array<Event>;
  /** An aggregate relationship */
  eventsAggregate: EventAggregate;
  /** An object relationship */
  extrinsic?: Maybe<Extrinsic>;
  extrinsicId?: Maybe<Scalars['String']['output']>;
  id: Scalars['String']['output'];
  name: Scalars['String']['output'];
  pallet: Scalars['String']['output'];
  /** An object relationship */
  parent?: Maybe<Call>;
  parentId?: Maybe<Scalars['String']['output']>;
  /** An array relationship */
  subcalls: Array<Call>;
  /** An aggregate relationship */
  subcallsAggregate: CallAggregate;
  success: Scalars['Boolean']['output'];
};


/** columns and relationships of "call" */
export type CallArgsArgs = {
  path?: InputMaybe<Scalars['String']['input']>;
};


/** columns and relationships of "call" */
export type CallErrorArgs = {
  path?: InputMaybe<Scalars['String']['input']>;
};


/** columns and relationships of "call" */
export type CallEventsArgs = {
  distinctOn?: InputMaybe<Array<EventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<EventOrderBy>>;
  where?: InputMaybe<EventBoolExp>;
};


/** columns and relationships of "call" */
export type CallEventsAggregateArgs = {
  distinctOn?: InputMaybe<Array<EventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<EventOrderBy>>;
  where?: InputMaybe<EventBoolExp>;
};


/** columns and relationships of "call" */
export type CallSubcallsArgs = {
  distinctOn?: InputMaybe<Array<CallSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CallOrderBy>>;
  where?: InputMaybe<CallBoolExp>;
};


/** columns and relationships of "call" */
export type CallSubcallsAggregateArgs = {
  distinctOn?: InputMaybe<Array<CallSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CallOrderBy>>;
  where?: InputMaybe<CallBoolExp>;
};

/** aggregated selection of "call" */
export type CallAggregate = {
  __typename?: 'CallAggregate';
  aggregate?: Maybe<CallAggregateFields>;
  nodes: Array<Call>;
};

export type CallAggregateBoolExp = {
  bool_and?: InputMaybe<CallAggregateBoolExpBool_And>;
  bool_or?: InputMaybe<CallAggregateBoolExpBool_Or>;
  count?: InputMaybe<CallAggregateBoolExpCount>;
};

/** aggregate fields of "call" */
export type CallAggregateFields = {
  __typename?: 'CallAggregateFields';
  count: Scalars['Int']['output'];
  max?: Maybe<CallMaxFields>;
  min?: Maybe<CallMinFields>;
};


/** aggregate fields of "call" */
export type CallAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<CallSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "call" */
export type CallAggregateOrderBy = {
  count?: InputMaybe<OrderBy>;
  max?: InputMaybe<CallMaxOrderBy>;
  min?: InputMaybe<CallMinOrderBy>;
};

/** Boolean expression to filter rows from the table "call". All fields are combined with a logical 'AND'. */
export type CallBoolExp = {
  _and?: InputMaybe<Array<CallBoolExp>>;
  _not?: InputMaybe<CallBoolExp>;
  _or?: InputMaybe<Array<CallBoolExp>>;
  address?: InputMaybe<IntArrayComparisonExp>;
  args?: InputMaybe<JsonbComparisonExp>;
  argsStr?: InputMaybe<StringArrayComparisonExp>;
  block?: InputMaybe<BlockBoolExp>;
  blockId?: InputMaybe<StringComparisonExp>;
  error?: InputMaybe<JsonbComparisonExp>;
  events?: InputMaybe<EventBoolExp>;
  eventsAggregate?: InputMaybe<EventAggregateBoolExp>;
  extrinsic?: InputMaybe<ExtrinsicBoolExp>;
  extrinsicId?: InputMaybe<StringComparisonExp>;
  id?: InputMaybe<StringComparisonExp>;
  name?: InputMaybe<StringComparisonExp>;
  pallet?: InputMaybe<StringComparisonExp>;
  parent?: InputMaybe<CallBoolExp>;
  parentId?: InputMaybe<StringComparisonExp>;
  subcalls?: InputMaybe<CallBoolExp>;
  subcallsAggregate?: InputMaybe<CallAggregateBoolExp>;
  success?: InputMaybe<BooleanComparisonExp>;
};

/** aggregate max on columns */
export type CallMaxFields = {
  __typename?: 'CallMaxFields';
  address?: Maybe<Array<Scalars['Int']['output']>>;
  argsStr?: Maybe<Array<Scalars['String']['output']>>;
  blockId?: Maybe<Scalars['String']['output']>;
  extrinsicId?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  name?: Maybe<Scalars['String']['output']>;
  pallet?: Maybe<Scalars['String']['output']>;
  parentId?: Maybe<Scalars['String']['output']>;
};

/** order by max() on columns of table "call" */
export type CallMaxOrderBy = {
  address?: InputMaybe<OrderBy>;
  argsStr?: InputMaybe<OrderBy>;
  blockId?: InputMaybe<OrderBy>;
  extrinsicId?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  name?: InputMaybe<OrderBy>;
  pallet?: InputMaybe<OrderBy>;
  parentId?: InputMaybe<OrderBy>;
};

/** aggregate min on columns */
export type CallMinFields = {
  __typename?: 'CallMinFields';
  address?: Maybe<Array<Scalars['Int']['output']>>;
  argsStr?: Maybe<Array<Scalars['String']['output']>>;
  blockId?: Maybe<Scalars['String']['output']>;
  extrinsicId?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  name?: Maybe<Scalars['String']['output']>;
  pallet?: Maybe<Scalars['String']['output']>;
  parentId?: Maybe<Scalars['String']['output']>;
};

/** order by min() on columns of table "call" */
export type CallMinOrderBy = {
  address?: InputMaybe<OrderBy>;
  argsStr?: InputMaybe<OrderBy>;
  blockId?: InputMaybe<OrderBy>;
  extrinsicId?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  name?: InputMaybe<OrderBy>;
  pallet?: InputMaybe<OrderBy>;
  parentId?: InputMaybe<OrderBy>;
};

/** Ordering options when selecting data from "call". */
export type CallOrderBy = {
  address?: InputMaybe<OrderBy>;
  args?: InputMaybe<OrderBy>;
  argsStr?: InputMaybe<OrderBy>;
  block?: InputMaybe<BlockOrderBy>;
  blockId?: InputMaybe<OrderBy>;
  error?: InputMaybe<OrderBy>;
  eventsAggregate?: InputMaybe<EventAggregateOrderBy>;
  extrinsic?: InputMaybe<ExtrinsicOrderBy>;
  extrinsicId?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  name?: InputMaybe<OrderBy>;
  pallet?: InputMaybe<OrderBy>;
  parent?: InputMaybe<CallOrderBy>;
  parentId?: InputMaybe<OrderBy>;
  subcallsAggregate?: InputMaybe<CallAggregateOrderBy>;
  success?: InputMaybe<OrderBy>;
};

/** select columns of table "call" */
export enum CallSelectColumn {
  /** column name */
  Address = 'address',
  /** column name */
  Args = 'args',
  /** column name */
  ArgsStr = 'argsStr',
  /** column name */
  BlockId = 'blockId',
  /** column name */
  Error = 'error',
  /** column name */
  ExtrinsicId = 'extrinsicId',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  Pallet = 'pallet',
  /** column name */
  ParentId = 'parentId',
  /** column name */
  Success = 'success'
}

/** select "callAggregateBoolExpBool_andArgumentsColumns" columns of table "call" */
export enum CallSelectColumnCallAggregateBoolExpBool_AndArgumentsColumns {
  /** column name */
  Success = 'success'
}

/** select "callAggregateBoolExpBool_orArgumentsColumns" columns of table "call" */
export enum CallSelectColumnCallAggregateBoolExpBool_OrArgumentsColumns {
  /** column name */
  Success = 'success'
}

/** Streaming cursor of the table "call" */
export type CallStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: CallStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type CallStreamCursorValueInput = {
  address?: InputMaybe<Array<Scalars['Int']['input']>>;
  args?: InputMaybe<Scalars['jsonb']['input']>;
  argsStr?: InputMaybe<Array<Scalars['String']['input']>>;
  blockId?: InputMaybe<Scalars['String']['input']>;
  error?: InputMaybe<Scalars['jsonb']['input']>;
  extrinsicId?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  pallet?: InputMaybe<Scalars['String']['input']>;
  parentId?: InputMaybe<Scalars['String']['input']>;
  success?: InputMaybe<Scalars['Boolean']['input']>;
};

/** columns and relationships of "cert" */
export type Cert = {
  __typename?: 'Cert';
  /** An array relationship */
  certHistory: Array<CertEvent>;
  /** An aggregate relationship */
  certHistoryAggregate: CertEventAggregate;
  /** An object relationship */
  createdIn?: Maybe<Event>;
  createdInId?: Maybe<Scalars['String']['output']>;
  createdOn: Scalars['Int']['output'];
  expireOn: Scalars['Int']['output'];
  id: Scalars['String']['output'];
  isActive: Scalars['Boolean']['output'];
  /** An object relationship */
  issuer?: Maybe<Identity>;
  issuerId?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  receiver?: Maybe<Identity>;
  receiverId?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  updatedIn?: Maybe<Event>;
  updatedInId?: Maybe<Scalars['String']['output']>;
  updatedOn: Scalars['Int']['output'];
};


/** columns and relationships of "cert" */
export type CertCertHistoryArgs = {
  distinctOn?: InputMaybe<Array<CertEventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CertEventOrderBy>>;
  where?: InputMaybe<CertEventBoolExp>;
};


/** columns and relationships of "cert" */
export type CertCertHistoryAggregateArgs = {
  distinctOn?: InputMaybe<Array<CertEventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CertEventOrderBy>>;
  where?: InputMaybe<CertEventBoolExp>;
};

/** aggregated selection of "cert" */
export type CertAggregate = {
  __typename?: 'CertAggregate';
  aggregate?: Maybe<CertAggregateFields>;
  nodes: Array<Cert>;
};

export type CertAggregateBoolExp = {
  bool_and?: InputMaybe<CertAggregateBoolExpBool_And>;
  bool_or?: InputMaybe<CertAggregateBoolExpBool_Or>;
  count?: InputMaybe<CertAggregateBoolExpCount>;
};

/** aggregate fields of "cert" */
export type CertAggregateFields = {
  __typename?: 'CertAggregateFields';
  avg?: Maybe<CertAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<CertMaxFields>;
  min?: Maybe<CertMinFields>;
  stddev?: Maybe<CertStddevFields>;
  stddevPop?: Maybe<CertStddevPopFields>;
  stddevSamp?: Maybe<CertStddevSampFields>;
  sum?: Maybe<CertSumFields>;
  varPop?: Maybe<CertVarPopFields>;
  varSamp?: Maybe<CertVarSampFields>;
  variance?: Maybe<CertVarianceFields>;
};


/** aggregate fields of "cert" */
export type CertAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<CertSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "cert" */
export type CertAggregateOrderBy = {
  avg?: InputMaybe<CertAvgOrderBy>;
  count?: InputMaybe<OrderBy>;
  max?: InputMaybe<CertMaxOrderBy>;
  min?: InputMaybe<CertMinOrderBy>;
  stddev?: InputMaybe<CertStddevOrderBy>;
  stddevPop?: InputMaybe<CertStddevPopOrderBy>;
  stddevSamp?: InputMaybe<CertStddevSampOrderBy>;
  sum?: InputMaybe<CertSumOrderBy>;
  varPop?: InputMaybe<CertVarPopOrderBy>;
  varSamp?: InputMaybe<CertVarSampOrderBy>;
  variance?: InputMaybe<CertVarianceOrderBy>;
};

/** aggregate avg on columns */
export type CertAvgFields = {
  __typename?: 'CertAvgFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
  expireOn?: Maybe<Scalars['Float']['output']>;
  updatedOn?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "cert" */
export type CertAvgOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  updatedOn?: InputMaybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "cert". All fields are combined with a logical 'AND'. */
export type CertBoolExp = {
  _and?: InputMaybe<Array<CertBoolExp>>;
  _not?: InputMaybe<CertBoolExp>;
  _or?: InputMaybe<Array<CertBoolExp>>;
  certHistory?: InputMaybe<CertEventBoolExp>;
  certHistoryAggregate?: InputMaybe<CertEventAggregateBoolExp>;
  createdIn?: InputMaybe<EventBoolExp>;
  createdInId?: InputMaybe<StringComparisonExp>;
  createdOn?: InputMaybe<IntComparisonExp>;
  expireOn?: InputMaybe<IntComparisonExp>;
  id?: InputMaybe<StringComparisonExp>;
  isActive?: InputMaybe<BooleanComparisonExp>;
  issuer?: InputMaybe<IdentityBoolExp>;
  issuerId?: InputMaybe<StringComparisonExp>;
  receiver?: InputMaybe<IdentityBoolExp>;
  receiverId?: InputMaybe<StringComparisonExp>;
  updatedIn?: InputMaybe<EventBoolExp>;
  updatedInId?: InputMaybe<StringComparisonExp>;
  updatedOn?: InputMaybe<IntComparisonExp>;
};

/** columns and relationships of "cert_event" */
export type CertEvent = {
  __typename?: 'CertEvent';
  blockNumber: Scalars['Int']['output'];
  /** An object relationship */
  cert?: Maybe<Cert>;
  certId?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  event?: Maybe<Event>;
  eventId?: Maybe<Scalars['String']['output']>;
  eventType?: Maybe<EventTypeEnum>;
  id: Scalars['String']['output'];
};

/** aggregated selection of "cert_event" */
export type CertEventAggregate = {
  __typename?: 'CertEventAggregate';
  aggregate?: Maybe<CertEventAggregateFields>;
  nodes: Array<CertEvent>;
};

export type CertEventAggregateBoolExp = {
  count?: InputMaybe<CertEventAggregateBoolExpCount>;
};

/** aggregate fields of "cert_event" */
export type CertEventAggregateFields = {
  __typename?: 'CertEventAggregateFields';
  avg?: Maybe<CertEventAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<CertEventMaxFields>;
  min?: Maybe<CertEventMinFields>;
  stddev?: Maybe<CertEventStddevFields>;
  stddevPop?: Maybe<CertEventStddevPopFields>;
  stddevSamp?: Maybe<CertEventStddevSampFields>;
  sum?: Maybe<CertEventSumFields>;
  varPop?: Maybe<CertEventVarPopFields>;
  varSamp?: Maybe<CertEventVarSampFields>;
  variance?: Maybe<CertEventVarianceFields>;
};


/** aggregate fields of "cert_event" */
export type CertEventAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<CertEventSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "cert_event" */
export type CertEventAggregateOrderBy = {
  avg?: InputMaybe<CertEventAvgOrderBy>;
  count?: InputMaybe<OrderBy>;
  max?: InputMaybe<CertEventMaxOrderBy>;
  min?: InputMaybe<CertEventMinOrderBy>;
  stddev?: InputMaybe<CertEventStddevOrderBy>;
  stddevPop?: InputMaybe<CertEventStddevPopOrderBy>;
  stddevSamp?: InputMaybe<CertEventStddevSampOrderBy>;
  sum?: InputMaybe<CertEventSumOrderBy>;
  varPop?: InputMaybe<CertEventVarPopOrderBy>;
  varSamp?: InputMaybe<CertEventVarSampOrderBy>;
  variance?: InputMaybe<CertEventVarianceOrderBy>;
};

/** aggregate avg on columns */
export type CertEventAvgFields = {
  __typename?: 'CertEventAvgFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "cert_event" */
export type CertEventAvgOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "cert_event". All fields are combined with a logical 'AND'. */
export type CertEventBoolExp = {
  _and?: InputMaybe<Array<CertEventBoolExp>>;
  _not?: InputMaybe<CertEventBoolExp>;
  _or?: InputMaybe<Array<CertEventBoolExp>>;
  blockNumber?: InputMaybe<IntComparisonExp>;
  cert?: InputMaybe<CertBoolExp>;
  certId?: InputMaybe<StringComparisonExp>;
  event?: InputMaybe<EventBoolExp>;
  eventId?: InputMaybe<StringComparisonExp>;
  eventType?: InputMaybe<EventTypeEnumComparisonExp>;
  id?: InputMaybe<StringComparisonExp>;
};

/** aggregate max on columns */
export type CertEventMaxFields = {
  __typename?: 'CertEventMaxFields';
  blockNumber?: Maybe<Scalars['Int']['output']>;
  certId?: Maybe<Scalars['String']['output']>;
  eventId?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
};

/** order by max() on columns of table "cert_event" */
export type CertEventMaxOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
  certId?: InputMaybe<OrderBy>;
  eventId?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

/** aggregate min on columns */
export type CertEventMinFields = {
  __typename?: 'CertEventMinFields';
  blockNumber?: Maybe<Scalars['Int']['output']>;
  certId?: Maybe<Scalars['String']['output']>;
  eventId?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
};

/** order by min() on columns of table "cert_event" */
export type CertEventMinOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
  certId?: InputMaybe<OrderBy>;
  eventId?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

/** Ordering options when selecting data from "cert_event". */
export type CertEventOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
  cert?: InputMaybe<CertOrderBy>;
  certId?: InputMaybe<OrderBy>;
  event?: InputMaybe<EventOrderBy>;
  eventId?: InputMaybe<OrderBy>;
  eventType?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

/** select columns of table "cert_event" */
export enum CertEventSelectColumn {
  /** column name */
  BlockNumber = 'blockNumber',
  /** column name */
  CertId = 'certId',
  /** column name */
  EventId = 'eventId',
  /** column name */
  EventType = 'eventType',
  /** column name */
  Id = 'id'
}

/** aggregate stddev on columns */
export type CertEventStddevFields = {
  __typename?: 'CertEventStddevFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "cert_event" */
export type CertEventStddevOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate stddevPop on columns */
export type CertEventStddevPopFields = {
  __typename?: 'CertEventStddevPopFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevPop() on columns of table "cert_event" */
export type CertEventStddevPopOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate stddevSamp on columns */
export type CertEventStddevSampFields = {
  __typename?: 'CertEventStddevSampFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevSamp() on columns of table "cert_event" */
export type CertEventStddevSampOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** Streaming cursor of the table "cert_event" */
export type CertEventStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: CertEventStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type CertEventStreamCursorValueInput = {
  blockNumber?: InputMaybe<Scalars['Int']['input']>;
  certId?: InputMaybe<Scalars['String']['input']>;
  eventId?: InputMaybe<Scalars['String']['input']>;
  eventType?: InputMaybe<EventTypeEnum>;
  id?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate sum on columns */
export type CertEventSumFields = {
  __typename?: 'CertEventSumFields';
  blockNumber?: Maybe<Scalars['Int']['output']>;
};

/** order by sum() on columns of table "cert_event" */
export type CertEventSumOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate varPop on columns */
export type CertEventVarPopFields = {
  __typename?: 'CertEventVarPopFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by varPop() on columns of table "cert_event" */
export type CertEventVarPopOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate varSamp on columns */
export type CertEventVarSampFields = {
  __typename?: 'CertEventVarSampFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by varSamp() on columns of table "cert_event" */
export type CertEventVarSampOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate variance on columns */
export type CertEventVarianceFields = {
  __typename?: 'CertEventVarianceFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "cert_event" */
export type CertEventVarianceOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate max on columns */
export type CertMaxFields = {
  __typename?: 'CertMaxFields';
  createdInId?: Maybe<Scalars['String']['output']>;
  createdOn?: Maybe<Scalars['Int']['output']>;
  expireOn?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  issuerId?: Maybe<Scalars['String']['output']>;
  receiverId?: Maybe<Scalars['String']['output']>;
  updatedInId?: Maybe<Scalars['String']['output']>;
  updatedOn?: Maybe<Scalars['Int']['output']>;
};

/** order by max() on columns of table "cert" */
export type CertMaxOrderBy = {
  createdInId?: InputMaybe<OrderBy>;
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  issuerId?: InputMaybe<OrderBy>;
  receiverId?: InputMaybe<OrderBy>;
  updatedInId?: InputMaybe<OrderBy>;
  updatedOn?: InputMaybe<OrderBy>;
};

/** aggregate min on columns */
export type CertMinFields = {
  __typename?: 'CertMinFields';
  createdInId?: Maybe<Scalars['String']['output']>;
  createdOn?: Maybe<Scalars['Int']['output']>;
  expireOn?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  issuerId?: Maybe<Scalars['String']['output']>;
  receiverId?: Maybe<Scalars['String']['output']>;
  updatedInId?: Maybe<Scalars['String']['output']>;
  updatedOn?: Maybe<Scalars['Int']['output']>;
};

/** order by min() on columns of table "cert" */
export type CertMinOrderBy = {
  createdInId?: InputMaybe<OrderBy>;
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  issuerId?: InputMaybe<OrderBy>;
  receiverId?: InputMaybe<OrderBy>;
  updatedInId?: InputMaybe<OrderBy>;
  updatedOn?: InputMaybe<OrderBy>;
};

/** Ordering options when selecting data from "cert". */
export type CertOrderBy = {
  certHistoryAggregate?: InputMaybe<CertEventAggregateOrderBy>;
  createdIn?: InputMaybe<EventOrderBy>;
  createdInId?: InputMaybe<OrderBy>;
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  isActive?: InputMaybe<OrderBy>;
  issuer?: InputMaybe<IdentityOrderBy>;
  issuerId?: InputMaybe<OrderBy>;
  receiver?: InputMaybe<IdentityOrderBy>;
  receiverId?: InputMaybe<OrderBy>;
  updatedIn?: InputMaybe<EventOrderBy>;
  updatedInId?: InputMaybe<OrderBy>;
  updatedOn?: InputMaybe<OrderBy>;
};

/** select columns of table "cert" */
export enum CertSelectColumn {
  /** column name */
  CreatedInId = 'createdInId',
  /** column name */
  CreatedOn = 'createdOn',
  /** column name */
  ExpireOn = 'expireOn',
  /** column name */
  Id = 'id',
  /** column name */
  IsActive = 'isActive',
  /** column name */
  IssuerId = 'issuerId',
  /** column name */
  ReceiverId = 'receiverId',
  /** column name */
  UpdatedInId = 'updatedInId',
  /** column name */
  UpdatedOn = 'updatedOn'
}

/** select "certAggregateBoolExpBool_andArgumentsColumns" columns of table "cert" */
export enum CertSelectColumnCertAggregateBoolExpBool_AndArgumentsColumns {
  /** column name */
  IsActive = 'isActive'
}

/** select "certAggregateBoolExpBool_orArgumentsColumns" columns of table "cert" */
export enum CertSelectColumnCertAggregateBoolExpBool_OrArgumentsColumns {
  /** column name */
  IsActive = 'isActive'
}

/** aggregate stddev on columns */
export type CertStddevFields = {
  __typename?: 'CertStddevFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
  expireOn?: Maybe<Scalars['Float']['output']>;
  updatedOn?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "cert" */
export type CertStddevOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  updatedOn?: InputMaybe<OrderBy>;
};

/** aggregate stddevPop on columns */
export type CertStddevPopFields = {
  __typename?: 'CertStddevPopFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
  expireOn?: Maybe<Scalars['Float']['output']>;
  updatedOn?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevPop() on columns of table "cert" */
export type CertStddevPopOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  updatedOn?: InputMaybe<OrderBy>;
};

/** aggregate stddevSamp on columns */
export type CertStddevSampFields = {
  __typename?: 'CertStddevSampFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
  expireOn?: Maybe<Scalars['Float']['output']>;
  updatedOn?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevSamp() on columns of table "cert" */
export type CertStddevSampOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  updatedOn?: InputMaybe<OrderBy>;
};

/** Streaming cursor of the table "cert" */
export type CertStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: CertStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type CertStreamCursorValueInput = {
  createdInId?: InputMaybe<Scalars['String']['input']>;
  createdOn?: InputMaybe<Scalars['Int']['input']>;
  expireOn?: InputMaybe<Scalars['Int']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  isActive?: InputMaybe<Scalars['Boolean']['input']>;
  issuerId?: InputMaybe<Scalars['String']['input']>;
  receiverId?: InputMaybe<Scalars['String']['input']>;
  updatedInId?: InputMaybe<Scalars['String']['input']>;
  updatedOn?: InputMaybe<Scalars['Int']['input']>;
};

/** aggregate sum on columns */
export type CertSumFields = {
  __typename?: 'CertSumFields';
  createdOn?: Maybe<Scalars['Int']['output']>;
  expireOn?: Maybe<Scalars['Int']['output']>;
  updatedOn?: Maybe<Scalars['Int']['output']>;
};

/** order by sum() on columns of table "cert" */
export type CertSumOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  updatedOn?: InputMaybe<OrderBy>;
};

/** aggregate varPop on columns */
export type CertVarPopFields = {
  __typename?: 'CertVarPopFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
  expireOn?: Maybe<Scalars['Float']['output']>;
  updatedOn?: Maybe<Scalars['Float']['output']>;
};

/** order by varPop() on columns of table "cert" */
export type CertVarPopOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  updatedOn?: InputMaybe<OrderBy>;
};

/** aggregate varSamp on columns */
export type CertVarSampFields = {
  __typename?: 'CertVarSampFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
  expireOn?: Maybe<Scalars['Float']['output']>;
  updatedOn?: Maybe<Scalars['Float']['output']>;
};

/** order by varSamp() on columns of table "cert" */
export type CertVarSampOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  updatedOn?: InputMaybe<OrderBy>;
};

/** aggregate variance on columns */
export type CertVarianceFields = {
  __typename?: 'CertVarianceFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
  expireOn?: Maybe<Scalars['Float']['output']>;
  updatedOn?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "cert" */
export type CertVarianceOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  updatedOn?: InputMaybe<OrderBy>;
};

/** columns and relationships of "change_owner_key" */
export type ChangeOwnerKey = {
  __typename?: 'ChangeOwnerKey';
  blockNumber: Scalars['Int']['output'];
  id: Scalars['String']['output'];
  /** An object relationship */
  identity?: Maybe<Identity>;
  identityId?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  next?: Maybe<Account>;
  nextId?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  previous?: Maybe<Account>;
  previousId?: Maybe<Scalars['String']['output']>;
};

/** aggregated selection of "change_owner_key" */
export type ChangeOwnerKeyAggregate = {
  __typename?: 'ChangeOwnerKeyAggregate';
  aggregate?: Maybe<ChangeOwnerKeyAggregateFields>;
  nodes: Array<ChangeOwnerKey>;
};

export type ChangeOwnerKeyAggregateBoolExp = {
  count?: InputMaybe<ChangeOwnerKeyAggregateBoolExpCount>;
};

/** aggregate fields of "change_owner_key" */
export type ChangeOwnerKeyAggregateFields = {
  __typename?: 'ChangeOwnerKeyAggregateFields';
  avg?: Maybe<ChangeOwnerKeyAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<ChangeOwnerKeyMaxFields>;
  min?: Maybe<ChangeOwnerKeyMinFields>;
  stddev?: Maybe<ChangeOwnerKeyStddevFields>;
  stddevPop?: Maybe<ChangeOwnerKeyStddevPopFields>;
  stddevSamp?: Maybe<ChangeOwnerKeyStddevSampFields>;
  sum?: Maybe<ChangeOwnerKeySumFields>;
  varPop?: Maybe<ChangeOwnerKeyVarPopFields>;
  varSamp?: Maybe<ChangeOwnerKeyVarSampFields>;
  variance?: Maybe<ChangeOwnerKeyVarianceFields>;
};


/** aggregate fields of "change_owner_key" */
export type ChangeOwnerKeyAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<ChangeOwnerKeySelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "change_owner_key" */
export type ChangeOwnerKeyAggregateOrderBy = {
  avg?: InputMaybe<ChangeOwnerKeyAvgOrderBy>;
  count?: InputMaybe<OrderBy>;
  max?: InputMaybe<ChangeOwnerKeyMaxOrderBy>;
  min?: InputMaybe<ChangeOwnerKeyMinOrderBy>;
  stddev?: InputMaybe<ChangeOwnerKeyStddevOrderBy>;
  stddevPop?: InputMaybe<ChangeOwnerKeyStddevPopOrderBy>;
  stddevSamp?: InputMaybe<ChangeOwnerKeyStddevSampOrderBy>;
  sum?: InputMaybe<ChangeOwnerKeySumOrderBy>;
  varPop?: InputMaybe<ChangeOwnerKeyVarPopOrderBy>;
  varSamp?: InputMaybe<ChangeOwnerKeyVarSampOrderBy>;
  variance?: InputMaybe<ChangeOwnerKeyVarianceOrderBy>;
};

/** aggregate avg on columns */
export type ChangeOwnerKeyAvgFields = {
  __typename?: 'ChangeOwnerKeyAvgFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "change_owner_key" */
export type ChangeOwnerKeyAvgOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "change_owner_key". All fields are combined with a logical 'AND'. */
export type ChangeOwnerKeyBoolExp = {
  _and?: InputMaybe<Array<ChangeOwnerKeyBoolExp>>;
  _not?: InputMaybe<ChangeOwnerKeyBoolExp>;
  _or?: InputMaybe<Array<ChangeOwnerKeyBoolExp>>;
  blockNumber?: InputMaybe<IntComparisonExp>;
  id?: InputMaybe<StringComparisonExp>;
  identity?: InputMaybe<IdentityBoolExp>;
  identityId?: InputMaybe<StringComparisonExp>;
  next?: InputMaybe<AccountBoolExp>;
  nextId?: InputMaybe<StringComparisonExp>;
  previous?: InputMaybe<AccountBoolExp>;
  previousId?: InputMaybe<StringComparisonExp>;
};

/** aggregate max on columns */
export type ChangeOwnerKeyMaxFields = {
  __typename?: 'ChangeOwnerKeyMaxFields';
  blockNumber?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  identityId?: Maybe<Scalars['String']['output']>;
  nextId?: Maybe<Scalars['String']['output']>;
  previousId?: Maybe<Scalars['String']['output']>;
};

/** order by max() on columns of table "change_owner_key" */
export type ChangeOwnerKeyMaxOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  identityId?: InputMaybe<OrderBy>;
  nextId?: InputMaybe<OrderBy>;
  previousId?: InputMaybe<OrderBy>;
};

/** aggregate min on columns */
export type ChangeOwnerKeyMinFields = {
  __typename?: 'ChangeOwnerKeyMinFields';
  blockNumber?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  identityId?: Maybe<Scalars['String']['output']>;
  nextId?: Maybe<Scalars['String']['output']>;
  previousId?: Maybe<Scalars['String']['output']>;
};

/** order by min() on columns of table "change_owner_key" */
export type ChangeOwnerKeyMinOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  identityId?: InputMaybe<OrderBy>;
  nextId?: InputMaybe<OrderBy>;
  previousId?: InputMaybe<OrderBy>;
};

/** Ordering options when selecting data from "change_owner_key". */
export type ChangeOwnerKeyOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  identity?: InputMaybe<IdentityOrderBy>;
  identityId?: InputMaybe<OrderBy>;
  next?: InputMaybe<AccountOrderBy>;
  nextId?: InputMaybe<OrderBy>;
  previous?: InputMaybe<AccountOrderBy>;
  previousId?: InputMaybe<OrderBy>;
};

/** select columns of table "change_owner_key" */
export enum ChangeOwnerKeySelectColumn {
  /** column name */
  BlockNumber = 'blockNumber',
  /** column name */
  Id = 'id',
  /** column name */
  IdentityId = 'identityId',
  /** column name */
  NextId = 'nextId',
  /** column name */
  PreviousId = 'previousId'
}

/** aggregate stddev on columns */
export type ChangeOwnerKeyStddevFields = {
  __typename?: 'ChangeOwnerKeyStddevFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "change_owner_key" */
export type ChangeOwnerKeyStddevOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate stddevPop on columns */
export type ChangeOwnerKeyStddevPopFields = {
  __typename?: 'ChangeOwnerKeyStddevPopFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevPop() on columns of table "change_owner_key" */
export type ChangeOwnerKeyStddevPopOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate stddevSamp on columns */
export type ChangeOwnerKeyStddevSampFields = {
  __typename?: 'ChangeOwnerKeyStddevSampFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevSamp() on columns of table "change_owner_key" */
export type ChangeOwnerKeyStddevSampOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** Streaming cursor of the table "change_owner_key" */
export type ChangeOwnerKeyStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: ChangeOwnerKeyStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type ChangeOwnerKeyStreamCursorValueInput = {
  blockNumber?: InputMaybe<Scalars['Int']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  identityId?: InputMaybe<Scalars['String']['input']>;
  nextId?: InputMaybe<Scalars['String']['input']>;
  previousId?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate sum on columns */
export type ChangeOwnerKeySumFields = {
  __typename?: 'ChangeOwnerKeySumFields';
  blockNumber?: Maybe<Scalars['Int']['output']>;
};

/** order by sum() on columns of table "change_owner_key" */
export type ChangeOwnerKeySumOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate varPop on columns */
export type ChangeOwnerKeyVarPopFields = {
  __typename?: 'ChangeOwnerKeyVarPopFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by varPop() on columns of table "change_owner_key" */
export type ChangeOwnerKeyVarPopOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate varSamp on columns */
export type ChangeOwnerKeyVarSampFields = {
  __typename?: 'ChangeOwnerKeyVarSampFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by varSamp() on columns of table "change_owner_key" */
export type ChangeOwnerKeyVarSampOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate variance on columns */
export type ChangeOwnerKeyVarianceFields = {
  __typename?: 'ChangeOwnerKeyVarianceFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "change_owner_key" */
export type ChangeOwnerKeyVarianceOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

export enum CommentTypeEnum {
  Ascii = 'ASCII',
  Cid = 'CID',
  Raw = 'RAW',
  Unicode = 'UNICODE'
}

/** Boolean expression to compare columns of type "CommentTypeEnum". All fields are combined with logical 'AND'. */
export type CommentTypeEnumComparisonExp = {
  _eq?: InputMaybe<CommentTypeEnum>;
  _in?: InputMaybe<Array<CommentTypeEnum>>;
  _isNull?: InputMaybe<Scalars['Boolean']['input']>;
  _neq?: InputMaybe<CommentTypeEnum>;
  _nin?: InputMaybe<Array<CommentTypeEnum>>;
};

export enum CounterLevelEnum {
  Global = 'GLOBAL',
  Item = 'ITEM',
  Pallet = 'PALLET'
}

/** Boolean expression to compare columns of type "CounterLevelEnum". All fields are combined with logical 'AND'. */
export type CounterLevelEnumComparisonExp = {
  _eq?: InputMaybe<CounterLevelEnum>;
  _in?: InputMaybe<Array<CounterLevelEnum>>;
  _isNull?: InputMaybe<Scalars['Boolean']['input']>;
  _neq?: InputMaybe<CounterLevelEnum>;
  _nin?: InputMaybe<Array<CounterLevelEnum>>;
};

/** ordering argument of a cursor */
export enum CursorOrdering {
  /** ascending ordering of the cursor */
  Asc = 'ASC',
  /** descending ordering of the cursor */
  Desc = 'DESC'
}

/** columns and relationships of "event" */
export type Event = {
  __typename?: 'Event';
  args?: Maybe<Scalars['jsonb']['output']>;
  argsStr?: Maybe<Array<Scalars['String']['output']>>;
  /** An object relationship */
  block?: Maybe<Block>;
  blockId?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  call?: Maybe<Call>;
  callId?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  extrinsic?: Maybe<Extrinsic>;
  extrinsicId?: Maybe<Scalars['String']['output']>;
  id: Scalars['String']['output'];
  index: Scalars['Int']['output'];
  name: Scalars['String']['output'];
  pallet: Scalars['String']['output'];
  phase: Scalars['String']['output'];
};


/** columns and relationships of "event" */
export type EventArgsArgs = {
  path?: InputMaybe<Scalars['String']['input']>;
};

/** aggregated selection of "event" */
export type EventAggregate = {
  __typename?: 'EventAggregate';
  aggregate?: Maybe<EventAggregateFields>;
  nodes: Array<Event>;
};

export type EventAggregateBoolExp = {
  count?: InputMaybe<EventAggregateBoolExpCount>;
};

/** aggregate fields of "event" */
export type EventAggregateFields = {
  __typename?: 'EventAggregateFields';
  avg?: Maybe<EventAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<EventMaxFields>;
  min?: Maybe<EventMinFields>;
  stddev?: Maybe<EventStddevFields>;
  stddevPop?: Maybe<EventStddevPopFields>;
  stddevSamp?: Maybe<EventStddevSampFields>;
  sum?: Maybe<EventSumFields>;
  varPop?: Maybe<EventVarPopFields>;
  varSamp?: Maybe<EventVarSampFields>;
  variance?: Maybe<EventVarianceFields>;
};


/** aggregate fields of "event" */
export type EventAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<EventSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "event" */
export type EventAggregateOrderBy = {
  avg?: InputMaybe<EventAvgOrderBy>;
  count?: InputMaybe<OrderBy>;
  max?: InputMaybe<EventMaxOrderBy>;
  min?: InputMaybe<EventMinOrderBy>;
  stddev?: InputMaybe<EventStddevOrderBy>;
  stddevPop?: InputMaybe<EventStddevPopOrderBy>;
  stddevSamp?: InputMaybe<EventStddevSampOrderBy>;
  sum?: InputMaybe<EventSumOrderBy>;
  varPop?: InputMaybe<EventVarPopOrderBy>;
  varSamp?: InputMaybe<EventVarSampOrderBy>;
  variance?: InputMaybe<EventVarianceOrderBy>;
};

/** aggregate avg on columns */
export type EventAvgFields = {
  __typename?: 'EventAvgFields';
  index?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "event" */
export type EventAvgOrderBy = {
  index?: InputMaybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "event". All fields are combined with a logical 'AND'. */
export type EventBoolExp = {
  _and?: InputMaybe<Array<EventBoolExp>>;
  _not?: InputMaybe<EventBoolExp>;
  _or?: InputMaybe<Array<EventBoolExp>>;
  args?: InputMaybe<JsonbComparisonExp>;
  argsStr?: InputMaybe<StringArrayComparisonExp>;
  block?: InputMaybe<BlockBoolExp>;
  blockId?: InputMaybe<StringComparisonExp>;
  call?: InputMaybe<CallBoolExp>;
  callId?: InputMaybe<StringComparisonExp>;
  extrinsic?: InputMaybe<ExtrinsicBoolExp>;
  extrinsicId?: InputMaybe<StringComparisonExp>;
  id?: InputMaybe<StringComparisonExp>;
  index?: InputMaybe<IntComparisonExp>;
  name?: InputMaybe<StringComparisonExp>;
  pallet?: InputMaybe<StringComparisonExp>;
  phase?: InputMaybe<StringComparisonExp>;
};

/** aggregate max on columns */
export type EventMaxFields = {
  __typename?: 'EventMaxFields';
  argsStr?: Maybe<Array<Scalars['String']['output']>>;
  blockId?: Maybe<Scalars['String']['output']>;
  callId?: Maybe<Scalars['String']['output']>;
  extrinsicId?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  index?: Maybe<Scalars['Int']['output']>;
  name?: Maybe<Scalars['String']['output']>;
  pallet?: Maybe<Scalars['String']['output']>;
  phase?: Maybe<Scalars['String']['output']>;
};

/** order by max() on columns of table "event" */
export type EventMaxOrderBy = {
  argsStr?: InputMaybe<OrderBy>;
  blockId?: InputMaybe<OrderBy>;
  callId?: InputMaybe<OrderBy>;
  extrinsicId?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  name?: InputMaybe<OrderBy>;
  pallet?: InputMaybe<OrderBy>;
  phase?: InputMaybe<OrderBy>;
};

/** aggregate min on columns */
export type EventMinFields = {
  __typename?: 'EventMinFields';
  argsStr?: Maybe<Array<Scalars['String']['output']>>;
  blockId?: Maybe<Scalars['String']['output']>;
  callId?: Maybe<Scalars['String']['output']>;
  extrinsicId?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  index?: Maybe<Scalars['Int']['output']>;
  name?: Maybe<Scalars['String']['output']>;
  pallet?: Maybe<Scalars['String']['output']>;
  phase?: Maybe<Scalars['String']['output']>;
};

/** order by min() on columns of table "event" */
export type EventMinOrderBy = {
  argsStr?: InputMaybe<OrderBy>;
  blockId?: InputMaybe<OrderBy>;
  callId?: InputMaybe<OrderBy>;
  extrinsicId?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  name?: InputMaybe<OrderBy>;
  pallet?: InputMaybe<OrderBy>;
  phase?: InputMaybe<OrderBy>;
};

/** Ordering options when selecting data from "event". */
export type EventOrderBy = {
  args?: InputMaybe<OrderBy>;
  argsStr?: InputMaybe<OrderBy>;
  block?: InputMaybe<BlockOrderBy>;
  blockId?: InputMaybe<OrderBy>;
  call?: InputMaybe<CallOrderBy>;
  callId?: InputMaybe<OrderBy>;
  extrinsic?: InputMaybe<ExtrinsicOrderBy>;
  extrinsicId?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  name?: InputMaybe<OrderBy>;
  pallet?: InputMaybe<OrderBy>;
  phase?: InputMaybe<OrderBy>;
};

/** select columns of table "event" */
export enum EventSelectColumn {
  /** column name */
  Args = 'args',
  /** column name */
  ArgsStr = 'argsStr',
  /** column name */
  BlockId = 'blockId',
  /** column name */
  CallId = 'callId',
  /** column name */
  ExtrinsicId = 'extrinsicId',
  /** column name */
  Id = 'id',
  /** column name */
  Index = 'index',
  /** column name */
  Name = 'name',
  /** column name */
  Pallet = 'pallet',
  /** column name */
  Phase = 'phase'
}

/** aggregate stddev on columns */
export type EventStddevFields = {
  __typename?: 'EventStddevFields';
  index?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "event" */
export type EventStddevOrderBy = {
  index?: InputMaybe<OrderBy>;
};

/** aggregate stddevPop on columns */
export type EventStddevPopFields = {
  __typename?: 'EventStddevPopFields';
  index?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevPop() on columns of table "event" */
export type EventStddevPopOrderBy = {
  index?: InputMaybe<OrderBy>;
};

/** aggregate stddevSamp on columns */
export type EventStddevSampFields = {
  __typename?: 'EventStddevSampFields';
  index?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevSamp() on columns of table "event" */
export type EventStddevSampOrderBy = {
  index?: InputMaybe<OrderBy>;
};

/** Streaming cursor of the table "event" */
export type EventStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: EventStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type EventStreamCursorValueInput = {
  args?: InputMaybe<Scalars['jsonb']['input']>;
  argsStr?: InputMaybe<Array<Scalars['String']['input']>>;
  blockId?: InputMaybe<Scalars['String']['input']>;
  callId?: InputMaybe<Scalars['String']['input']>;
  extrinsicId?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  index?: InputMaybe<Scalars['Int']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  pallet?: InputMaybe<Scalars['String']['input']>;
  phase?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate sum on columns */
export type EventSumFields = {
  __typename?: 'EventSumFields';
  index?: Maybe<Scalars['Int']['output']>;
};

/** order by sum() on columns of table "event" */
export type EventSumOrderBy = {
  index?: InputMaybe<OrderBy>;
};

export enum EventTypeEnum {
  Creation = 'CREATION',
  Removal = 'REMOVAL',
  Renewal = 'RENEWAL'
}

/** Boolean expression to compare columns of type "EventTypeEnum". All fields are combined with logical 'AND'. */
export type EventTypeEnumComparisonExp = {
  _eq?: InputMaybe<EventTypeEnum>;
  _in?: InputMaybe<Array<EventTypeEnum>>;
  _isNull?: InputMaybe<Scalars['Boolean']['input']>;
  _neq?: InputMaybe<EventTypeEnum>;
  _nin?: InputMaybe<Array<EventTypeEnum>>;
};

/** aggregate varPop on columns */
export type EventVarPopFields = {
  __typename?: 'EventVarPopFields';
  index?: Maybe<Scalars['Float']['output']>;
};

/** order by varPop() on columns of table "event" */
export type EventVarPopOrderBy = {
  index?: InputMaybe<OrderBy>;
};

/** aggregate varSamp on columns */
export type EventVarSampFields = {
  __typename?: 'EventVarSampFields';
  index?: Maybe<Scalars['Float']['output']>;
};

/** order by varSamp() on columns of table "event" */
export type EventVarSampOrderBy = {
  index?: InputMaybe<OrderBy>;
};

/** aggregate variance on columns */
export type EventVarianceFields = {
  __typename?: 'EventVarianceFields';
  index?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "event" */
export type EventVarianceOrderBy = {
  index?: InputMaybe<OrderBy>;
};

/** columns and relationships of "extrinsic" */
export type Extrinsic = {
  __typename?: 'Extrinsic';
  /** An object relationship */
  block?: Maybe<Block>;
  blockId?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  call?: Maybe<Call>;
  callId?: Maybe<Scalars['String']['output']>;
  /** An array relationship */
  calls: Array<Call>;
  /** An aggregate relationship */
  callsAggregate: CallAggregate;
  error?: Maybe<Scalars['jsonb']['output']>;
  /** An array relationship */
  events: Array<Event>;
  /** An aggregate relationship */
  eventsAggregate: EventAggregate;
  fee?: Maybe<Scalars['numeric']['output']>;
  hash: Scalars['bytea']['output'];
  id: Scalars['String']['output'];
  index: Scalars['Int']['output'];
  signature?: Maybe<Scalars['jsonb']['output']>;
  success?: Maybe<Scalars['Boolean']['output']>;
  tip?: Maybe<Scalars['numeric']['output']>;
  version: Scalars['Int']['output'];
};


/** columns and relationships of "extrinsic" */
export type ExtrinsicCallsArgs = {
  distinctOn?: InputMaybe<Array<CallSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CallOrderBy>>;
  where?: InputMaybe<CallBoolExp>;
};


/** columns and relationships of "extrinsic" */
export type ExtrinsicCallsAggregateArgs = {
  distinctOn?: InputMaybe<Array<CallSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CallOrderBy>>;
  where?: InputMaybe<CallBoolExp>;
};


/** columns and relationships of "extrinsic" */
export type ExtrinsicErrorArgs = {
  path?: InputMaybe<Scalars['String']['input']>;
};


/** columns and relationships of "extrinsic" */
export type ExtrinsicEventsArgs = {
  distinctOn?: InputMaybe<Array<EventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<EventOrderBy>>;
  where?: InputMaybe<EventBoolExp>;
};


/** columns and relationships of "extrinsic" */
export type ExtrinsicEventsAggregateArgs = {
  distinctOn?: InputMaybe<Array<EventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<EventOrderBy>>;
  where?: InputMaybe<EventBoolExp>;
};


/** columns and relationships of "extrinsic" */
export type ExtrinsicSignatureArgs = {
  path?: InputMaybe<Scalars['String']['input']>;
};

/** aggregated selection of "extrinsic" */
export type ExtrinsicAggregate = {
  __typename?: 'ExtrinsicAggregate';
  aggregate?: Maybe<ExtrinsicAggregateFields>;
  nodes: Array<Extrinsic>;
};

export type ExtrinsicAggregateBoolExp = {
  bool_and?: InputMaybe<ExtrinsicAggregateBoolExpBool_And>;
  bool_or?: InputMaybe<ExtrinsicAggregateBoolExpBool_Or>;
  count?: InputMaybe<ExtrinsicAggregateBoolExpCount>;
};

/** aggregate fields of "extrinsic" */
export type ExtrinsicAggregateFields = {
  __typename?: 'ExtrinsicAggregateFields';
  avg?: Maybe<ExtrinsicAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<ExtrinsicMaxFields>;
  min?: Maybe<ExtrinsicMinFields>;
  stddev?: Maybe<ExtrinsicStddevFields>;
  stddevPop?: Maybe<ExtrinsicStddevPopFields>;
  stddevSamp?: Maybe<ExtrinsicStddevSampFields>;
  sum?: Maybe<ExtrinsicSumFields>;
  varPop?: Maybe<ExtrinsicVarPopFields>;
  varSamp?: Maybe<ExtrinsicVarSampFields>;
  variance?: Maybe<ExtrinsicVarianceFields>;
};


/** aggregate fields of "extrinsic" */
export type ExtrinsicAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<ExtrinsicSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "extrinsic" */
export type ExtrinsicAggregateOrderBy = {
  avg?: InputMaybe<ExtrinsicAvgOrderBy>;
  count?: InputMaybe<OrderBy>;
  max?: InputMaybe<ExtrinsicMaxOrderBy>;
  min?: InputMaybe<ExtrinsicMinOrderBy>;
  stddev?: InputMaybe<ExtrinsicStddevOrderBy>;
  stddevPop?: InputMaybe<ExtrinsicStddevPopOrderBy>;
  stddevSamp?: InputMaybe<ExtrinsicStddevSampOrderBy>;
  sum?: InputMaybe<ExtrinsicSumOrderBy>;
  varPop?: InputMaybe<ExtrinsicVarPopOrderBy>;
  varSamp?: InputMaybe<ExtrinsicVarSampOrderBy>;
  variance?: InputMaybe<ExtrinsicVarianceOrderBy>;
};

/** aggregate avg on columns */
export type ExtrinsicAvgFields = {
  __typename?: 'ExtrinsicAvgFields';
  fee?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  tip?: Maybe<Scalars['Float']['output']>;
  version?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "extrinsic" */
export type ExtrinsicAvgOrderBy = {
  fee?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  tip?: InputMaybe<OrderBy>;
  version?: InputMaybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "extrinsic". All fields are combined with a logical 'AND'. */
export type ExtrinsicBoolExp = {
  _and?: InputMaybe<Array<ExtrinsicBoolExp>>;
  _not?: InputMaybe<ExtrinsicBoolExp>;
  _or?: InputMaybe<Array<ExtrinsicBoolExp>>;
  block?: InputMaybe<BlockBoolExp>;
  blockId?: InputMaybe<StringComparisonExp>;
  call?: InputMaybe<CallBoolExp>;
  callId?: InputMaybe<StringComparisonExp>;
  calls?: InputMaybe<CallBoolExp>;
  callsAggregate?: InputMaybe<CallAggregateBoolExp>;
  error?: InputMaybe<JsonbComparisonExp>;
  events?: InputMaybe<EventBoolExp>;
  eventsAggregate?: InputMaybe<EventAggregateBoolExp>;
  fee?: InputMaybe<NumericComparisonExp>;
  hash?: InputMaybe<ByteaComparisonExp>;
  id?: InputMaybe<StringComparisonExp>;
  index?: InputMaybe<IntComparisonExp>;
  signature?: InputMaybe<JsonbComparisonExp>;
  success?: InputMaybe<BooleanComparisonExp>;
  tip?: InputMaybe<NumericComparisonExp>;
  version?: InputMaybe<IntComparisonExp>;
};

/** aggregate max on columns */
export type ExtrinsicMaxFields = {
  __typename?: 'ExtrinsicMaxFields';
  blockId?: Maybe<Scalars['String']['output']>;
  callId?: Maybe<Scalars['String']['output']>;
  fee?: Maybe<Scalars['numeric']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  index?: Maybe<Scalars['Int']['output']>;
  tip?: Maybe<Scalars['numeric']['output']>;
  version?: Maybe<Scalars['Int']['output']>;
};

/** order by max() on columns of table "extrinsic" */
export type ExtrinsicMaxOrderBy = {
  blockId?: InputMaybe<OrderBy>;
  callId?: InputMaybe<OrderBy>;
  fee?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  tip?: InputMaybe<OrderBy>;
  version?: InputMaybe<OrderBy>;
};

/** aggregate min on columns */
export type ExtrinsicMinFields = {
  __typename?: 'ExtrinsicMinFields';
  blockId?: Maybe<Scalars['String']['output']>;
  callId?: Maybe<Scalars['String']['output']>;
  fee?: Maybe<Scalars['numeric']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  index?: Maybe<Scalars['Int']['output']>;
  tip?: Maybe<Scalars['numeric']['output']>;
  version?: Maybe<Scalars['Int']['output']>;
};

/** order by min() on columns of table "extrinsic" */
export type ExtrinsicMinOrderBy = {
  blockId?: InputMaybe<OrderBy>;
  callId?: InputMaybe<OrderBy>;
  fee?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  tip?: InputMaybe<OrderBy>;
  version?: InputMaybe<OrderBy>;
};

/** Ordering options when selecting data from "extrinsic". */
export type ExtrinsicOrderBy = {
  block?: InputMaybe<BlockOrderBy>;
  blockId?: InputMaybe<OrderBy>;
  call?: InputMaybe<CallOrderBy>;
  callId?: InputMaybe<OrderBy>;
  callsAggregate?: InputMaybe<CallAggregateOrderBy>;
  error?: InputMaybe<OrderBy>;
  eventsAggregate?: InputMaybe<EventAggregateOrderBy>;
  fee?: InputMaybe<OrderBy>;
  hash?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  signature?: InputMaybe<OrderBy>;
  success?: InputMaybe<OrderBy>;
  tip?: InputMaybe<OrderBy>;
  version?: InputMaybe<OrderBy>;
};

/** select columns of table "extrinsic" */
export enum ExtrinsicSelectColumn {
  /** column name */
  BlockId = 'blockId',
  /** column name */
  CallId = 'callId',
  /** column name */
  Error = 'error',
  /** column name */
  Fee = 'fee',
  /** column name */
  Hash = 'hash',
  /** column name */
  Id = 'id',
  /** column name */
  Index = 'index',
  /** column name */
  Signature = 'signature',
  /** column name */
  Success = 'success',
  /** column name */
  Tip = 'tip',
  /** column name */
  Version = 'version'
}

/** select "extrinsicAggregateBoolExpBool_andArgumentsColumns" columns of table "extrinsic" */
export enum ExtrinsicSelectColumnExtrinsicAggregateBoolExpBool_AndArgumentsColumns {
  /** column name */
  Success = 'success'
}

/** select "extrinsicAggregateBoolExpBool_orArgumentsColumns" columns of table "extrinsic" */
export enum ExtrinsicSelectColumnExtrinsicAggregateBoolExpBool_OrArgumentsColumns {
  /** column name */
  Success = 'success'
}

/** aggregate stddev on columns */
export type ExtrinsicStddevFields = {
  __typename?: 'ExtrinsicStddevFields';
  fee?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  tip?: Maybe<Scalars['Float']['output']>;
  version?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "extrinsic" */
export type ExtrinsicStddevOrderBy = {
  fee?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  tip?: InputMaybe<OrderBy>;
  version?: InputMaybe<OrderBy>;
};

/** aggregate stddevPop on columns */
export type ExtrinsicStddevPopFields = {
  __typename?: 'ExtrinsicStddevPopFields';
  fee?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  tip?: Maybe<Scalars['Float']['output']>;
  version?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevPop() on columns of table "extrinsic" */
export type ExtrinsicStddevPopOrderBy = {
  fee?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  tip?: InputMaybe<OrderBy>;
  version?: InputMaybe<OrderBy>;
};

/** aggregate stddevSamp on columns */
export type ExtrinsicStddevSampFields = {
  __typename?: 'ExtrinsicStddevSampFields';
  fee?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  tip?: Maybe<Scalars['Float']['output']>;
  version?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevSamp() on columns of table "extrinsic" */
export type ExtrinsicStddevSampOrderBy = {
  fee?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  tip?: InputMaybe<OrderBy>;
  version?: InputMaybe<OrderBy>;
};

/** Streaming cursor of the table "extrinsic" */
export type ExtrinsicStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: ExtrinsicStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type ExtrinsicStreamCursorValueInput = {
  blockId?: InputMaybe<Scalars['String']['input']>;
  callId?: InputMaybe<Scalars['String']['input']>;
  error?: InputMaybe<Scalars['jsonb']['input']>;
  fee?: InputMaybe<Scalars['numeric']['input']>;
  hash?: InputMaybe<Scalars['bytea']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  index?: InputMaybe<Scalars['Int']['input']>;
  signature?: InputMaybe<Scalars['jsonb']['input']>;
  success?: InputMaybe<Scalars['Boolean']['input']>;
  tip?: InputMaybe<Scalars['numeric']['input']>;
  version?: InputMaybe<Scalars['Int']['input']>;
};

/** aggregate sum on columns */
export type ExtrinsicSumFields = {
  __typename?: 'ExtrinsicSumFields';
  fee?: Maybe<Scalars['numeric']['output']>;
  index?: Maybe<Scalars['Int']['output']>;
  tip?: Maybe<Scalars['numeric']['output']>;
  version?: Maybe<Scalars['Int']['output']>;
};

/** order by sum() on columns of table "extrinsic" */
export type ExtrinsicSumOrderBy = {
  fee?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  tip?: InputMaybe<OrderBy>;
  version?: InputMaybe<OrderBy>;
};

/** aggregate varPop on columns */
export type ExtrinsicVarPopFields = {
  __typename?: 'ExtrinsicVarPopFields';
  fee?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  tip?: Maybe<Scalars['Float']['output']>;
  version?: Maybe<Scalars['Float']['output']>;
};

/** order by varPop() on columns of table "extrinsic" */
export type ExtrinsicVarPopOrderBy = {
  fee?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  tip?: InputMaybe<OrderBy>;
  version?: InputMaybe<OrderBy>;
};

/** aggregate varSamp on columns */
export type ExtrinsicVarSampFields = {
  __typename?: 'ExtrinsicVarSampFields';
  fee?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  tip?: Maybe<Scalars['Float']['output']>;
  version?: Maybe<Scalars['Float']['output']>;
};

/** order by varSamp() on columns of table "extrinsic" */
export type ExtrinsicVarSampOrderBy = {
  fee?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  tip?: InputMaybe<OrderBy>;
  version?: InputMaybe<OrderBy>;
};

/** aggregate variance on columns */
export type ExtrinsicVarianceFields = {
  __typename?: 'ExtrinsicVarianceFields';
  fee?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  tip?: Maybe<Scalars['Float']['output']>;
  version?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "extrinsic" */
export type ExtrinsicVarianceOrderBy = {
  fee?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  tip?: InputMaybe<OrderBy>;
  version?: InputMaybe<OrderBy>;
};

/** columns and relationships of "identity" */
export type Identity = {
  __typename?: 'Identity';
  /** An object relationship */
  account?: Maybe<Account>;
  accountId?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  accountRemoved?: Maybe<Account>;
  accountRemovedId?: Maybe<Scalars['String']['output']>;
  /** An array relationship */
  certIssued: Array<Cert>;
  /** An aggregate relationship */
  certIssuedAggregate: CertAggregate;
  /** An array relationship */
  certReceived: Array<Cert>;
  /** An aggregate relationship */
  certReceivedAggregate: CertAggregate;
  /** An object relationship */
  createdIn?: Maybe<Event>;
  createdInId?: Maybe<Scalars['String']['output']>;
  createdOn: Scalars['Int']['output'];
  expireOn: Scalars['Int']['output'];
  id: Scalars['String']['output'];
  index: Scalars['Int']['output'];
  isMember: Scalars['Boolean']['output'];
  lastChangeOn: Scalars['Int']['output'];
  /** An array relationship */
  linkedAccount: Array<Account>;
  /** An aggregate relationship */
  linkedAccountAggregate: AccountAggregate;
  /** An array relationship */
  membershipHistory: Array<MembershipEvent>;
  /** An aggregate relationship */
  membershipHistoryAggregate: MembershipEventAggregate;
  name: Scalars['String']['output'];
  /** An array relationship */
  ownerKeyChange: Array<ChangeOwnerKey>;
  /** An aggregate relationship */
  ownerKeyChangeAggregate: ChangeOwnerKeyAggregate;
  /** An object relationship */
  smith?: Maybe<Smith>;
  status?: Maybe<IdentityStatusEnum>;
  /** "Get UD History by Identity" */
  udHistory?: Maybe<Array<UdHistory>>;
};


/** columns and relationships of "identity" */
export type IdentityCertIssuedArgs = {
  distinctOn?: InputMaybe<Array<CertSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CertOrderBy>>;
  where?: InputMaybe<CertBoolExp>;
};


/** columns and relationships of "identity" */
export type IdentityCertIssuedAggregateArgs = {
  distinctOn?: InputMaybe<Array<CertSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CertOrderBy>>;
  where?: InputMaybe<CertBoolExp>;
};


/** columns and relationships of "identity" */
export type IdentityCertReceivedArgs = {
  distinctOn?: InputMaybe<Array<CertSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CertOrderBy>>;
  where?: InputMaybe<CertBoolExp>;
};


/** columns and relationships of "identity" */
export type IdentityCertReceivedAggregateArgs = {
  distinctOn?: InputMaybe<Array<CertSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CertOrderBy>>;
  where?: InputMaybe<CertBoolExp>;
};


/** columns and relationships of "identity" */
export type IdentityLinkedAccountArgs = {
  distinctOn?: InputMaybe<Array<AccountSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<AccountOrderBy>>;
  where?: InputMaybe<AccountBoolExp>;
};


/** columns and relationships of "identity" */
export type IdentityLinkedAccountAggregateArgs = {
  distinctOn?: InputMaybe<Array<AccountSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<AccountOrderBy>>;
  where?: InputMaybe<AccountBoolExp>;
};


/** columns and relationships of "identity" */
export type IdentityMembershipHistoryArgs = {
  distinctOn?: InputMaybe<Array<MembershipEventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<MembershipEventOrderBy>>;
  where?: InputMaybe<MembershipEventBoolExp>;
};


/** columns and relationships of "identity" */
export type IdentityMembershipHistoryAggregateArgs = {
  distinctOn?: InputMaybe<Array<MembershipEventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<MembershipEventOrderBy>>;
  where?: InputMaybe<MembershipEventBoolExp>;
};


/** columns and relationships of "identity" */
export type IdentityOwnerKeyChangeArgs = {
  distinctOn?: InputMaybe<Array<ChangeOwnerKeySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ChangeOwnerKeyOrderBy>>;
  where?: InputMaybe<ChangeOwnerKeyBoolExp>;
};


/** columns and relationships of "identity" */
export type IdentityOwnerKeyChangeAggregateArgs = {
  distinctOn?: InputMaybe<Array<ChangeOwnerKeySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ChangeOwnerKeyOrderBy>>;
  where?: InputMaybe<ChangeOwnerKeyBoolExp>;
};


/** columns and relationships of "identity" */
export type IdentityUdHistoryArgs = {
  distinctOn?: InputMaybe<Array<UdHistorySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UdHistoryOrderBy>>;
  where?: InputMaybe<UdHistoryBoolExp>;
};

/** aggregated selection of "identity" */
export type IdentityAggregate = {
  __typename?: 'IdentityAggregate';
  aggregate?: Maybe<IdentityAggregateFields>;
  nodes: Array<Identity>;
};

export type IdentityAggregateBoolExp = {
  bool_and?: InputMaybe<IdentityAggregateBoolExpBool_And>;
  bool_or?: InputMaybe<IdentityAggregateBoolExpBool_Or>;
  count?: InputMaybe<IdentityAggregateBoolExpCount>;
};

/** aggregate fields of "identity" */
export type IdentityAggregateFields = {
  __typename?: 'IdentityAggregateFields';
  avg?: Maybe<IdentityAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<IdentityMaxFields>;
  min?: Maybe<IdentityMinFields>;
  stddev?: Maybe<IdentityStddevFields>;
  stddevPop?: Maybe<IdentityStddevPopFields>;
  stddevSamp?: Maybe<IdentityStddevSampFields>;
  sum?: Maybe<IdentitySumFields>;
  varPop?: Maybe<IdentityVarPopFields>;
  varSamp?: Maybe<IdentityVarSampFields>;
  variance?: Maybe<IdentityVarianceFields>;
};


/** aggregate fields of "identity" */
export type IdentityAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<IdentitySelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "identity" */
export type IdentityAggregateOrderBy = {
  avg?: InputMaybe<IdentityAvgOrderBy>;
  count?: InputMaybe<OrderBy>;
  max?: InputMaybe<IdentityMaxOrderBy>;
  min?: InputMaybe<IdentityMinOrderBy>;
  stddev?: InputMaybe<IdentityStddevOrderBy>;
  stddevPop?: InputMaybe<IdentityStddevPopOrderBy>;
  stddevSamp?: InputMaybe<IdentityStddevSampOrderBy>;
  sum?: InputMaybe<IdentitySumOrderBy>;
  varPop?: InputMaybe<IdentityVarPopOrderBy>;
  varSamp?: InputMaybe<IdentityVarSampOrderBy>;
  variance?: InputMaybe<IdentityVarianceOrderBy>;
};

/** aggregate avg on columns */
export type IdentityAvgFields = {
  __typename?: 'IdentityAvgFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
  expireOn?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  lastChangeOn?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "identity" */
export type IdentityAvgOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  lastChangeOn?: InputMaybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "identity". All fields are combined with a logical 'AND'. */
export type IdentityBoolExp = {
  _and?: InputMaybe<Array<IdentityBoolExp>>;
  _not?: InputMaybe<IdentityBoolExp>;
  _or?: InputMaybe<Array<IdentityBoolExp>>;
  account?: InputMaybe<AccountBoolExp>;
  accountId?: InputMaybe<StringComparisonExp>;
  accountRemoved?: InputMaybe<AccountBoolExp>;
  accountRemovedId?: InputMaybe<StringComparisonExp>;
  certIssued?: InputMaybe<CertBoolExp>;
  certIssuedAggregate?: InputMaybe<CertAggregateBoolExp>;
  certReceived?: InputMaybe<CertBoolExp>;
  certReceivedAggregate?: InputMaybe<CertAggregateBoolExp>;
  createdIn?: InputMaybe<EventBoolExp>;
  createdInId?: InputMaybe<StringComparisonExp>;
  createdOn?: InputMaybe<IntComparisonExp>;
  expireOn?: InputMaybe<IntComparisonExp>;
  id?: InputMaybe<StringComparisonExp>;
  index?: InputMaybe<IntComparisonExp>;
  isMember?: InputMaybe<BooleanComparisonExp>;
  lastChangeOn?: InputMaybe<IntComparisonExp>;
  linkedAccount?: InputMaybe<AccountBoolExp>;
  linkedAccountAggregate?: InputMaybe<AccountAggregateBoolExp>;
  membershipHistory?: InputMaybe<MembershipEventBoolExp>;
  membershipHistoryAggregate?: InputMaybe<MembershipEventAggregateBoolExp>;
  name?: InputMaybe<StringComparisonExp>;
  ownerKeyChange?: InputMaybe<ChangeOwnerKeyBoolExp>;
  ownerKeyChangeAggregate?: InputMaybe<ChangeOwnerKeyAggregateBoolExp>;
  smith?: InputMaybe<SmithBoolExp>;
  status?: InputMaybe<IdentityStatusEnumComparisonExp>;
  udHistory?: InputMaybe<UdHistoryBoolExp>;
};

/** aggregate max on columns */
export type IdentityMaxFields = {
  __typename?: 'IdentityMaxFields';
  accountId?: Maybe<Scalars['String']['output']>;
  accountRemovedId?: Maybe<Scalars['String']['output']>;
  createdInId?: Maybe<Scalars['String']['output']>;
  createdOn?: Maybe<Scalars['Int']['output']>;
  expireOn?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  index?: Maybe<Scalars['Int']['output']>;
  lastChangeOn?: Maybe<Scalars['Int']['output']>;
  name?: Maybe<Scalars['String']['output']>;
};

/** order by max() on columns of table "identity" */
export type IdentityMaxOrderBy = {
  accountId?: InputMaybe<OrderBy>;
  accountRemovedId?: InputMaybe<OrderBy>;
  createdInId?: InputMaybe<OrderBy>;
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  lastChangeOn?: InputMaybe<OrderBy>;
  name?: InputMaybe<OrderBy>;
};

/** aggregate min on columns */
export type IdentityMinFields = {
  __typename?: 'IdentityMinFields';
  accountId?: Maybe<Scalars['String']['output']>;
  accountRemovedId?: Maybe<Scalars['String']['output']>;
  createdInId?: Maybe<Scalars['String']['output']>;
  createdOn?: Maybe<Scalars['Int']['output']>;
  expireOn?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  index?: Maybe<Scalars['Int']['output']>;
  lastChangeOn?: Maybe<Scalars['Int']['output']>;
  name?: Maybe<Scalars['String']['output']>;
};

/** order by min() on columns of table "identity" */
export type IdentityMinOrderBy = {
  accountId?: InputMaybe<OrderBy>;
  accountRemovedId?: InputMaybe<OrderBy>;
  createdInId?: InputMaybe<OrderBy>;
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  lastChangeOn?: InputMaybe<OrderBy>;
  name?: InputMaybe<OrderBy>;
};

/** Ordering options when selecting data from "identity". */
export type IdentityOrderBy = {
  account?: InputMaybe<AccountOrderBy>;
  accountId?: InputMaybe<OrderBy>;
  accountRemoved?: InputMaybe<AccountOrderBy>;
  accountRemovedId?: InputMaybe<OrderBy>;
  certIssuedAggregate?: InputMaybe<CertAggregateOrderBy>;
  certReceivedAggregate?: InputMaybe<CertAggregateOrderBy>;
  createdIn?: InputMaybe<EventOrderBy>;
  createdInId?: InputMaybe<OrderBy>;
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  isMember?: InputMaybe<OrderBy>;
  lastChangeOn?: InputMaybe<OrderBy>;
  linkedAccountAggregate?: InputMaybe<AccountAggregateOrderBy>;
  membershipHistoryAggregate?: InputMaybe<MembershipEventAggregateOrderBy>;
  name?: InputMaybe<OrderBy>;
  ownerKeyChangeAggregate?: InputMaybe<ChangeOwnerKeyAggregateOrderBy>;
  smith?: InputMaybe<SmithOrderBy>;
  status?: InputMaybe<OrderBy>;
  udHistoryAggregate?: InputMaybe<UdHistoryAggregateOrderBy>;
};

/** select columns of table "identity" */
export enum IdentitySelectColumn {
  /** column name */
  AccountId = 'accountId',
  /** column name */
  AccountRemovedId = 'accountRemovedId',
  /** column name */
  CreatedInId = 'createdInId',
  /** column name */
  CreatedOn = 'createdOn',
  /** column name */
  ExpireOn = 'expireOn',
  /** column name */
  Id = 'id',
  /** column name */
  Index = 'index',
  /** column name */
  IsMember = 'isMember',
  /** column name */
  LastChangeOn = 'lastChangeOn',
  /** column name */
  Name = 'name',
  /** column name */
  Status = 'status'
}

/** select "identityAggregateBoolExpBool_andArgumentsColumns" columns of table "identity" */
export enum IdentitySelectColumnIdentityAggregateBoolExpBool_AndArgumentsColumns {
  /** column name */
  IsMember = 'isMember'
}

/** select "identityAggregateBoolExpBool_orArgumentsColumns" columns of table "identity" */
export enum IdentitySelectColumnIdentityAggregateBoolExpBool_OrArgumentsColumns {
  /** column name */
  IsMember = 'isMember'
}

export enum IdentityStatusEnum {
  Member = 'MEMBER',
  Notmember = 'NOTMEMBER',
  Removed = 'REMOVED',
  Revoked = 'REVOKED',
  Unconfirmed = 'UNCONFIRMED',
  Unvalidated = 'UNVALIDATED'
}

/** Boolean expression to compare columns of type "IdentityStatusEnum". All fields are combined with logical 'AND'. */
export type IdentityStatusEnumComparisonExp = {
  _eq?: InputMaybe<IdentityStatusEnum>;
  _in?: InputMaybe<Array<IdentityStatusEnum>>;
  _isNull?: InputMaybe<Scalars['Boolean']['input']>;
  _neq?: InputMaybe<IdentityStatusEnum>;
  _nin?: InputMaybe<Array<IdentityStatusEnum>>;
};

/** aggregate stddev on columns */
export type IdentityStddevFields = {
  __typename?: 'IdentityStddevFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
  expireOn?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  lastChangeOn?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "identity" */
export type IdentityStddevOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  lastChangeOn?: InputMaybe<OrderBy>;
};

/** aggregate stddevPop on columns */
export type IdentityStddevPopFields = {
  __typename?: 'IdentityStddevPopFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
  expireOn?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  lastChangeOn?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevPop() on columns of table "identity" */
export type IdentityStddevPopOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  lastChangeOn?: InputMaybe<OrderBy>;
};

/** aggregate stddevSamp on columns */
export type IdentityStddevSampFields = {
  __typename?: 'IdentityStddevSampFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
  expireOn?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  lastChangeOn?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevSamp() on columns of table "identity" */
export type IdentityStddevSampOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  lastChangeOn?: InputMaybe<OrderBy>;
};

/** Streaming cursor of the table "identity" */
export type IdentityStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: IdentityStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type IdentityStreamCursorValueInput = {
  accountId?: InputMaybe<Scalars['String']['input']>;
  accountRemovedId?: InputMaybe<Scalars['String']['input']>;
  createdInId?: InputMaybe<Scalars['String']['input']>;
  createdOn?: InputMaybe<Scalars['Int']['input']>;
  expireOn?: InputMaybe<Scalars['Int']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  index?: InputMaybe<Scalars['Int']['input']>;
  isMember?: InputMaybe<Scalars['Boolean']['input']>;
  lastChangeOn?: InputMaybe<Scalars['Int']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  status?: InputMaybe<IdentityStatusEnum>;
};

/** aggregate sum on columns */
export type IdentitySumFields = {
  __typename?: 'IdentitySumFields';
  createdOn?: Maybe<Scalars['Int']['output']>;
  expireOn?: Maybe<Scalars['Int']['output']>;
  index?: Maybe<Scalars['Int']['output']>;
  lastChangeOn?: Maybe<Scalars['Int']['output']>;
};

/** order by sum() on columns of table "identity" */
export type IdentitySumOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  lastChangeOn?: InputMaybe<OrderBy>;
};

/** aggregate varPop on columns */
export type IdentityVarPopFields = {
  __typename?: 'IdentityVarPopFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
  expireOn?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  lastChangeOn?: Maybe<Scalars['Float']['output']>;
};

/** order by varPop() on columns of table "identity" */
export type IdentityVarPopOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  lastChangeOn?: InputMaybe<OrderBy>;
};

/** aggregate varSamp on columns */
export type IdentityVarSampFields = {
  __typename?: 'IdentityVarSampFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
  expireOn?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  lastChangeOn?: Maybe<Scalars['Float']['output']>;
};

/** order by varSamp() on columns of table "identity" */
export type IdentityVarSampOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  lastChangeOn?: InputMaybe<OrderBy>;
};

/** aggregate variance on columns */
export type IdentityVarianceFields = {
  __typename?: 'IdentityVarianceFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
  expireOn?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  lastChangeOn?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "identity" */
export type IdentityVarianceOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  expireOn?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  lastChangeOn?: InputMaybe<OrderBy>;
};

/** Boolean expression to compare columns of type "Int". All fields are combined with logical 'AND'. */
export type IntArrayComparisonExp = {
  /** is the array contained in the given array value */
  _containedIn?: InputMaybe<Array<Scalars['Int']['input']>>;
  /** does the array contain the given value */
  _contains?: InputMaybe<Array<Scalars['Int']['input']>>;
  _eq?: InputMaybe<Array<Scalars['Int']['input']>>;
  _gt?: InputMaybe<Array<Scalars['Int']['input']>>;
  _gte?: InputMaybe<Array<Scalars['Int']['input']>>;
  _in?: InputMaybe<Array<Array<Scalars['Int']['input']>>>;
  _isNull?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Array<Scalars['Int']['input']>>;
  _lte?: InputMaybe<Array<Scalars['Int']['input']>>;
  _neq?: InputMaybe<Array<Scalars['Int']['input']>>;
  _nin?: InputMaybe<Array<Array<Scalars['Int']['input']>>>;
};

/** Boolean expression to compare columns of type "Int". All fields are combined with logical 'AND'. */
export type IntComparisonExp = {
  _eq?: InputMaybe<Scalars['Int']['input']>;
  _gt?: InputMaybe<Scalars['Int']['input']>;
  _gte?: InputMaybe<Scalars['Int']['input']>;
  _in?: InputMaybe<Array<Scalars['Int']['input']>>;
  _isNull?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['Int']['input']>;
  _lte?: InputMaybe<Scalars['Int']['input']>;
  _neq?: InputMaybe<Scalars['Int']['input']>;
  _nin?: InputMaybe<Array<Scalars['Int']['input']>>;
};

export enum ItemTypeEnum {
  Calls = 'CALLS',
  Events = 'EVENTS',
  Extrinsics = 'EXTRINSICS'
}

/** Boolean expression to compare columns of type "ItemTypeEnum". All fields are combined with logical 'AND'. */
export type ItemTypeEnumComparisonExp = {
  _eq?: InputMaybe<ItemTypeEnum>;
  _in?: InputMaybe<Array<ItemTypeEnum>>;
  _isNull?: InputMaybe<Scalars['Boolean']['input']>;
  _neq?: InputMaybe<ItemTypeEnum>;
  _nin?: InputMaybe<Array<ItemTypeEnum>>;
};

/** columns and relationships of "items_counter" */
export type ItemsCounter = {
  __typename?: 'ItemsCounter';
  id: Scalars['String']['output'];
  level?: Maybe<CounterLevelEnum>;
  total: Scalars['Int']['output'];
  type?: Maybe<ItemTypeEnum>;
};

/** aggregated selection of "items_counter" */
export type ItemsCounterAggregate = {
  __typename?: 'ItemsCounterAggregate';
  aggregate?: Maybe<ItemsCounterAggregateFields>;
  nodes: Array<ItemsCounter>;
};

/** aggregate fields of "items_counter" */
export type ItemsCounterAggregateFields = {
  __typename?: 'ItemsCounterAggregateFields';
  avg?: Maybe<ItemsCounterAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<ItemsCounterMaxFields>;
  min?: Maybe<ItemsCounterMinFields>;
  stddev?: Maybe<ItemsCounterStddevFields>;
  stddevPop?: Maybe<ItemsCounterStddevPopFields>;
  stddevSamp?: Maybe<ItemsCounterStddevSampFields>;
  sum?: Maybe<ItemsCounterSumFields>;
  varPop?: Maybe<ItemsCounterVarPopFields>;
  varSamp?: Maybe<ItemsCounterVarSampFields>;
  variance?: Maybe<ItemsCounterVarianceFields>;
};


/** aggregate fields of "items_counter" */
export type ItemsCounterAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<ItemsCounterSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** aggregate avg on columns */
export type ItemsCounterAvgFields = {
  __typename?: 'ItemsCounterAvgFields';
  total?: Maybe<Scalars['Float']['output']>;
};

/** Boolean expression to filter rows from the table "items_counter". All fields are combined with a logical 'AND'. */
export type ItemsCounterBoolExp = {
  _and?: InputMaybe<Array<ItemsCounterBoolExp>>;
  _not?: InputMaybe<ItemsCounterBoolExp>;
  _or?: InputMaybe<Array<ItemsCounterBoolExp>>;
  id?: InputMaybe<StringComparisonExp>;
  level?: InputMaybe<CounterLevelEnumComparisonExp>;
  total?: InputMaybe<IntComparisonExp>;
  type?: InputMaybe<ItemTypeEnumComparisonExp>;
};

/** aggregate max on columns */
export type ItemsCounterMaxFields = {
  __typename?: 'ItemsCounterMaxFields';
  id?: Maybe<Scalars['String']['output']>;
  total?: Maybe<Scalars['Int']['output']>;
};

/** aggregate min on columns */
export type ItemsCounterMinFields = {
  __typename?: 'ItemsCounterMinFields';
  id?: Maybe<Scalars['String']['output']>;
  total?: Maybe<Scalars['Int']['output']>;
};

/** Ordering options when selecting data from "items_counter". */
export type ItemsCounterOrderBy = {
  id?: InputMaybe<OrderBy>;
  level?: InputMaybe<OrderBy>;
  total?: InputMaybe<OrderBy>;
  type?: InputMaybe<OrderBy>;
};

/** select columns of table "items_counter" */
export enum ItemsCounterSelectColumn {
  /** column name */
  Id = 'id',
  /** column name */
  Level = 'level',
  /** column name */
  Total = 'total',
  /** column name */
  Type = 'type'
}

/** aggregate stddev on columns */
export type ItemsCounterStddevFields = {
  __typename?: 'ItemsCounterStddevFields';
  total?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddevPop on columns */
export type ItemsCounterStddevPopFields = {
  __typename?: 'ItemsCounterStddevPopFields';
  total?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddevSamp on columns */
export type ItemsCounterStddevSampFields = {
  __typename?: 'ItemsCounterStddevSampFields';
  total?: Maybe<Scalars['Float']['output']>;
};

/** Streaming cursor of the table "items_counter" */
export type ItemsCounterStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: ItemsCounterStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type ItemsCounterStreamCursorValueInput = {
  id?: InputMaybe<Scalars['String']['input']>;
  level?: InputMaybe<CounterLevelEnum>;
  total?: InputMaybe<Scalars['Int']['input']>;
  type?: InputMaybe<ItemTypeEnum>;
};

/** aggregate sum on columns */
export type ItemsCounterSumFields = {
  __typename?: 'ItemsCounterSumFields';
  total?: Maybe<Scalars['Int']['output']>;
};

/** aggregate varPop on columns */
export type ItemsCounterVarPopFields = {
  __typename?: 'ItemsCounterVarPopFields';
  total?: Maybe<Scalars['Float']['output']>;
};

/** aggregate varSamp on columns */
export type ItemsCounterVarSampFields = {
  __typename?: 'ItemsCounterVarSampFields';
  total?: Maybe<Scalars['Float']['output']>;
};

/** aggregate variance on columns */
export type ItemsCounterVarianceFields = {
  __typename?: 'ItemsCounterVarianceFields';
  total?: Maybe<Scalars['Float']['output']>;
};

export type JsonbCastExp = {
  String?: InputMaybe<StringComparisonExp>;
};

/** Boolean expression to compare columns of type "jsonb". All fields are combined with logical 'AND'. */
export type JsonbComparisonExp = {
  _cast?: InputMaybe<JsonbCastExp>;
  /** is the column contained in the given json value */
  _containedIn?: InputMaybe<Scalars['jsonb']['input']>;
  /** does the column contain the given json value at the top level */
  _contains?: InputMaybe<Scalars['jsonb']['input']>;
  _eq?: InputMaybe<Scalars['jsonb']['input']>;
  _gt?: InputMaybe<Scalars['jsonb']['input']>;
  _gte?: InputMaybe<Scalars['jsonb']['input']>;
  /** does the string exist as a top-level key in the column */
  _hasKey?: InputMaybe<Scalars['String']['input']>;
  /** do all of these strings exist as top-level keys in the column */
  _hasKeysAll?: InputMaybe<Array<Scalars['String']['input']>>;
  /** do any of these strings exist as top-level keys in the column */
  _hasKeysAny?: InputMaybe<Array<Scalars['String']['input']>>;
  _in?: InputMaybe<Array<Scalars['jsonb']['input']>>;
  _isNull?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['jsonb']['input']>;
  _lte?: InputMaybe<Scalars['jsonb']['input']>;
  _neq?: InputMaybe<Scalars['jsonb']['input']>;
  _nin?: InputMaybe<Array<Scalars['jsonb']['input']>>;
};

/** columns and relationships of "membership_event" */
export type MembershipEvent = {
  __typename?: 'MembershipEvent';
  blockNumber: Scalars['Int']['output'];
  /** An object relationship */
  event?: Maybe<Event>;
  eventId?: Maybe<Scalars['String']['output']>;
  eventType?: Maybe<EventTypeEnum>;
  id: Scalars['String']['output'];
  /** An object relationship */
  identity?: Maybe<Identity>;
  identityId?: Maybe<Scalars['String']['output']>;
};

/** aggregated selection of "membership_event" */
export type MembershipEventAggregate = {
  __typename?: 'MembershipEventAggregate';
  aggregate?: Maybe<MembershipEventAggregateFields>;
  nodes: Array<MembershipEvent>;
};

export type MembershipEventAggregateBoolExp = {
  count?: InputMaybe<MembershipEventAggregateBoolExpCount>;
};

/** aggregate fields of "membership_event" */
export type MembershipEventAggregateFields = {
  __typename?: 'MembershipEventAggregateFields';
  avg?: Maybe<MembershipEventAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<MembershipEventMaxFields>;
  min?: Maybe<MembershipEventMinFields>;
  stddev?: Maybe<MembershipEventStddevFields>;
  stddevPop?: Maybe<MembershipEventStddevPopFields>;
  stddevSamp?: Maybe<MembershipEventStddevSampFields>;
  sum?: Maybe<MembershipEventSumFields>;
  varPop?: Maybe<MembershipEventVarPopFields>;
  varSamp?: Maybe<MembershipEventVarSampFields>;
  variance?: Maybe<MembershipEventVarianceFields>;
};


/** aggregate fields of "membership_event" */
export type MembershipEventAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<MembershipEventSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "membership_event" */
export type MembershipEventAggregateOrderBy = {
  avg?: InputMaybe<MembershipEventAvgOrderBy>;
  count?: InputMaybe<OrderBy>;
  max?: InputMaybe<MembershipEventMaxOrderBy>;
  min?: InputMaybe<MembershipEventMinOrderBy>;
  stddev?: InputMaybe<MembershipEventStddevOrderBy>;
  stddevPop?: InputMaybe<MembershipEventStddevPopOrderBy>;
  stddevSamp?: InputMaybe<MembershipEventStddevSampOrderBy>;
  sum?: InputMaybe<MembershipEventSumOrderBy>;
  varPop?: InputMaybe<MembershipEventVarPopOrderBy>;
  varSamp?: InputMaybe<MembershipEventVarSampOrderBy>;
  variance?: InputMaybe<MembershipEventVarianceOrderBy>;
};

/** aggregate avg on columns */
export type MembershipEventAvgFields = {
  __typename?: 'MembershipEventAvgFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "membership_event" */
export type MembershipEventAvgOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "membership_event". All fields are combined with a logical 'AND'. */
export type MembershipEventBoolExp = {
  _and?: InputMaybe<Array<MembershipEventBoolExp>>;
  _not?: InputMaybe<MembershipEventBoolExp>;
  _or?: InputMaybe<Array<MembershipEventBoolExp>>;
  blockNumber?: InputMaybe<IntComparisonExp>;
  event?: InputMaybe<EventBoolExp>;
  eventId?: InputMaybe<StringComparisonExp>;
  eventType?: InputMaybe<EventTypeEnumComparisonExp>;
  id?: InputMaybe<StringComparisonExp>;
  identity?: InputMaybe<IdentityBoolExp>;
  identityId?: InputMaybe<StringComparisonExp>;
};

/** aggregate max on columns */
export type MembershipEventMaxFields = {
  __typename?: 'MembershipEventMaxFields';
  blockNumber?: Maybe<Scalars['Int']['output']>;
  eventId?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  identityId?: Maybe<Scalars['String']['output']>;
};

/** order by max() on columns of table "membership_event" */
export type MembershipEventMaxOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
  eventId?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  identityId?: InputMaybe<OrderBy>;
};

/** aggregate min on columns */
export type MembershipEventMinFields = {
  __typename?: 'MembershipEventMinFields';
  blockNumber?: Maybe<Scalars['Int']['output']>;
  eventId?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  identityId?: Maybe<Scalars['String']['output']>;
};

/** order by min() on columns of table "membership_event" */
export type MembershipEventMinOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
  eventId?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  identityId?: InputMaybe<OrderBy>;
};

/** Ordering options when selecting data from "membership_event". */
export type MembershipEventOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
  event?: InputMaybe<EventOrderBy>;
  eventId?: InputMaybe<OrderBy>;
  eventType?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  identity?: InputMaybe<IdentityOrderBy>;
  identityId?: InputMaybe<OrderBy>;
};

/** select columns of table "membership_event" */
export enum MembershipEventSelectColumn {
  /** column name */
  BlockNumber = 'blockNumber',
  /** column name */
  EventId = 'eventId',
  /** column name */
  EventType = 'eventType',
  /** column name */
  Id = 'id',
  /** column name */
  IdentityId = 'identityId'
}

/** aggregate stddev on columns */
export type MembershipEventStddevFields = {
  __typename?: 'MembershipEventStddevFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "membership_event" */
export type MembershipEventStddevOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate stddevPop on columns */
export type MembershipEventStddevPopFields = {
  __typename?: 'MembershipEventStddevPopFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevPop() on columns of table "membership_event" */
export type MembershipEventStddevPopOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate stddevSamp on columns */
export type MembershipEventStddevSampFields = {
  __typename?: 'MembershipEventStddevSampFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevSamp() on columns of table "membership_event" */
export type MembershipEventStddevSampOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** Streaming cursor of the table "membership_event" */
export type MembershipEventStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: MembershipEventStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type MembershipEventStreamCursorValueInput = {
  blockNumber?: InputMaybe<Scalars['Int']['input']>;
  eventId?: InputMaybe<Scalars['String']['input']>;
  eventType?: InputMaybe<EventTypeEnum>;
  id?: InputMaybe<Scalars['String']['input']>;
  identityId?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate sum on columns */
export type MembershipEventSumFields = {
  __typename?: 'MembershipEventSumFields';
  blockNumber?: Maybe<Scalars['Int']['output']>;
};

/** order by sum() on columns of table "membership_event" */
export type MembershipEventSumOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate varPop on columns */
export type MembershipEventVarPopFields = {
  __typename?: 'MembershipEventVarPopFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by varPop() on columns of table "membership_event" */
export type MembershipEventVarPopOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate varSamp on columns */
export type MembershipEventVarSampFields = {
  __typename?: 'MembershipEventVarSampFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by varSamp() on columns of table "membership_event" */
export type MembershipEventVarSampOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate variance on columns */
export type MembershipEventVarianceFields = {
  __typename?: 'MembershipEventVarianceFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "membership_event" */
export type MembershipEventVarianceOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** Boolean expression to compare columns of type "numeric". All fields are combined with logical 'AND'. */
export type NumericComparisonExp = {
  _eq?: InputMaybe<Scalars['numeric']['input']>;
  _gt?: InputMaybe<Scalars['numeric']['input']>;
  _gte?: InputMaybe<Scalars['numeric']['input']>;
  _in?: InputMaybe<Array<Scalars['numeric']['input']>>;
  _isNull?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['numeric']['input']>;
  _lte?: InputMaybe<Scalars['numeric']['input']>;
  _neq?: InputMaybe<Scalars['numeric']['input']>;
  _nin?: InputMaybe<Array<Scalars['numeric']['input']>>;
};

/** column ordering options */
export enum OrderBy {
  /** in ascending order, nulls last */
  Asc = 'ASC',
  /** in ascending order, nulls first */
  AscNullsFirst = 'ASC_NULLS_FIRST',
  /** in ascending order, nulls last */
  AscNullsLast = 'ASC_NULLS_LAST',
  /** in descending order, nulls first */
  Desc = 'DESC',
  /** in descending order, nulls first */
  DescNullsFirst = 'DESC_NULLS_FIRST',
  /** in descending order, nulls last */
  DescNullsLast = 'DESC_NULLS_LAST'
}

/** columns and relationships of "population_history" */
export type PopulationHistory = {
  __typename?: 'PopulationHistory';
  activeAccountCount: Scalars['Int']['output'];
  blockNumber: Scalars['Int']['output'];
  id: Scalars['String']['output'];
  memberCount: Scalars['Int']['output'];
  smithCount: Scalars['Int']['output'];
};

/** aggregated selection of "population_history" */
export type PopulationHistoryAggregate = {
  __typename?: 'PopulationHistoryAggregate';
  aggregate?: Maybe<PopulationHistoryAggregateFields>;
  nodes: Array<PopulationHistory>;
};

/** aggregate fields of "population_history" */
export type PopulationHistoryAggregateFields = {
  __typename?: 'PopulationHistoryAggregateFields';
  avg?: Maybe<PopulationHistoryAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<PopulationHistoryMaxFields>;
  min?: Maybe<PopulationHistoryMinFields>;
  stddev?: Maybe<PopulationHistoryStddevFields>;
  stddevPop?: Maybe<PopulationHistoryStddevPopFields>;
  stddevSamp?: Maybe<PopulationHistoryStddevSampFields>;
  sum?: Maybe<PopulationHistorySumFields>;
  varPop?: Maybe<PopulationHistoryVarPopFields>;
  varSamp?: Maybe<PopulationHistoryVarSampFields>;
  variance?: Maybe<PopulationHistoryVarianceFields>;
};


/** aggregate fields of "population_history" */
export type PopulationHistoryAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<PopulationHistorySelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** aggregate avg on columns */
export type PopulationHistoryAvgFields = {
  __typename?: 'PopulationHistoryAvgFields';
  activeAccountCount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
  memberCount?: Maybe<Scalars['Float']['output']>;
  smithCount?: Maybe<Scalars['Float']['output']>;
};

/** Boolean expression to filter rows from the table "population_history". All fields are combined with a logical 'AND'. */
export type PopulationHistoryBoolExp = {
  _and?: InputMaybe<Array<PopulationHistoryBoolExp>>;
  _not?: InputMaybe<PopulationHistoryBoolExp>;
  _or?: InputMaybe<Array<PopulationHistoryBoolExp>>;
  activeAccountCount?: InputMaybe<IntComparisonExp>;
  blockNumber?: InputMaybe<IntComparisonExp>;
  id?: InputMaybe<StringComparisonExp>;
  memberCount?: InputMaybe<IntComparisonExp>;
  smithCount?: InputMaybe<IntComparisonExp>;
};

/** aggregate max on columns */
export type PopulationHistoryMaxFields = {
  __typename?: 'PopulationHistoryMaxFields';
  activeAccountCount?: Maybe<Scalars['Int']['output']>;
  blockNumber?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  memberCount?: Maybe<Scalars['Int']['output']>;
  smithCount?: Maybe<Scalars['Int']['output']>;
};

/** aggregate min on columns */
export type PopulationHistoryMinFields = {
  __typename?: 'PopulationHistoryMinFields';
  activeAccountCount?: Maybe<Scalars['Int']['output']>;
  blockNumber?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  memberCount?: Maybe<Scalars['Int']['output']>;
  smithCount?: Maybe<Scalars['Int']['output']>;
};

/** Ordering options when selecting data from "population_history". */
export type PopulationHistoryOrderBy = {
  activeAccountCount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  memberCount?: InputMaybe<OrderBy>;
  smithCount?: InputMaybe<OrderBy>;
};

/** select columns of table "population_history" */
export enum PopulationHistorySelectColumn {
  /** column name */
  ActiveAccountCount = 'activeAccountCount',
  /** column name */
  BlockNumber = 'blockNumber',
  /** column name */
  Id = 'id',
  /** column name */
  MemberCount = 'memberCount',
  /** column name */
  SmithCount = 'smithCount'
}

/** aggregate stddev on columns */
export type PopulationHistoryStddevFields = {
  __typename?: 'PopulationHistoryStddevFields';
  activeAccountCount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
  memberCount?: Maybe<Scalars['Float']['output']>;
  smithCount?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddevPop on columns */
export type PopulationHistoryStddevPopFields = {
  __typename?: 'PopulationHistoryStddevPopFields';
  activeAccountCount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
  memberCount?: Maybe<Scalars['Float']['output']>;
  smithCount?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddevSamp on columns */
export type PopulationHistoryStddevSampFields = {
  __typename?: 'PopulationHistoryStddevSampFields';
  activeAccountCount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
  memberCount?: Maybe<Scalars['Float']['output']>;
  smithCount?: Maybe<Scalars['Float']['output']>;
};

/** Streaming cursor of the table "population_history" */
export type PopulationHistoryStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: PopulationHistoryStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type PopulationHistoryStreamCursorValueInput = {
  activeAccountCount?: InputMaybe<Scalars['Int']['input']>;
  blockNumber?: InputMaybe<Scalars['Int']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  memberCount?: InputMaybe<Scalars['Int']['input']>;
  smithCount?: InputMaybe<Scalars['Int']['input']>;
};

/** aggregate sum on columns */
export type PopulationHistorySumFields = {
  __typename?: 'PopulationHistorySumFields';
  activeAccountCount?: Maybe<Scalars['Int']['output']>;
  blockNumber?: Maybe<Scalars['Int']['output']>;
  memberCount?: Maybe<Scalars['Int']['output']>;
  smithCount?: Maybe<Scalars['Int']['output']>;
};

/** aggregate varPop on columns */
export type PopulationHistoryVarPopFields = {
  __typename?: 'PopulationHistoryVarPopFields';
  activeAccountCount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
  memberCount?: Maybe<Scalars['Float']['output']>;
  smithCount?: Maybe<Scalars['Float']['output']>;
};

/** aggregate varSamp on columns */
export type PopulationHistoryVarSampFields = {
  __typename?: 'PopulationHistoryVarSampFields';
  activeAccountCount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
  memberCount?: Maybe<Scalars['Float']['output']>;
  smithCount?: Maybe<Scalars['Float']['output']>;
};

/** aggregate variance on columns */
export type PopulationHistoryVarianceFields = {
  __typename?: 'PopulationHistoryVarianceFields';
  activeAccountCount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
  memberCount?: Maybe<Scalars['Float']['output']>;
  smithCount?: Maybe<Scalars['Float']['output']>;
};

/** columns and relationships of "smith" */
export type Smith = {
  __typename?: 'Smith';
  forged: Scalars['Int']['output'];
  id: Scalars['String']['output'];
  /** An object relationship */
  identity?: Maybe<Identity>;
  identityId?: Maybe<Scalars['String']['output']>;
  index: Scalars['Int']['output'];
  lastChanged?: Maybe<Scalars['Int']['output']>;
  lastForged?: Maybe<Scalars['Int']['output']>;
  /** An array relationship */
  smithCertIssued: Array<SmithCert>;
  /** An aggregate relationship */
  smithCertIssuedAggregate: SmithCertAggregate;
  /** An array relationship */
  smithCertReceived: Array<SmithCert>;
  /** An aggregate relationship */
  smithCertReceivedAggregate: SmithCertAggregate;
  /** An array relationship */
  smithHistory: Array<SmithEvent>;
  /** An aggregate relationship */
  smithHistoryAggregate: SmithEventAggregate;
  smithStatus?: Maybe<SmithStatusEnum>;
};


/** columns and relationships of "smith" */
export type SmithSmithCertIssuedArgs = {
  distinctOn?: InputMaybe<Array<SmithCertSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<SmithCertOrderBy>>;
  where?: InputMaybe<SmithCertBoolExp>;
};


/** columns and relationships of "smith" */
export type SmithSmithCertIssuedAggregateArgs = {
  distinctOn?: InputMaybe<Array<SmithCertSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<SmithCertOrderBy>>;
  where?: InputMaybe<SmithCertBoolExp>;
};


/** columns and relationships of "smith" */
export type SmithSmithCertReceivedArgs = {
  distinctOn?: InputMaybe<Array<SmithCertSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<SmithCertOrderBy>>;
  where?: InputMaybe<SmithCertBoolExp>;
};


/** columns and relationships of "smith" */
export type SmithSmithCertReceivedAggregateArgs = {
  distinctOn?: InputMaybe<Array<SmithCertSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<SmithCertOrderBy>>;
  where?: InputMaybe<SmithCertBoolExp>;
};


/** columns and relationships of "smith" */
export type SmithSmithHistoryArgs = {
  distinctOn?: InputMaybe<Array<SmithEventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<SmithEventOrderBy>>;
  where?: InputMaybe<SmithEventBoolExp>;
};


/** columns and relationships of "smith" */
export type SmithSmithHistoryAggregateArgs = {
  distinctOn?: InputMaybe<Array<SmithEventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<SmithEventOrderBy>>;
  where?: InputMaybe<SmithEventBoolExp>;
};

/** aggregated selection of "smith" */
export type SmithAggregate = {
  __typename?: 'SmithAggregate';
  aggregate?: Maybe<SmithAggregateFields>;
  nodes: Array<Smith>;
};

/** aggregate fields of "smith" */
export type SmithAggregateFields = {
  __typename?: 'SmithAggregateFields';
  avg?: Maybe<SmithAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<SmithMaxFields>;
  min?: Maybe<SmithMinFields>;
  stddev?: Maybe<SmithStddevFields>;
  stddevPop?: Maybe<SmithStddevPopFields>;
  stddevSamp?: Maybe<SmithStddevSampFields>;
  sum?: Maybe<SmithSumFields>;
  varPop?: Maybe<SmithVarPopFields>;
  varSamp?: Maybe<SmithVarSampFields>;
  variance?: Maybe<SmithVarianceFields>;
};


/** aggregate fields of "smith" */
export type SmithAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<SmithSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** aggregate avg on columns */
export type SmithAvgFields = {
  __typename?: 'SmithAvgFields';
  forged?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  lastChanged?: Maybe<Scalars['Float']['output']>;
  lastForged?: Maybe<Scalars['Float']['output']>;
};

/** Boolean expression to filter rows from the table "smith". All fields are combined with a logical 'AND'. */
export type SmithBoolExp = {
  _and?: InputMaybe<Array<SmithBoolExp>>;
  _not?: InputMaybe<SmithBoolExp>;
  _or?: InputMaybe<Array<SmithBoolExp>>;
  forged?: InputMaybe<IntComparisonExp>;
  id?: InputMaybe<StringComparisonExp>;
  identity?: InputMaybe<IdentityBoolExp>;
  identityId?: InputMaybe<StringComparisonExp>;
  index?: InputMaybe<IntComparisonExp>;
  lastChanged?: InputMaybe<IntComparisonExp>;
  lastForged?: InputMaybe<IntComparisonExp>;
  smithCertIssued?: InputMaybe<SmithCertBoolExp>;
  smithCertIssuedAggregate?: InputMaybe<SmithCertAggregateBoolExp>;
  smithCertReceived?: InputMaybe<SmithCertBoolExp>;
  smithCertReceivedAggregate?: InputMaybe<SmithCertAggregateBoolExp>;
  smithHistory?: InputMaybe<SmithEventBoolExp>;
  smithHistoryAggregate?: InputMaybe<SmithEventAggregateBoolExp>;
  smithStatus?: InputMaybe<SmithStatusEnumComparisonExp>;
};

/** columns and relationships of "smith_cert" */
export type SmithCert = {
  __typename?: 'SmithCert';
  createdOn: Scalars['Int']['output'];
  id: Scalars['String']['output'];
  /** An object relationship */
  issuer?: Maybe<Smith>;
  issuerId?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  receiver?: Maybe<Smith>;
  receiverId?: Maybe<Scalars['String']['output']>;
};

/** aggregated selection of "smith_cert" */
export type SmithCertAggregate = {
  __typename?: 'SmithCertAggregate';
  aggregate?: Maybe<SmithCertAggregateFields>;
  nodes: Array<SmithCert>;
};

export type SmithCertAggregateBoolExp = {
  count?: InputMaybe<SmithCertAggregateBoolExpCount>;
};

/** aggregate fields of "smith_cert" */
export type SmithCertAggregateFields = {
  __typename?: 'SmithCertAggregateFields';
  avg?: Maybe<SmithCertAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<SmithCertMaxFields>;
  min?: Maybe<SmithCertMinFields>;
  stddev?: Maybe<SmithCertStddevFields>;
  stddevPop?: Maybe<SmithCertStddevPopFields>;
  stddevSamp?: Maybe<SmithCertStddevSampFields>;
  sum?: Maybe<SmithCertSumFields>;
  varPop?: Maybe<SmithCertVarPopFields>;
  varSamp?: Maybe<SmithCertVarSampFields>;
  variance?: Maybe<SmithCertVarianceFields>;
};


/** aggregate fields of "smith_cert" */
export type SmithCertAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<SmithCertSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "smith_cert" */
export type SmithCertAggregateOrderBy = {
  avg?: InputMaybe<SmithCertAvgOrderBy>;
  count?: InputMaybe<OrderBy>;
  max?: InputMaybe<SmithCertMaxOrderBy>;
  min?: InputMaybe<SmithCertMinOrderBy>;
  stddev?: InputMaybe<SmithCertStddevOrderBy>;
  stddevPop?: InputMaybe<SmithCertStddevPopOrderBy>;
  stddevSamp?: InputMaybe<SmithCertStddevSampOrderBy>;
  sum?: InputMaybe<SmithCertSumOrderBy>;
  varPop?: InputMaybe<SmithCertVarPopOrderBy>;
  varSamp?: InputMaybe<SmithCertVarSampOrderBy>;
  variance?: InputMaybe<SmithCertVarianceOrderBy>;
};

/** aggregate avg on columns */
export type SmithCertAvgFields = {
  __typename?: 'SmithCertAvgFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "smith_cert" */
export type SmithCertAvgOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "smith_cert". All fields are combined with a logical 'AND'. */
export type SmithCertBoolExp = {
  _and?: InputMaybe<Array<SmithCertBoolExp>>;
  _not?: InputMaybe<SmithCertBoolExp>;
  _or?: InputMaybe<Array<SmithCertBoolExp>>;
  createdOn?: InputMaybe<IntComparisonExp>;
  id?: InputMaybe<StringComparisonExp>;
  issuer?: InputMaybe<SmithBoolExp>;
  issuerId?: InputMaybe<StringComparisonExp>;
  receiver?: InputMaybe<SmithBoolExp>;
  receiverId?: InputMaybe<StringComparisonExp>;
};

/** aggregate max on columns */
export type SmithCertMaxFields = {
  __typename?: 'SmithCertMaxFields';
  createdOn?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  issuerId?: Maybe<Scalars['String']['output']>;
  receiverId?: Maybe<Scalars['String']['output']>;
};

/** order by max() on columns of table "smith_cert" */
export type SmithCertMaxOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  issuerId?: InputMaybe<OrderBy>;
  receiverId?: InputMaybe<OrderBy>;
};

/** aggregate min on columns */
export type SmithCertMinFields = {
  __typename?: 'SmithCertMinFields';
  createdOn?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  issuerId?: Maybe<Scalars['String']['output']>;
  receiverId?: Maybe<Scalars['String']['output']>;
};

/** order by min() on columns of table "smith_cert" */
export type SmithCertMinOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  issuerId?: InputMaybe<OrderBy>;
  receiverId?: InputMaybe<OrderBy>;
};

/** Ordering options when selecting data from "smith_cert". */
export type SmithCertOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  issuer?: InputMaybe<SmithOrderBy>;
  issuerId?: InputMaybe<OrderBy>;
  receiver?: InputMaybe<SmithOrderBy>;
  receiverId?: InputMaybe<OrderBy>;
};

/** select columns of table "smith_cert" */
export enum SmithCertSelectColumn {
  /** column name */
  CreatedOn = 'createdOn',
  /** column name */
  Id = 'id',
  /** column name */
  IssuerId = 'issuerId',
  /** column name */
  ReceiverId = 'receiverId'
}

/** aggregate stddev on columns */
export type SmithCertStddevFields = {
  __typename?: 'SmithCertStddevFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "smith_cert" */
export type SmithCertStddevOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
};

/** aggregate stddevPop on columns */
export type SmithCertStddevPopFields = {
  __typename?: 'SmithCertStddevPopFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevPop() on columns of table "smith_cert" */
export type SmithCertStddevPopOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
};

/** aggregate stddevSamp on columns */
export type SmithCertStddevSampFields = {
  __typename?: 'SmithCertStddevSampFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevSamp() on columns of table "smith_cert" */
export type SmithCertStddevSampOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
};

/** Streaming cursor of the table "smith_cert" */
export type SmithCertStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: SmithCertStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type SmithCertStreamCursorValueInput = {
  createdOn?: InputMaybe<Scalars['Int']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  issuerId?: InputMaybe<Scalars['String']['input']>;
  receiverId?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate sum on columns */
export type SmithCertSumFields = {
  __typename?: 'SmithCertSumFields';
  createdOn?: Maybe<Scalars['Int']['output']>;
};

/** order by sum() on columns of table "smith_cert" */
export type SmithCertSumOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
};

/** aggregate varPop on columns */
export type SmithCertVarPopFields = {
  __typename?: 'SmithCertVarPopFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
};

/** order by varPop() on columns of table "smith_cert" */
export type SmithCertVarPopOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
};

/** aggregate varSamp on columns */
export type SmithCertVarSampFields = {
  __typename?: 'SmithCertVarSampFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
};

/** order by varSamp() on columns of table "smith_cert" */
export type SmithCertVarSampOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
};

/** aggregate variance on columns */
export type SmithCertVarianceFields = {
  __typename?: 'SmithCertVarianceFields';
  createdOn?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "smith_cert" */
export type SmithCertVarianceOrderBy = {
  createdOn?: InputMaybe<OrderBy>;
};

/** columns and relationships of "smith_event" */
export type SmithEvent = {
  __typename?: 'SmithEvent';
  blockNumber: Scalars['Int']['output'];
  /** An object relationship */
  event?: Maybe<Event>;
  eventId?: Maybe<Scalars['String']['output']>;
  eventType?: Maybe<SmithEventTypeEnum>;
  id: Scalars['String']['output'];
  /** An object relationship */
  smith?: Maybe<Smith>;
  smithId?: Maybe<Scalars['String']['output']>;
};

/** aggregated selection of "smith_event" */
export type SmithEventAggregate = {
  __typename?: 'SmithEventAggregate';
  aggregate?: Maybe<SmithEventAggregateFields>;
  nodes: Array<SmithEvent>;
};

export type SmithEventAggregateBoolExp = {
  count?: InputMaybe<SmithEventAggregateBoolExpCount>;
};

/** aggregate fields of "smith_event" */
export type SmithEventAggregateFields = {
  __typename?: 'SmithEventAggregateFields';
  avg?: Maybe<SmithEventAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<SmithEventMaxFields>;
  min?: Maybe<SmithEventMinFields>;
  stddev?: Maybe<SmithEventStddevFields>;
  stddevPop?: Maybe<SmithEventStddevPopFields>;
  stddevSamp?: Maybe<SmithEventStddevSampFields>;
  sum?: Maybe<SmithEventSumFields>;
  varPop?: Maybe<SmithEventVarPopFields>;
  varSamp?: Maybe<SmithEventVarSampFields>;
  variance?: Maybe<SmithEventVarianceFields>;
};


/** aggregate fields of "smith_event" */
export type SmithEventAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<SmithEventSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "smith_event" */
export type SmithEventAggregateOrderBy = {
  avg?: InputMaybe<SmithEventAvgOrderBy>;
  count?: InputMaybe<OrderBy>;
  max?: InputMaybe<SmithEventMaxOrderBy>;
  min?: InputMaybe<SmithEventMinOrderBy>;
  stddev?: InputMaybe<SmithEventStddevOrderBy>;
  stddevPop?: InputMaybe<SmithEventStddevPopOrderBy>;
  stddevSamp?: InputMaybe<SmithEventStddevSampOrderBy>;
  sum?: InputMaybe<SmithEventSumOrderBy>;
  varPop?: InputMaybe<SmithEventVarPopOrderBy>;
  varSamp?: InputMaybe<SmithEventVarSampOrderBy>;
  variance?: InputMaybe<SmithEventVarianceOrderBy>;
};

/** aggregate avg on columns */
export type SmithEventAvgFields = {
  __typename?: 'SmithEventAvgFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "smith_event" */
export type SmithEventAvgOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "smith_event". All fields are combined with a logical 'AND'. */
export type SmithEventBoolExp = {
  _and?: InputMaybe<Array<SmithEventBoolExp>>;
  _not?: InputMaybe<SmithEventBoolExp>;
  _or?: InputMaybe<Array<SmithEventBoolExp>>;
  blockNumber?: InputMaybe<IntComparisonExp>;
  event?: InputMaybe<EventBoolExp>;
  eventId?: InputMaybe<StringComparisonExp>;
  eventType?: InputMaybe<SmithEventTypeEnumComparisonExp>;
  id?: InputMaybe<StringComparisonExp>;
  smith?: InputMaybe<SmithBoolExp>;
  smithId?: InputMaybe<StringComparisonExp>;
};

/** aggregate max on columns */
export type SmithEventMaxFields = {
  __typename?: 'SmithEventMaxFields';
  blockNumber?: Maybe<Scalars['Int']['output']>;
  eventId?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  smithId?: Maybe<Scalars['String']['output']>;
};

/** order by max() on columns of table "smith_event" */
export type SmithEventMaxOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
  eventId?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  smithId?: InputMaybe<OrderBy>;
};

/** aggregate min on columns */
export type SmithEventMinFields = {
  __typename?: 'SmithEventMinFields';
  blockNumber?: Maybe<Scalars['Int']['output']>;
  eventId?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  smithId?: Maybe<Scalars['String']['output']>;
};

/** order by min() on columns of table "smith_event" */
export type SmithEventMinOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
  eventId?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  smithId?: InputMaybe<OrderBy>;
};

/** Ordering options when selecting data from "smith_event". */
export type SmithEventOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
  event?: InputMaybe<EventOrderBy>;
  eventId?: InputMaybe<OrderBy>;
  eventType?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  smith?: InputMaybe<SmithOrderBy>;
  smithId?: InputMaybe<OrderBy>;
};

/** select columns of table "smith_event" */
export enum SmithEventSelectColumn {
  /** column name */
  BlockNumber = 'blockNumber',
  /** column name */
  EventId = 'eventId',
  /** column name */
  EventType = 'eventType',
  /** column name */
  Id = 'id',
  /** column name */
  SmithId = 'smithId'
}

/** aggregate stddev on columns */
export type SmithEventStddevFields = {
  __typename?: 'SmithEventStddevFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "smith_event" */
export type SmithEventStddevOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate stddevPop on columns */
export type SmithEventStddevPopFields = {
  __typename?: 'SmithEventStddevPopFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevPop() on columns of table "smith_event" */
export type SmithEventStddevPopOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate stddevSamp on columns */
export type SmithEventStddevSampFields = {
  __typename?: 'SmithEventStddevSampFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevSamp() on columns of table "smith_event" */
export type SmithEventStddevSampOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** Streaming cursor of the table "smith_event" */
export type SmithEventStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: SmithEventStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type SmithEventStreamCursorValueInput = {
  blockNumber?: InputMaybe<Scalars['Int']['input']>;
  eventId?: InputMaybe<Scalars['String']['input']>;
  eventType?: InputMaybe<SmithEventTypeEnum>;
  id?: InputMaybe<Scalars['String']['input']>;
  smithId?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate sum on columns */
export type SmithEventSumFields = {
  __typename?: 'SmithEventSumFields';
  blockNumber?: Maybe<Scalars['Int']['output']>;
};

/** order by sum() on columns of table "smith_event" */
export type SmithEventSumOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

export enum SmithEventTypeEnum {
  Accepted = 'ACCEPTED',
  Excluded = 'EXCLUDED',
  Invited = 'INVITED',
  Promoted = 'PROMOTED'
}

/** Boolean expression to compare columns of type "SmithEventTypeEnum". All fields are combined with logical 'AND'. */
export type SmithEventTypeEnumComparisonExp = {
  _eq?: InputMaybe<SmithEventTypeEnum>;
  _in?: InputMaybe<Array<SmithEventTypeEnum>>;
  _isNull?: InputMaybe<Scalars['Boolean']['input']>;
  _neq?: InputMaybe<SmithEventTypeEnum>;
  _nin?: InputMaybe<Array<SmithEventTypeEnum>>;
};

/** aggregate varPop on columns */
export type SmithEventVarPopFields = {
  __typename?: 'SmithEventVarPopFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by varPop() on columns of table "smith_event" */
export type SmithEventVarPopOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate varSamp on columns */
export type SmithEventVarSampFields = {
  __typename?: 'SmithEventVarSampFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by varSamp() on columns of table "smith_event" */
export type SmithEventVarSampOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate variance on columns */
export type SmithEventVarianceFields = {
  __typename?: 'SmithEventVarianceFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "smith_event" */
export type SmithEventVarianceOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate max on columns */
export type SmithMaxFields = {
  __typename?: 'SmithMaxFields';
  forged?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  identityId?: Maybe<Scalars['String']['output']>;
  index?: Maybe<Scalars['Int']['output']>;
  lastChanged?: Maybe<Scalars['Int']['output']>;
  lastForged?: Maybe<Scalars['Int']['output']>;
};

/** aggregate min on columns */
export type SmithMinFields = {
  __typename?: 'SmithMinFields';
  forged?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  identityId?: Maybe<Scalars['String']['output']>;
  index?: Maybe<Scalars['Int']['output']>;
  lastChanged?: Maybe<Scalars['Int']['output']>;
  lastForged?: Maybe<Scalars['Int']['output']>;
};

/** Ordering options when selecting data from "smith". */
export type SmithOrderBy = {
  forged?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  identity?: InputMaybe<IdentityOrderBy>;
  identityId?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
  lastChanged?: InputMaybe<OrderBy>;
  lastForged?: InputMaybe<OrderBy>;
  smithCertIssuedAggregate?: InputMaybe<SmithCertAggregateOrderBy>;
  smithCertReceivedAggregate?: InputMaybe<SmithCertAggregateOrderBy>;
  smithHistoryAggregate?: InputMaybe<SmithEventAggregateOrderBy>;
  smithStatus?: InputMaybe<OrderBy>;
};

/** select columns of table "smith" */
export enum SmithSelectColumn {
  /** column name */
  Forged = 'forged',
  /** column name */
  Id = 'id',
  /** column name */
  IdentityId = 'identityId',
  /** column name */
  Index = 'index',
  /** column name */
  LastChanged = 'lastChanged',
  /** column name */
  LastForged = 'lastForged',
  /** column name */
  SmithStatus = 'smithStatus'
}

export enum SmithStatusEnum {
  Excluded = 'EXCLUDED',
  Invited = 'INVITED',
  Pending = 'PENDING',
  Smith = 'SMITH'
}

/** Boolean expression to compare columns of type "SmithStatusEnum". All fields are combined with logical 'AND'. */
export type SmithStatusEnumComparisonExp = {
  _eq?: InputMaybe<SmithStatusEnum>;
  _in?: InputMaybe<Array<SmithStatusEnum>>;
  _isNull?: InputMaybe<Scalars['Boolean']['input']>;
  _neq?: InputMaybe<SmithStatusEnum>;
  _nin?: InputMaybe<Array<SmithStatusEnum>>;
};

/** aggregate stddev on columns */
export type SmithStddevFields = {
  __typename?: 'SmithStddevFields';
  forged?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  lastChanged?: Maybe<Scalars['Float']['output']>;
  lastForged?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddevPop on columns */
export type SmithStddevPopFields = {
  __typename?: 'SmithStddevPopFields';
  forged?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  lastChanged?: Maybe<Scalars['Float']['output']>;
  lastForged?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddevSamp on columns */
export type SmithStddevSampFields = {
  __typename?: 'SmithStddevSampFields';
  forged?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  lastChanged?: Maybe<Scalars['Float']['output']>;
  lastForged?: Maybe<Scalars['Float']['output']>;
};

/** Streaming cursor of the table "smith" */
export type SmithStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: SmithStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type SmithStreamCursorValueInput = {
  forged?: InputMaybe<Scalars['Int']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  identityId?: InputMaybe<Scalars['String']['input']>;
  index?: InputMaybe<Scalars['Int']['input']>;
  lastChanged?: InputMaybe<Scalars['Int']['input']>;
  lastForged?: InputMaybe<Scalars['Int']['input']>;
  smithStatus?: InputMaybe<SmithStatusEnum>;
};

/** aggregate sum on columns */
export type SmithSumFields = {
  __typename?: 'SmithSumFields';
  forged?: Maybe<Scalars['Int']['output']>;
  index?: Maybe<Scalars['Int']['output']>;
  lastChanged?: Maybe<Scalars['Int']['output']>;
  lastForged?: Maybe<Scalars['Int']['output']>;
};

/** aggregate varPop on columns */
export type SmithVarPopFields = {
  __typename?: 'SmithVarPopFields';
  forged?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  lastChanged?: Maybe<Scalars['Float']['output']>;
  lastForged?: Maybe<Scalars['Float']['output']>;
};

/** aggregate varSamp on columns */
export type SmithVarSampFields = {
  __typename?: 'SmithVarSampFields';
  forged?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  lastChanged?: Maybe<Scalars['Float']['output']>;
  lastForged?: Maybe<Scalars['Float']['output']>;
};

/** aggregate variance on columns */
export type SmithVarianceFields = {
  __typename?: 'SmithVarianceFields';
  forged?: Maybe<Scalars['Float']['output']>;
  index?: Maybe<Scalars['Float']['output']>;
  lastChanged?: Maybe<Scalars['Float']['output']>;
  lastForged?: Maybe<Scalars['Float']['output']>;
};

/** Boolean expression to compare columns of type "String". All fields are combined with logical 'AND'. */
export type StringArrayComparisonExp = {
  /** is the array contained in the given array value */
  _containedIn?: InputMaybe<Array<Scalars['String']['input']>>;
  /** does the array contain the given value */
  _contains?: InputMaybe<Array<Scalars['String']['input']>>;
  _eq?: InputMaybe<Array<Scalars['String']['input']>>;
  _gt?: InputMaybe<Array<Scalars['String']['input']>>;
  _gte?: InputMaybe<Array<Scalars['String']['input']>>;
  _in?: InputMaybe<Array<Array<Scalars['String']['input']>>>;
  _isNull?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Array<Scalars['String']['input']>>;
  _lte?: InputMaybe<Array<Scalars['String']['input']>>;
  _neq?: InputMaybe<Array<Scalars['String']['input']>>;
  _nin?: InputMaybe<Array<Array<Scalars['String']['input']>>>;
};

/** Boolean expression to compare columns of type "String". All fields are combined with logical 'AND'. */
export type StringComparisonExp = {
  _eq?: InputMaybe<Scalars['String']['input']>;
  _gt?: InputMaybe<Scalars['String']['input']>;
  _gte?: InputMaybe<Scalars['String']['input']>;
  /** does the column match the given case-insensitive pattern */
  _ilike?: InputMaybe<Scalars['String']['input']>;
  _in?: InputMaybe<Array<Scalars['String']['input']>>;
  /** does the column match the given POSIX regular expression, case insensitive */
  _iregex?: InputMaybe<Scalars['String']['input']>;
  _isNull?: InputMaybe<Scalars['Boolean']['input']>;
  /** does the column match the given pattern */
  _like?: InputMaybe<Scalars['String']['input']>;
  _lt?: InputMaybe<Scalars['String']['input']>;
  _lte?: InputMaybe<Scalars['String']['input']>;
  _neq?: InputMaybe<Scalars['String']['input']>;
  /** does the column NOT match the given case-insensitive pattern */
  _nilike?: InputMaybe<Scalars['String']['input']>;
  _nin?: InputMaybe<Array<Scalars['String']['input']>>;
  /** does the column NOT match the given POSIX regular expression, case insensitive */
  _niregex?: InputMaybe<Scalars['String']['input']>;
  /** does the column NOT match the given pattern */
  _nlike?: InputMaybe<Scalars['String']['input']>;
  /** does the column NOT match the given POSIX regular expression, case sensitive */
  _nregex?: InputMaybe<Scalars['String']['input']>;
  /** does the column NOT match the given SQL regular expression */
  _nsimilar?: InputMaybe<Scalars['String']['input']>;
  /** does the column match the given POSIX regular expression, case sensitive */
  _regex?: InputMaybe<Scalars['String']['input']>;
  /** does the column match the given SQL regular expression */
  _similar?: InputMaybe<Scalars['String']['input']>;
};

/** Boolean expression to compare columns of type "timestamptz". All fields are combined with logical 'AND'. */
export type TimestamptzComparisonExp = {
  _eq?: InputMaybe<Scalars['timestamptz']['input']>;
  _gt?: InputMaybe<Scalars['timestamptz']['input']>;
  _gte?: InputMaybe<Scalars['timestamptz']['input']>;
  _in?: InputMaybe<Array<Scalars['timestamptz']['input']>>;
  _isNull?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['timestamptz']['input']>;
  _lte?: InputMaybe<Scalars['timestamptz']['input']>;
  _neq?: InputMaybe<Scalars['timestamptz']['input']>;
  _nin?: InputMaybe<Array<Scalars['timestamptz']['input']>>;
};

/** columns and relationships of "transfer" */
export type Transfer = {
  __typename?: 'Transfer';
  amount: Scalars['numeric']['output'];
  blockNumber: Scalars['Int']['output'];
  /** An object relationship */
  comment?: Maybe<TxComment>;
  commentId?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  event?: Maybe<Event>;
  eventId?: Maybe<Scalars['String']['output']>;
  /** An object relationship */
  from?: Maybe<Account>;
  fromId?: Maybe<Scalars['String']['output']>;
  id: Scalars['String']['output'];
  timestamp: Scalars['timestamptz']['output'];
  /** An object relationship */
  to?: Maybe<Account>;
  toId?: Maybe<Scalars['String']['output']>;
};

/** aggregated selection of "transfer" */
export type TransferAggregate = {
  __typename?: 'TransferAggregate';
  aggregate?: Maybe<TransferAggregateFields>;
  nodes: Array<Transfer>;
};

export type TransferAggregateBoolExp = {
  count?: InputMaybe<TransferAggregateBoolExpCount>;
};

/** aggregate fields of "transfer" */
export type TransferAggregateFields = {
  __typename?: 'TransferAggregateFields';
  avg?: Maybe<TransferAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<TransferMaxFields>;
  min?: Maybe<TransferMinFields>;
  stddev?: Maybe<TransferStddevFields>;
  stddevPop?: Maybe<TransferStddevPopFields>;
  stddevSamp?: Maybe<TransferStddevSampFields>;
  sum?: Maybe<TransferSumFields>;
  varPop?: Maybe<TransferVarPopFields>;
  varSamp?: Maybe<TransferVarSampFields>;
  variance?: Maybe<TransferVarianceFields>;
};


/** aggregate fields of "transfer" */
export type TransferAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<TransferSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "transfer" */
export type TransferAggregateOrderBy = {
  avg?: InputMaybe<TransferAvgOrderBy>;
  count?: InputMaybe<OrderBy>;
  max?: InputMaybe<TransferMaxOrderBy>;
  min?: InputMaybe<TransferMinOrderBy>;
  stddev?: InputMaybe<TransferStddevOrderBy>;
  stddevPop?: InputMaybe<TransferStddevPopOrderBy>;
  stddevSamp?: InputMaybe<TransferStddevSampOrderBy>;
  sum?: InputMaybe<TransferSumOrderBy>;
  varPop?: InputMaybe<TransferVarPopOrderBy>;
  varSamp?: InputMaybe<TransferVarSampOrderBy>;
  variance?: InputMaybe<TransferVarianceOrderBy>;
};

/** aggregate avg on columns */
export type TransferAvgFields = {
  __typename?: 'TransferAvgFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "transfer" */
export type TransferAvgOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "transfer". All fields are combined with a logical 'AND'. */
export type TransferBoolExp = {
  _and?: InputMaybe<Array<TransferBoolExp>>;
  _not?: InputMaybe<TransferBoolExp>;
  _or?: InputMaybe<Array<TransferBoolExp>>;
  amount?: InputMaybe<NumericComparisonExp>;
  blockNumber?: InputMaybe<IntComparisonExp>;
  comment?: InputMaybe<TxCommentBoolExp>;
  commentId?: InputMaybe<StringComparisonExp>;
  event?: InputMaybe<EventBoolExp>;
  eventId?: InputMaybe<StringComparisonExp>;
  from?: InputMaybe<AccountBoolExp>;
  fromId?: InputMaybe<StringComparisonExp>;
  id?: InputMaybe<StringComparisonExp>;
  timestamp?: InputMaybe<TimestamptzComparisonExp>;
  to?: InputMaybe<AccountBoolExp>;
  toId?: InputMaybe<StringComparisonExp>;
};

/** aggregate max on columns */
export type TransferMaxFields = {
  __typename?: 'TransferMaxFields';
  amount?: Maybe<Scalars['numeric']['output']>;
  blockNumber?: Maybe<Scalars['Int']['output']>;
  commentId?: Maybe<Scalars['String']['output']>;
  eventId?: Maybe<Scalars['String']['output']>;
  fromId?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  timestamp?: Maybe<Scalars['timestamptz']['output']>;
  toId?: Maybe<Scalars['String']['output']>;
};

/** order by max() on columns of table "transfer" */
export type TransferMaxOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
  commentId?: InputMaybe<OrderBy>;
  eventId?: InputMaybe<OrderBy>;
  fromId?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  timestamp?: InputMaybe<OrderBy>;
  toId?: InputMaybe<OrderBy>;
};

/** aggregate min on columns */
export type TransferMinFields = {
  __typename?: 'TransferMinFields';
  amount?: Maybe<Scalars['numeric']['output']>;
  blockNumber?: Maybe<Scalars['Int']['output']>;
  commentId?: Maybe<Scalars['String']['output']>;
  eventId?: Maybe<Scalars['String']['output']>;
  fromId?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  timestamp?: Maybe<Scalars['timestamptz']['output']>;
  toId?: Maybe<Scalars['String']['output']>;
};

/** order by min() on columns of table "transfer" */
export type TransferMinOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
  commentId?: InputMaybe<OrderBy>;
  eventId?: InputMaybe<OrderBy>;
  fromId?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  timestamp?: InputMaybe<OrderBy>;
  toId?: InputMaybe<OrderBy>;
};

/** Ordering options when selecting data from "transfer". */
export type TransferOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
  comment?: InputMaybe<TxCommentOrderBy>;
  commentId?: InputMaybe<OrderBy>;
  event?: InputMaybe<EventOrderBy>;
  eventId?: InputMaybe<OrderBy>;
  from?: InputMaybe<AccountOrderBy>;
  fromId?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  timestamp?: InputMaybe<OrderBy>;
  to?: InputMaybe<AccountOrderBy>;
  toId?: InputMaybe<OrderBy>;
};

/** select columns of table "transfer" */
export enum TransferSelectColumn {
  /** column name */
  Amount = 'amount',
  /** column name */
  BlockNumber = 'blockNumber',
  /** column name */
  CommentId = 'commentId',
  /** column name */
  EventId = 'eventId',
  /** column name */
  FromId = 'fromId',
  /** column name */
  Id = 'id',
  /** column name */
  Timestamp = 'timestamp',
  /** column name */
  ToId = 'toId'
}

/** aggregate stddev on columns */
export type TransferStddevFields = {
  __typename?: 'TransferStddevFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "transfer" */
export type TransferStddevOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate stddevPop on columns */
export type TransferStddevPopFields = {
  __typename?: 'TransferStddevPopFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevPop() on columns of table "transfer" */
export type TransferStddevPopOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate stddevSamp on columns */
export type TransferStddevSampFields = {
  __typename?: 'TransferStddevSampFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevSamp() on columns of table "transfer" */
export type TransferStddevSampOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
};

/** Streaming cursor of the table "transfer" */
export type TransferStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: TransferStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type TransferStreamCursorValueInput = {
  amount?: InputMaybe<Scalars['numeric']['input']>;
  blockNumber?: InputMaybe<Scalars['Int']['input']>;
  commentId?: InputMaybe<Scalars['String']['input']>;
  eventId?: InputMaybe<Scalars['String']['input']>;
  fromId?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  timestamp?: InputMaybe<Scalars['timestamptz']['input']>;
  toId?: InputMaybe<Scalars['String']['input']>;
};

/** aggregate sum on columns */
export type TransferSumFields = {
  __typename?: 'TransferSumFields';
  amount?: Maybe<Scalars['numeric']['output']>;
  blockNumber?: Maybe<Scalars['Int']['output']>;
};

/** order by sum() on columns of table "transfer" */
export type TransferSumOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate varPop on columns */
export type TransferVarPopFields = {
  __typename?: 'TransferVarPopFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by varPop() on columns of table "transfer" */
export type TransferVarPopOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate varSamp on columns */
export type TransferVarSampFields = {
  __typename?: 'TransferVarSampFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by varSamp() on columns of table "transfer" */
export type TransferVarSampOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate variance on columns */
export type TransferVarianceFields = {
  __typename?: 'TransferVarianceFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "transfer" */
export type TransferVarianceOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
};

/** columns and relationships of "tx_comment" */
export type TxComment = {
  __typename?: 'TxComment';
  /** An object relationship */
  author?: Maybe<Account>;
  authorId?: Maybe<Scalars['String']['output']>;
  blockNumber: Scalars['Int']['output'];
  /** An object relationship */
  event?: Maybe<Event>;
  eventId?: Maybe<Scalars['String']['output']>;
  hash: Scalars['String']['output'];
  id: Scalars['String']['output'];
  remark: Scalars['String']['output'];
  remarkBytes: Scalars['bytea']['output'];
  type?: Maybe<CommentTypeEnum>;
};

/** aggregated selection of "tx_comment" */
export type TxCommentAggregate = {
  __typename?: 'TxCommentAggregate';
  aggregate?: Maybe<TxCommentAggregateFields>;
  nodes: Array<TxComment>;
};

export type TxCommentAggregateBoolExp = {
  count?: InputMaybe<TxCommentAggregateBoolExpCount>;
};

/** aggregate fields of "tx_comment" */
export type TxCommentAggregateFields = {
  __typename?: 'TxCommentAggregateFields';
  avg?: Maybe<TxCommentAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<TxCommentMaxFields>;
  min?: Maybe<TxCommentMinFields>;
  stddev?: Maybe<TxCommentStddevFields>;
  stddevPop?: Maybe<TxCommentStddevPopFields>;
  stddevSamp?: Maybe<TxCommentStddevSampFields>;
  sum?: Maybe<TxCommentSumFields>;
  varPop?: Maybe<TxCommentVarPopFields>;
  varSamp?: Maybe<TxCommentVarSampFields>;
  variance?: Maybe<TxCommentVarianceFields>;
};


/** aggregate fields of "tx_comment" */
export type TxCommentAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<TxCommentSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "tx_comment" */
export type TxCommentAggregateOrderBy = {
  avg?: InputMaybe<TxCommentAvgOrderBy>;
  count?: InputMaybe<OrderBy>;
  max?: InputMaybe<TxCommentMaxOrderBy>;
  min?: InputMaybe<TxCommentMinOrderBy>;
  stddev?: InputMaybe<TxCommentStddevOrderBy>;
  stddevPop?: InputMaybe<TxCommentStddevPopOrderBy>;
  stddevSamp?: InputMaybe<TxCommentStddevSampOrderBy>;
  sum?: InputMaybe<TxCommentSumOrderBy>;
  varPop?: InputMaybe<TxCommentVarPopOrderBy>;
  varSamp?: InputMaybe<TxCommentVarSampOrderBy>;
  variance?: InputMaybe<TxCommentVarianceOrderBy>;
};

/** aggregate avg on columns */
export type TxCommentAvgFields = {
  __typename?: 'TxCommentAvgFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "tx_comment" */
export type TxCommentAvgOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "tx_comment". All fields are combined with a logical 'AND'. */
export type TxCommentBoolExp = {
  _and?: InputMaybe<Array<TxCommentBoolExp>>;
  _not?: InputMaybe<TxCommentBoolExp>;
  _or?: InputMaybe<Array<TxCommentBoolExp>>;
  author?: InputMaybe<AccountBoolExp>;
  authorId?: InputMaybe<StringComparisonExp>;
  blockNumber?: InputMaybe<IntComparisonExp>;
  event?: InputMaybe<EventBoolExp>;
  eventId?: InputMaybe<StringComparisonExp>;
  hash?: InputMaybe<StringComparisonExp>;
  id?: InputMaybe<StringComparisonExp>;
  remark?: InputMaybe<StringComparisonExp>;
  remarkBytes?: InputMaybe<ByteaComparisonExp>;
  type?: InputMaybe<CommentTypeEnumComparisonExp>;
};

/** aggregate max on columns */
export type TxCommentMaxFields = {
  __typename?: 'TxCommentMaxFields';
  authorId?: Maybe<Scalars['String']['output']>;
  blockNumber?: Maybe<Scalars['Int']['output']>;
  eventId?: Maybe<Scalars['String']['output']>;
  hash?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  remark?: Maybe<Scalars['String']['output']>;
};

/** order by max() on columns of table "tx_comment" */
export type TxCommentMaxOrderBy = {
  authorId?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
  eventId?: InputMaybe<OrderBy>;
  hash?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  remark?: InputMaybe<OrderBy>;
};

/** aggregate min on columns */
export type TxCommentMinFields = {
  __typename?: 'TxCommentMinFields';
  authorId?: Maybe<Scalars['String']['output']>;
  blockNumber?: Maybe<Scalars['Int']['output']>;
  eventId?: Maybe<Scalars['String']['output']>;
  hash?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  remark?: Maybe<Scalars['String']['output']>;
};

/** order by min() on columns of table "tx_comment" */
export type TxCommentMinOrderBy = {
  authorId?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
  eventId?: InputMaybe<OrderBy>;
  hash?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  remark?: InputMaybe<OrderBy>;
};

/** Ordering options when selecting data from "tx_comment". */
export type TxCommentOrderBy = {
  author?: InputMaybe<AccountOrderBy>;
  authorId?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
  event?: InputMaybe<EventOrderBy>;
  eventId?: InputMaybe<OrderBy>;
  hash?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  remark?: InputMaybe<OrderBy>;
  remarkBytes?: InputMaybe<OrderBy>;
  type?: InputMaybe<OrderBy>;
};

/** select columns of table "tx_comment" */
export enum TxCommentSelectColumn {
  /** column name */
  AuthorId = 'authorId',
  /** column name */
  BlockNumber = 'blockNumber',
  /** column name */
  EventId = 'eventId',
  /** column name */
  Hash = 'hash',
  /** column name */
  Id = 'id',
  /** column name */
  Remark = 'remark',
  /** column name */
  RemarkBytes = 'remarkBytes',
  /** column name */
  Type = 'type'
}

/** aggregate stddev on columns */
export type TxCommentStddevFields = {
  __typename?: 'TxCommentStddevFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "tx_comment" */
export type TxCommentStddevOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate stddevPop on columns */
export type TxCommentStddevPopFields = {
  __typename?: 'TxCommentStddevPopFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevPop() on columns of table "tx_comment" */
export type TxCommentStddevPopOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate stddevSamp on columns */
export type TxCommentStddevSampFields = {
  __typename?: 'TxCommentStddevSampFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevSamp() on columns of table "tx_comment" */
export type TxCommentStddevSampOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** Streaming cursor of the table "tx_comment" */
export type TxCommentStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: TxCommentStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type TxCommentStreamCursorValueInput = {
  authorId?: InputMaybe<Scalars['String']['input']>;
  blockNumber?: InputMaybe<Scalars['Int']['input']>;
  eventId?: InputMaybe<Scalars['String']['input']>;
  hash?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  remark?: InputMaybe<Scalars['String']['input']>;
  remarkBytes?: InputMaybe<Scalars['bytea']['input']>;
  type?: InputMaybe<CommentTypeEnum>;
};

/** aggregate sum on columns */
export type TxCommentSumFields = {
  __typename?: 'TxCommentSumFields';
  blockNumber?: Maybe<Scalars['Int']['output']>;
};

/** order by sum() on columns of table "tx_comment" */
export type TxCommentSumOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate varPop on columns */
export type TxCommentVarPopFields = {
  __typename?: 'TxCommentVarPopFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by varPop() on columns of table "tx_comment" */
export type TxCommentVarPopOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate varSamp on columns */
export type TxCommentVarSampFields = {
  __typename?: 'TxCommentVarSampFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by varSamp() on columns of table "tx_comment" */
export type TxCommentVarSampOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate variance on columns */
export type TxCommentVarianceFields = {
  __typename?: 'TxCommentVarianceFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "tx_comment" */
export type TxCommentVarianceOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
};

/** columns and relationships of "ud_history" */
export type UdHistory = {
  __typename?: 'UdHistory';
  amount: Scalars['numeric']['output'];
  blockNumber: Scalars['Int']['output'];
  id: Scalars['String']['output'];
  /** An object relationship */
  identity?: Maybe<Identity>;
  identityId?: Maybe<Scalars['String']['output']>;
  timestamp: Scalars['timestamptz']['output'];
};

export type UdHistoryAggregate = {
  __typename?: 'UdHistoryAggregate';
  aggregate?: Maybe<UdHistoryAggregateFields>;
  nodes: Array<UdHistory>;
};

/** aggregate fields of "ud_history" */
export type UdHistoryAggregateFields = {
  __typename?: 'UdHistoryAggregateFields';
  avg?: Maybe<UdHistoryAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<UdHistoryMaxFields>;
  min?: Maybe<UdHistoryMinFields>;
  stddev?: Maybe<UdHistoryStddevFields>;
  stddevPop?: Maybe<UdHistoryStddevPopFields>;
  stddevSamp?: Maybe<UdHistoryStddevSampFields>;
  sum?: Maybe<UdHistorySumFields>;
  varPop?: Maybe<UdHistoryVarPopFields>;
  varSamp?: Maybe<UdHistoryVarSampFields>;
  variance?: Maybe<UdHistoryVarianceFields>;
};


/** aggregate fields of "ud_history" */
export type UdHistoryAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<UdHistorySelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** order by aggregate values of table "ud_history" */
export type UdHistoryAggregateOrderBy = {
  avg?: InputMaybe<UdHistoryAvgOrderBy>;
  count?: InputMaybe<OrderBy>;
  max?: InputMaybe<UdHistoryMaxOrderBy>;
  min?: InputMaybe<UdHistoryMinOrderBy>;
  stddev?: InputMaybe<UdHistoryStddevOrderBy>;
  stddevPop?: InputMaybe<UdHistoryStddevPopOrderBy>;
  stddevSamp?: InputMaybe<UdHistoryStddevSampOrderBy>;
  sum?: InputMaybe<UdHistorySumOrderBy>;
  varPop?: InputMaybe<UdHistoryVarPopOrderBy>;
  varSamp?: InputMaybe<UdHistoryVarSampOrderBy>;
  variance?: InputMaybe<UdHistoryVarianceOrderBy>;
};

/** aggregate avg on columns */
export type UdHistoryAvgFields = {
  __typename?: 'UdHistoryAvgFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by avg() on columns of table "ud_history" */
export type UdHistoryAvgOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "ud_history". All fields are combined with a logical 'AND'. */
export type UdHistoryBoolExp = {
  _and?: InputMaybe<Array<UdHistoryBoolExp>>;
  _not?: InputMaybe<UdHistoryBoolExp>;
  _or?: InputMaybe<Array<UdHistoryBoolExp>>;
  amount?: InputMaybe<NumericComparisonExp>;
  blockNumber?: InputMaybe<IntComparisonExp>;
  id?: InputMaybe<StringComparisonExp>;
  identity?: InputMaybe<IdentityBoolExp>;
  identityId?: InputMaybe<StringComparisonExp>;
  timestamp?: InputMaybe<TimestamptzComparisonExp>;
};

/** aggregate max on columns */
export type UdHistoryMaxFields = {
  __typename?: 'UdHistoryMaxFields';
  amount?: Maybe<Scalars['numeric']['output']>;
  blockNumber?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  identityId?: Maybe<Scalars['String']['output']>;
  timestamp?: Maybe<Scalars['timestamptz']['output']>;
};

/** order by max() on columns of table "ud_history" */
export type UdHistoryMaxOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  identityId?: InputMaybe<OrderBy>;
  timestamp?: InputMaybe<OrderBy>;
};

/** aggregate min on columns */
export type UdHistoryMinFields = {
  __typename?: 'UdHistoryMinFields';
  amount?: Maybe<Scalars['numeric']['output']>;
  blockNumber?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  identityId?: Maybe<Scalars['String']['output']>;
  timestamp?: Maybe<Scalars['timestamptz']['output']>;
};

/** order by min() on columns of table "ud_history" */
export type UdHistoryMinOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  identityId?: InputMaybe<OrderBy>;
  timestamp?: InputMaybe<OrderBy>;
};

/** Ordering options when selecting data from "ud_history". */
export type UdHistoryOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  identity?: InputMaybe<IdentityOrderBy>;
  identityId?: InputMaybe<OrderBy>;
  timestamp?: InputMaybe<OrderBy>;
};

/** select columns of table "ud_history" */
export enum UdHistorySelectColumn {
  /** column name */
  Amount = 'amount',
  /** column name */
  BlockNumber = 'blockNumber',
  /** column name */
  Id = 'id',
  /** column name */
  IdentityId = 'identityId',
  /** column name */
  Timestamp = 'timestamp'
}

/** aggregate stddev on columns */
export type UdHistoryStddevFields = {
  __typename?: 'UdHistoryStddevFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddev() on columns of table "ud_history" */
export type UdHistoryStddevOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate stddevPop on columns */
export type UdHistoryStddevPopFields = {
  __typename?: 'UdHistoryStddevPopFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevPop() on columns of table "ud_history" */
export type UdHistoryStddevPopOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate stddevSamp on columns */
export type UdHistoryStddevSampFields = {
  __typename?: 'UdHistoryStddevSampFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by stddevSamp() on columns of table "ud_history" */
export type UdHistoryStddevSampOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
};

/** Streaming cursor of the table "ud_history" */
export type UdHistoryStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: UdHistoryStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type UdHistoryStreamCursorValueInput = {
  amount?: InputMaybe<Scalars['numeric']['input']>;
  blockNumber?: InputMaybe<Scalars['Int']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  identityId?: InputMaybe<Scalars['String']['input']>;
  timestamp?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate sum on columns */
export type UdHistorySumFields = {
  __typename?: 'UdHistorySumFields';
  amount?: Maybe<Scalars['numeric']['output']>;
  blockNumber?: Maybe<Scalars['Int']['output']>;
};

/** order by sum() on columns of table "ud_history" */
export type UdHistorySumOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate varPop on columns */
export type UdHistoryVarPopFields = {
  __typename?: 'UdHistoryVarPopFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by varPop() on columns of table "ud_history" */
export type UdHistoryVarPopOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate varSamp on columns */
export type UdHistoryVarSampFields = {
  __typename?: 'UdHistoryVarSampFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by varSamp() on columns of table "ud_history" */
export type UdHistoryVarSampOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
};

/** aggregate variance on columns */
export type UdHistoryVarianceFields = {
  __typename?: 'UdHistoryVarianceFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
};

/** order by variance() on columns of table "ud_history" */
export type UdHistoryVarianceOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
};

/** columns and relationships of "ud_reeval" */
export type UdReeval = {
  __typename?: 'UdReeval';
  blockNumber: Scalars['Int']['output'];
  /** An object relationship */
  event?: Maybe<Event>;
  eventId?: Maybe<Scalars['String']['output']>;
  id: Scalars['String']['output'];
  membersCount: Scalars['Int']['output'];
  monetaryMass: Scalars['numeric']['output'];
  newUdAmount: Scalars['numeric']['output'];
  timestamp: Scalars['timestamptz']['output'];
};

/** aggregated selection of "ud_reeval" */
export type UdReevalAggregate = {
  __typename?: 'UdReevalAggregate';
  aggregate?: Maybe<UdReevalAggregateFields>;
  nodes: Array<UdReeval>;
};

/** aggregate fields of "ud_reeval" */
export type UdReevalAggregateFields = {
  __typename?: 'UdReevalAggregateFields';
  avg?: Maybe<UdReevalAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<UdReevalMaxFields>;
  min?: Maybe<UdReevalMinFields>;
  stddev?: Maybe<UdReevalStddevFields>;
  stddevPop?: Maybe<UdReevalStddevPopFields>;
  stddevSamp?: Maybe<UdReevalStddevSampFields>;
  sum?: Maybe<UdReevalSumFields>;
  varPop?: Maybe<UdReevalVarPopFields>;
  varSamp?: Maybe<UdReevalVarSampFields>;
  variance?: Maybe<UdReevalVarianceFields>;
};


/** aggregate fields of "ud_reeval" */
export type UdReevalAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<UdReevalSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** aggregate avg on columns */
export type UdReevalAvgFields = {
  __typename?: 'UdReevalAvgFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
  membersCount?: Maybe<Scalars['Float']['output']>;
  monetaryMass?: Maybe<Scalars['Float']['output']>;
  newUdAmount?: Maybe<Scalars['Float']['output']>;
};

/** Boolean expression to filter rows from the table "ud_reeval". All fields are combined with a logical 'AND'. */
export type UdReevalBoolExp = {
  _and?: InputMaybe<Array<UdReevalBoolExp>>;
  _not?: InputMaybe<UdReevalBoolExp>;
  _or?: InputMaybe<Array<UdReevalBoolExp>>;
  blockNumber?: InputMaybe<IntComparisonExp>;
  event?: InputMaybe<EventBoolExp>;
  eventId?: InputMaybe<StringComparisonExp>;
  id?: InputMaybe<StringComparisonExp>;
  membersCount?: InputMaybe<IntComparisonExp>;
  monetaryMass?: InputMaybe<NumericComparisonExp>;
  newUdAmount?: InputMaybe<NumericComparisonExp>;
  timestamp?: InputMaybe<TimestamptzComparisonExp>;
};

/** aggregate max on columns */
export type UdReevalMaxFields = {
  __typename?: 'UdReevalMaxFields';
  blockNumber?: Maybe<Scalars['Int']['output']>;
  eventId?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  membersCount?: Maybe<Scalars['Int']['output']>;
  monetaryMass?: Maybe<Scalars['numeric']['output']>;
  newUdAmount?: Maybe<Scalars['numeric']['output']>;
  timestamp?: Maybe<Scalars['timestamptz']['output']>;
};

/** aggregate min on columns */
export type UdReevalMinFields = {
  __typename?: 'UdReevalMinFields';
  blockNumber?: Maybe<Scalars['Int']['output']>;
  eventId?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  membersCount?: Maybe<Scalars['Int']['output']>;
  monetaryMass?: Maybe<Scalars['numeric']['output']>;
  newUdAmount?: Maybe<Scalars['numeric']['output']>;
  timestamp?: Maybe<Scalars['timestamptz']['output']>;
};

/** Ordering options when selecting data from "ud_reeval". */
export type UdReevalOrderBy = {
  blockNumber?: InputMaybe<OrderBy>;
  event?: InputMaybe<EventOrderBy>;
  eventId?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  membersCount?: InputMaybe<OrderBy>;
  monetaryMass?: InputMaybe<OrderBy>;
  newUdAmount?: InputMaybe<OrderBy>;
  timestamp?: InputMaybe<OrderBy>;
};

/** select columns of table "ud_reeval" */
export enum UdReevalSelectColumn {
  /** column name */
  BlockNumber = 'blockNumber',
  /** column name */
  EventId = 'eventId',
  /** column name */
  Id = 'id',
  /** column name */
  MembersCount = 'membersCount',
  /** column name */
  MonetaryMass = 'monetaryMass',
  /** column name */
  NewUdAmount = 'newUdAmount',
  /** column name */
  Timestamp = 'timestamp'
}

/** aggregate stddev on columns */
export type UdReevalStddevFields = {
  __typename?: 'UdReevalStddevFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
  membersCount?: Maybe<Scalars['Float']['output']>;
  monetaryMass?: Maybe<Scalars['Float']['output']>;
  newUdAmount?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddevPop on columns */
export type UdReevalStddevPopFields = {
  __typename?: 'UdReevalStddevPopFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
  membersCount?: Maybe<Scalars['Float']['output']>;
  monetaryMass?: Maybe<Scalars['Float']['output']>;
  newUdAmount?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddevSamp on columns */
export type UdReevalStddevSampFields = {
  __typename?: 'UdReevalStddevSampFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
  membersCount?: Maybe<Scalars['Float']['output']>;
  monetaryMass?: Maybe<Scalars['Float']['output']>;
  newUdAmount?: Maybe<Scalars['Float']['output']>;
};

/** Streaming cursor of the table "ud_reeval" */
export type UdReevalStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: UdReevalStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type UdReevalStreamCursorValueInput = {
  blockNumber?: InputMaybe<Scalars['Int']['input']>;
  eventId?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  membersCount?: InputMaybe<Scalars['Int']['input']>;
  monetaryMass?: InputMaybe<Scalars['numeric']['input']>;
  newUdAmount?: InputMaybe<Scalars['numeric']['input']>;
  timestamp?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate sum on columns */
export type UdReevalSumFields = {
  __typename?: 'UdReevalSumFields';
  blockNumber?: Maybe<Scalars['Int']['output']>;
  membersCount?: Maybe<Scalars['Int']['output']>;
  monetaryMass?: Maybe<Scalars['numeric']['output']>;
  newUdAmount?: Maybe<Scalars['numeric']['output']>;
};

/** aggregate varPop on columns */
export type UdReevalVarPopFields = {
  __typename?: 'UdReevalVarPopFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
  membersCount?: Maybe<Scalars['Float']['output']>;
  monetaryMass?: Maybe<Scalars['Float']['output']>;
  newUdAmount?: Maybe<Scalars['Float']['output']>;
};

/** aggregate varSamp on columns */
export type UdReevalVarSampFields = {
  __typename?: 'UdReevalVarSampFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
  membersCount?: Maybe<Scalars['Float']['output']>;
  monetaryMass?: Maybe<Scalars['Float']['output']>;
  newUdAmount?: Maybe<Scalars['Float']['output']>;
};

/** aggregate variance on columns */
export type UdReevalVarianceFields = {
  __typename?: 'UdReevalVarianceFields';
  blockNumber?: Maybe<Scalars['Float']['output']>;
  membersCount?: Maybe<Scalars['Float']['output']>;
  monetaryMass?: Maybe<Scalars['Float']['output']>;
  newUdAmount?: Maybe<Scalars['Float']['output']>;
};

/** columns and relationships of "universal_dividend" */
export type UniversalDividend = {
  __typename?: 'UniversalDividend';
  amount: Scalars['numeric']['output'];
  blockNumber: Scalars['Int']['output'];
  /** An object relationship */
  event?: Maybe<Event>;
  eventId?: Maybe<Scalars['String']['output']>;
  id: Scalars['String']['output'];
  membersCount: Scalars['Int']['output'];
  monetaryMass: Scalars['numeric']['output'];
  timestamp: Scalars['timestamptz']['output'];
};

/** aggregated selection of "universal_dividend" */
export type UniversalDividendAggregate = {
  __typename?: 'UniversalDividendAggregate';
  aggregate?: Maybe<UniversalDividendAggregateFields>;
  nodes: Array<UniversalDividend>;
};

/** aggregate fields of "universal_dividend" */
export type UniversalDividendAggregateFields = {
  __typename?: 'UniversalDividendAggregateFields';
  avg?: Maybe<UniversalDividendAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<UniversalDividendMaxFields>;
  min?: Maybe<UniversalDividendMinFields>;
  stddev?: Maybe<UniversalDividendStddevFields>;
  stddevPop?: Maybe<UniversalDividendStddevPopFields>;
  stddevSamp?: Maybe<UniversalDividendStddevSampFields>;
  sum?: Maybe<UniversalDividendSumFields>;
  varPop?: Maybe<UniversalDividendVarPopFields>;
  varSamp?: Maybe<UniversalDividendVarSampFields>;
  variance?: Maybe<UniversalDividendVarianceFields>;
};


/** aggregate fields of "universal_dividend" */
export type UniversalDividendAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<UniversalDividendSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** aggregate avg on columns */
export type UniversalDividendAvgFields = {
  __typename?: 'UniversalDividendAvgFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
  membersCount?: Maybe<Scalars['Float']['output']>;
  monetaryMass?: Maybe<Scalars['Float']['output']>;
};

/** Boolean expression to filter rows from the table "universal_dividend". All fields are combined with a logical 'AND'. */
export type UniversalDividendBoolExp = {
  _and?: InputMaybe<Array<UniversalDividendBoolExp>>;
  _not?: InputMaybe<UniversalDividendBoolExp>;
  _or?: InputMaybe<Array<UniversalDividendBoolExp>>;
  amount?: InputMaybe<NumericComparisonExp>;
  blockNumber?: InputMaybe<IntComparisonExp>;
  event?: InputMaybe<EventBoolExp>;
  eventId?: InputMaybe<StringComparisonExp>;
  id?: InputMaybe<StringComparisonExp>;
  membersCount?: InputMaybe<IntComparisonExp>;
  monetaryMass?: InputMaybe<NumericComparisonExp>;
  timestamp?: InputMaybe<TimestamptzComparisonExp>;
};

/** aggregate max on columns */
export type UniversalDividendMaxFields = {
  __typename?: 'UniversalDividendMaxFields';
  amount?: Maybe<Scalars['numeric']['output']>;
  blockNumber?: Maybe<Scalars['Int']['output']>;
  eventId?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  membersCount?: Maybe<Scalars['Int']['output']>;
  monetaryMass?: Maybe<Scalars['numeric']['output']>;
  timestamp?: Maybe<Scalars['timestamptz']['output']>;
};

/** aggregate min on columns */
export type UniversalDividendMinFields = {
  __typename?: 'UniversalDividendMinFields';
  amount?: Maybe<Scalars['numeric']['output']>;
  blockNumber?: Maybe<Scalars['Int']['output']>;
  eventId?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  membersCount?: Maybe<Scalars['Int']['output']>;
  monetaryMass?: Maybe<Scalars['numeric']['output']>;
  timestamp?: Maybe<Scalars['timestamptz']['output']>;
};

/** Ordering options when selecting data from "universal_dividend". */
export type UniversalDividendOrderBy = {
  amount?: InputMaybe<OrderBy>;
  blockNumber?: InputMaybe<OrderBy>;
  event?: InputMaybe<EventOrderBy>;
  eventId?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  membersCount?: InputMaybe<OrderBy>;
  monetaryMass?: InputMaybe<OrderBy>;
  timestamp?: InputMaybe<OrderBy>;
};

/** select columns of table "universal_dividend" */
export enum UniversalDividendSelectColumn {
  /** column name */
  Amount = 'amount',
  /** column name */
  BlockNumber = 'blockNumber',
  /** column name */
  EventId = 'eventId',
  /** column name */
  Id = 'id',
  /** column name */
  MembersCount = 'membersCount',
  /** column name */
  MonetaryMass = 'monetaryMass',
  /** column name */
  Timestamp = 'timestamp'
}

/** aggregate stddev on columns */
export type UniversalDividendStddevFields = {
  __typename?: 'UniversalDividendStddevFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
  membersCount?: Maybe<Scalars['Float']['output']>;
  monetaryMass?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddevPop on columns */
export type UniversalDividendStddevPopFields = {
  __typename?: 'UniversalDividendStddevPopFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
  membersCount?: Maybe<Scalars['Float']['output']>;
  monetaryMass?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddevSamp on columns */
export type UniversalDividendStddevSampFields = {
  __typename?: 'UniversalDividendStddevSampFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
  membersCount?: Maybe<Scalars['Float']['output']>;
  monetaryMass?: Maybe<Scalars['Float']['output']>;
};

/** Streaming cursor of the table "universal_dividend" */
export type UniversalDividendStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: UniversalDividendStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type UniversalDividendStreamCursorValueInput = {
  amount?: InputMaybe<Scalars['numeric']['input']>;
  blockNumber?: InputMaybe<Scalars['Int']['input']>;
  eventId?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  membersCount?: InputMaybe<Scalars['Int']['input']>;
  monetaryMass?: InputMaybe<Scalars['numeric']['input']>;
  timestamp?: InputMaybe<Scalars['timestamptz']['input']>;
};

/** aggregate sum on columns */
export type UniversalDividendSumFields = {
  __typename?: 'UniversalDividendSumFields';
  amount?: Maybe<Scalars['numeric']['output']>;
  blockNumber?: Maybe<Scalars['Int']['output']>;
  membersCount?: Maybe<Scalars['Int']['output']>;
  monetaryMass?: Maybe<Scalars['numeric']['output']>;
};

/** aggregate varPop on columns */
export type UniversalDividendVarPopFields = {
  __typename?: 'UniversalDividendVarPopFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
  membersCount?: Maybe<Scalars['Float']['output']>;
  monetaryMass?: Maybe<Scalars['Float']['output']>;
};

/** aggregate varSamp on columns */
export type UniversalDividendVarSampFields = {
  __typename?: 'UniversalDividendVarSampFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
  membersCount?: Maybe<Scalars['Float']['output']>;
  monetaryMass?: Maybe<Scalars['Float']['output']>;
};

/** aggregate variance on columns */
export type UniversalDividendVarianceFields = {
  __typename?: 'UniversalDividendVarianceFields';
  amount?: Maybe<Scalars['Float']['output']>;
  blockNumber?: Maybe<Scalars['Float']['output']>;
  membersCount?: Maybe<Scalars['Float']['output']>;
  monetaryMass?: Maybe<Scalars['Float']['output']>;
};

/** columns and relationships of "validator" */
export type Validator = {
  __typename?: 'Validator';
  id: Scalars['String']['output'];
  index: Scalars['Int']['output'];
};

/** aggregated selection of "validator" */
export type ValidatorAggregate = {
  __typename?: 'ValidatorAggregate';
  aggregate?: Maybe<ValidatorAggregateFields>;
  nodes: Array<Validator>;
};

/** aggregate fields of "validator" */
export type ValidatorAggregateFields = {
  __typename?: 'ValidatorAggregateFields';
  avg?: Maybe<ValidatorAvgFields>;
  count: Scalars['Int']['output'];
  max?: Maybe<ValidatorMaxFields>;
  min?: Maybe<ValidatorMinFields>;
  stddev?: Maybe<ValidatorStddevFields>;
  stddevPop?: Maybe<ValidatorStddevPopFields>;
  stddevSamp?: Maybe<ValidatorStddevSampFields>;
  sum?: Maybe<ValidatorSumFields>;
  varPop?: Maybe<ValidatorVarPopFields>;
  varSamp?: Maybe<ValidatorVarSampFields>;
  variance?: Maybe<ValidatorVarianceFields>;
};


/** aggregate fields of "validator" */
export type ValidatorAggregateFieldsCountArgs = {
  columns?: InputMaybe<Array<ValidatorSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** aggregate avg on columns */
export type ValidatorAvgFields = {
  __typename?: 'ValidatorAvgFields';
  index?: Maybe<Scalars['Float']['output']>;
};

/** Boolean expression to filter rows from the table "validator". All fields are combined with a logical 'AND'. */
export type ValidatorBoolExp = {
  _and?: InputMaybe<Array<ValidatorBoolExp>>;
  _not?: InputMaybe<ValidatorBoolExp>;
  _or?: InputMaybe<Array<ValidatorBoolExp>>;
  id?: InputMaybe<StringComparisonExp>;
  index?: InputMaybe<IntComparisonExp>;
};

/** aggregate max on columns */
export type ValidatorMaxFields = {
  __typename?: 'ValidatorMaxFields';
  id?: Maybe<Scalars['String']['output']>;
  index?: Maybe<Scalars['Int']['output']>;
};

/** aggregate min on columns */
export type ValidatorMinFields = {
  __typename?: 'ValidatorMinFields';
  id?: Maybe<Scalars['String']['output']>;
  index?: Maybe<Scalars['Int']['output']>;
};

/** Ordering options when selecting data from "validator". */
export type ValidatorOrderBy = {
  id?: InputMaybe<OrderBy>;
  index?: InputMaybe<OrderBy>;
};

/** select columns of table "validator" */
export enum ValidatorSelectColumn {
  /** column name */
  Id = 'id',
  /** column name */
  Index = 'index'
}

/** aggregate stddev on columns */
export type ValidatorStddevFields = {
  __typename?: 'ValidatorStddevFields';
  index?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddevPop on columns */
export type ValidatorStddevPopFields = {
  __typename?: 'ValidatorStddevPopFields';
  index?: Maybe<Scalars['Float']['output']>;
};

/** aggregate stddevSamp on columns */
export type ValidatorStddevSampFields = {
  __typename?: 'ValidatorStddevSampFields';
  index?: Maybe<Scalars['Float']['output']>;
};

/** Streaming cursor of the table "validator" */
export type ValidatorStreamCursorInput = {
  /** Stream column input with initial value */
  initialValue: ValidatorStreamCursorValueInput;
  /** cursor ordering */
  ordering?: InputMaybe<CursorOrdering>;
};

/** Initial value of the column from where the streaming should start */
export type ValidatorStreamCursorValueInput = {
  id?: InputMaybe<Scalars['String']['input']>;
  index?: InputMaybe<Scalars['Int']['input']>;
};

/** aggregate sum on columns */
export type ValidatorSumFields = {
  __typename?: 'ValidatorSumFields';
  index?: Maybe<Scalars['Int']['output']>;
};

/** aggregate varPop on columns */
export type ValidatorVarPopFields = {
  __typename?: 'ValidatorVarPopFields';
  index?: Maybe<Scalars['Float']['output']>;
};

/** aggregate varSamp on columns */
export type ValidatorVarSampFields = {
  __typename?: 'ValidatorVarSampFields';
  index?: Maybe<Scalars['Float']['output']>;
};

/** aggregate variance on columns */
export type ValidatorVarianceFields = {
  __typename?: 'ValidatorVarianceFields';
  index?: Maybe<Scalars['Float']['output']>;
};

export type AccountAggregateBoolExpBool_And = {
  arguments: AccountSelectColumnAccountAggregateBoolExpBool_AndArgumentsColumns;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<AccountBoolExp>;
  predicate: BooleanComparisonExp;
};

export type AccountAggregateBoolExpBool_Or = {
  arguments: AccountSelectColumnAccountAggregateBoolExpBool_OrArgumentsColumns;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<AccountBoolExp>;
  predicate: BooleanComparisonExp;
};

export type AccountAggregateBoolExpCount = {
  arguments?: InputMaybe<Array<AccountSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<AccountBoolExp>;
  predicate: IntComparisonExp;
};

export type CallAggregateBoolExpBool_And = {
  arguments: CallSelectColumnCallAggregateBoolExpBool_AndArgumentsColumns;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<CallBoolExp>;
  predicate: BooleanComparisonExp;
};

export type CallAggregateBoolExpBool_Or = {
  arguments: CallSelectColumnCallAggregateBoolExpBool_OrArgumentsColumns;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<CallBoolExp>;
  predicate: BooleanComparisonExp;
};

export type CallAggregateBoolExpCount = {
  arguments?: InputMaybe<Array<CallSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<CallBoolExp>;
  predicate: IntComparisonExp;
};

export type CertAggregateBoolExpBool_And = {
  arguments: CertSelectColumnCertAggregateBoolExpBool_AndArgumentsColumns;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<CertBoolExp>;
  predicate: BooleanComparisonExp;
};

export type CertAggregateBoolExpBool_Or = {
  arguments: CertSelectColumnCertAggregateBoolExpBool_OrArgumentsColumns;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<CertBoolExp>;
  predicate: BooleanComparisonExp;
};

export type CertAggregateBoolExpCount = {
  arguments?: InputMaybe<Array<CertSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<CertBoolExp>;
  predicate: IntComparisonExp;
};

export type CertEventAggregateBoolExpCount = {
  arguments?: InputMaybe<Array<CertEventSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<CertEventBoolExp>;
  predicate: IntComparisonExp;
};

export type ChangeOwnerKeyAggregateBoolExpCount = {
  arguments?: InputMaybe<Array<ChangeOwnerKeySelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<ChangeOwnerKeyBoolExp>;
  predicate: IntComparisonExp;
};

export type EventAggregateBoolExpCount = {
  arguments?: InputMaybe<Array<EventSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<EventBoolExp>;
  predicate: IntComparisonExp;
};

export type ExtrinsicAggregateBoolExpBool_And = {
  arguments: ExtrinsicSelectColumnExtrinsicAggregateBoolExpBool_AndArgumentsColumns;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<ExtrinsicBoolExp>;
  predicate: BooleanComparisonExp;
};

export type ExtrinsicAggregateBoolExpBool_Or = {
  arguments: ExtrinsicSelectColumnExtrinsicAggregateBoolExpBool_OrArgumentsColumns;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<ExtrinsicBoolExp>;
  predicate: BooleanComparisonExp;
};

export type ExtrinsicAggregateBoolExpCount = {
  arguments?: InputMaybe<Array<ExtrinsicSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<ExtrinsicBoolExp>;
  predicate: IntComparisonExp;
};

export type GetUdHistoryArgs = {
  identity_row?: InputMaybe<Scalars['identity_scalar']['input']>;
};

export type IdentityAggregateBoolExpBool_And = {
  arguments: IdentitySelectColumnIdentityAggregateBoolExpBool_AndArgumentsColumns;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<IdentityBoolExp>;
  predicate: BooleanComparisonExp;
};

export type IdentityAggregateBoolExpBool_Or = {
  arguments: IdentitySelectColumnIdentityAggregateBoolExpBool_OrArgumentsColumns;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<IdentityBoolExp>;
  predicate: BooleanComparisonExp;
};

export type IdentityAggregateBoolExpCount = {
  arguments?: InputMaybe<Array<IdentitySelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<IdentityBoolExp>;
  predicate: IntComparisonExp;
};

export type MembershipEventAggregateBoolExpCount = {
  arguments?: InputMaybe<Array<MembershipEventSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<MembershipEventBoolExp>;
  predicate: IntComparisonExp;
};

export type Query_Root = {
  __typename?: 'query_root';
  /** fetch data from the table: "account" */
  account: Array<Account>;
  /** fetch aggregated fields from the table: "account" */
  accountAggregate: AccountAggregate;
  /** fetch data from the table: "account" using primary key columns */
  accountByPk?: Maybe<Account>;
  /** fetch data from the table: "block" */
  block: Array<Block>;
  /** fetch aggregated fields from the table: "block" */
  blockAggregate: BlockAggregate;
  /** fetch data from the table: "block" using primary key columns */
  blockByPk?: Maybe<Block>;
  /** fetch data from the table: "call" */
  call: Array<Call>;
  /** fetch aggregated fields from the table: "call" */
  callAggregate: CallAggregate;
  /** fetch data from the table: "call" using primary key columns */
  callByPk?: Maybe<Call>;
  /** fetch data from the table: "cert" */
  cert: Array<Cert>;
  /** fetch aggregated fields from the table: "cert" */
  certAggregate: CertAggregate;
  /** fetch data from the table: "cert" using primary key columns */
  certByPk?: Maybe<Cert>;
  /** fetch data from the table: "cert_event" */
  certEvent: Array<CertEvent>;
  /** fetch aggregated fields from the table: "cert_event" */
  certEventAggregate: CertEventAggregate;
  /** fetch data from the table: "cert_event" using primary key columns */
  certEventByPk?: Maybe<CertEvent>;
  /** fetch data from the table: "change_owner_key" */
  changeOwnerKey: Array<ChangeOwnerKey>;
  /** fetch aggregated fields from the table: "change_owner_key" */
  changeOwnerKeyAggregate: ChangeOwnerKeyAggregate;
  /** fetch data from the table: "change_owner_key" using primary key columns */
  changeOwnerKeyByPk?: Maybe<ChangeOwnerKey>;
  /** fetch data from the table: "event" */
  event: Array<Event>;
  /** fetch aggregated fields from the table: "event" */
  eventAggregate: EventAggregate;
  /** fetch data from the table: "event" using primary key columns */
  eventByPk?: Maybe<Event>;
  /** fetch data from the table: "extrinsic" */
  extrinsic: Array<Extrinsic>;
  /** fetch aggregated fields from the table: "extrinsic" */
  extrinsicAggregate: ExtrinsicAggregate;
  /** fetch data from the table: "extrinsic" using primary key columns */
  extrinsicByPk?: Maybe<Extrinsic>;
  /** execute function "get_ud_history" which returns "ud_history" */
  getUdHistory: Array<UdHistory>;
  /** execute function "get_ud_history" and query aggregates on result of table type "ud_history" */
  getUdHistoryAggregate: UdHistoryAggregate;
  /** fetch data from the table: "identity" */
  identity: Array<Identity>;
  /** fetch aggregated fields from the table: "identity" */
  identityAggregate: IdentityAggregate;
  /** fetch data from the table: "identity" using primary key columns */
  identityByPk?: Maybe<Identity>;
  /** fetch data from the table: "items_counter" */
  itemsCounter: Array<ItemsCounter>;
  /** fetch aggregated fields from the table: "items_counter" */
  itemsCounterAggregate: ItemsCounterAggregate;
  /** fetch data from the table: "items_counter" using primary key columns */
  itemsCounterByPk?: Maybe<ItemsCounter>;
  /** fetch data from the table: "membership_event" */
  membershipEvent: Array<MembershipEvent>;
  /** fetch aggregated fields from the table: "membership_event" */
  membershipEventAggregate: MembershipEventAggregate;
  /** fetch data from the table: "membership_event" using primary key columns */
  membershipEventByPk?: Maybe<MembershipEvent>;
  /** fetch data from the table: "population_history" */
  populationHistory: Array<PopulationHistory>;
  /** fetch aggregated fields from the table: "population_history" */
  populationHistoryAggregate: PopulationHistoryAggregate;
  /** fetch data from the table: "population_history" using primary key columns */
  populationHistoryByPk?: Maybe<PopulationHistory>;
  /** fetch data from the table: "smith" */
  smith: Array<Smith>;
  /** fetch aggregated fields from the table: "smith" */
  smithAggregate: SmithAggregate;
  /** fetch data from the table: "smith" using primary key columns */
  smithByPk?: Maybe<Smith>;
  /** fetch data from the table: "smith_cert" */
  smithCert: Array<SmithCert>;
  /** fetch aggregated fields from the table: "smith_cert" */
  smithCertAggregate: SmithCertAggregate;
  /** fetch data from the table: "smith_cert" using primary key columns */
  smithCertByPk?: Maybe<SmithCert>;
  /** fetch data from the table: "smith_event" */
  smithEvent: Array<SmithEvent>;
  /** fetch aggregated fields from the table: "smith_event" */
  smithEventAggregate: SmithEventAggregate;
  /** fetch data from the table: "smith_event" using primary key columns */
  smithEventByPk?: Maybe<SmithEvent>;
  /** fetch data from the table: "transfer" */
  transfer: Array<Transfer>;
  /** fetch aggregated fields from the table: "transfer" */
  transferAggregate: TransferAggregate;
  /** fetch data from the table: "transfer" using primary key columns */
  transferByPk?: Maybe<Transfer>;
  /** fetch data from the table: "tx_comment" */
  txComment: Array<TxComment>;
  /** fetch aggregated fields from the table: "tx_comment" */
  txCommentAggregate: TxCommentAggregate;
  /** fetch data from the table: "tx_comment" using primary key columns */
  txCommentByPk?: Maybe<TxComment>;
  /** fetch data from the table: "ud_history" */
  udHistory: Array<UdHistory>;
  /** fetch aggregated fields from the table: "ud_history" */
  udHistoryAggregate: UdHistoryAggregate;
  /** fetch data from the table: "ud_history" using primary key columns */
  udHistoryByPk?: Maybe<UdHistory>;
  /** fetch data from the table: "ud_reeval" */
  udReeval: Array<UdReeval>;
  /** fetch aggregated fields from the table: "ud_reeval" */
  udReevalAggregate: UdReevalAggregate;
  /** fetch data from the table: "ud_reeval" using primary key columns */
  udReevalByPk?: Maybe<UdReeval>;
  /** fetch data from the table: "universal_dividend" */
  universalDividend: Array<UniversalDividend>;
  /** fetch aggregated fields from the table: "universal_dividend" */
  universalDividendAggregate: UniversalDividendAggregate;
  /** fetch data from the table: "universal_dividend" using primary key columns */
  universalDividendByPk?: Maybe<UniversalDividend>;
  /** fetch data from the table: "validator" */
  validator: Array<Validator>;
  /** fetch aggregated fields from the table: "validator" */
  validatorAggregate: ValidatorAggregate;
  /** fetch data from the table: "validator" using primary key columns */
  validatorByPk?: Maybe<Validator>;
};


export type Query_RootAccountArgs = {
  distinctOn?: InputMaybe<Array<AccountSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<AccountOrderBy>>;
  where?: InputMaybe<AccountBoolExp>;
};


export type Query_RootAccountAggregateArgs = {
  distinctOn?: InputMaybe<Array<AccountSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<AccountOrderBy>>;
  where?: InputMaybe<AccountBoolExp>;
};


export type Query_RootAccountByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootBlockArgs = {
  distinctOn?: InputMaybe<Array<BlockSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<BlockOrderBy>>;
  where?: InputMaybe<BlockBoolExp>;
};


export type Query_RootBlockAggregateArgs = {
  distinctOn?: InputMaybe<Array<BlockSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<BlockOrderBy>>;
  where?: InputMaybe<BlockBoolExp>;
};


export type Query_RootBlockByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootCallArgs = {
  distinctOn?: InputMaybe<Array<CallSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CallOrderBy>>;
  where?: InputMaybe<CallBoolExp>;
};


export type Query_RootCallAggregateArgs = {
  distinctOn?: InputMaybe<Array<CallSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CallOrderBy>>;
  where?: InputMaybe<CallBoolExp>;
};


export type Query_RootCallByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootCertArgs = {
  distinctOn?: InputMaybe<Array<CertSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CertOrderBy>>;
  where?: InputMaybe<CertBoolExp>;
};


export type Query_RootCertAggregateArgs = {
  distinctOn?: InputMaybe<Array<CertSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CertOrderBy>>;
  where?: InputMaybe<CertBoolExp>;
};


export type Query_RootCertByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootCertEventArgs = {
  distinctOn?: InputMaybe<Array<CertEventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CertEventOrderBy>>;
  where?: InputMaybe<CertEventBoolExp>;
};


export type Query_RootCertEventAggregateArgs = {
  distinctOn?: InputMaybe<Array<CertEventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CertEventOrderBy>>;
  where?: InputMaybe<CertEventBoolExp>;
};


export type Query_RootCertEventByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootChangeOwnerKeyArgs = {
  distinctOn?: InputMaybe<Array<ChangeOwnerKeySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ChangeOwnerKeyOrderBy>>;
  where?: InputMaybe<ChangeOwnerKeyBoolExp>;
};


export type Query_RootChangeOwnerKeyAggregateArgs = {
  distinctOn?: InputMaybe<Array<ChangeOwnerKeySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ChangeOwnerKeyOrderBy>>;
  where?: InputMaybe<ChangeOwnerKeyBoolExp>;
};


export type Query_RootChangeOwnerKeyByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootEventArgs = {
  distinctOn?: InputMaybe<Array<EventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<EventOrderBy>>;
  where?: InputMaybe<EventBoolExp>;
};


export type Query_RootEventAggregateArgs = {
  distinctOn?: InputMaybe<Array<EventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<EventOrderBy>>;
  where?: InputMaybe<EventBoolExp>;
};


export type Query_RootEventByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootExtrinsicArgs = {
  distinctOn?: InputMaybe<Array<ExtrinsicSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ExtrinsicOrderBy>>;
  where?: InputMaybe<ExtrinsicBoolExp>;
};


export type Query_RootExtrinsicAggregateArgs = {
  distinctOn?: InputMaybe<Array<ExtrinsicSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ExtrinsicOrderBy>>;
  where?: InputMaybe<ExtrinsicBoolExp>;
};


export type Query_RootExtrinsicByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootGetUdHistoryArgs = {
  args: GetUdHistoryArgs;
  distinctOn?: InputMaybe<Array<UdHistorySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UdHistoryOrderBy>>;
  where?: InputMaybe<UdHistoryBoolExp>;
};


export type Query_RootGetUdHistoryAggregateArgs = {
  args: GetUdHistoryArgs;
  distinctOn?: InputMaybe<Array<UdHistorySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UdHistoryOrderBy>>;
  where?: InputMaybe<UdHistoryBoolExp>;
};


export type Query_RootIdentityArgs = {
  distinctOn?: InputMaybe<Array<IdentitySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<IdentityOrderBy>>;
  where?: InputMaybe<IdentityBoolExp>;
};


export type Query_RootIdentityAggregateArgs = {
  distinctOn?: InputMaybe<Array<IdentitySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<IdentityOrderBy>>;
  where?: InputMaybe<IdentityBoolExp>;
};


export type Query_RootIdentityByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootItemsCounterArgs = {
  distinctOn?: InputMaybe<Array<ItemsCounterSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ItemsCounterOrderBy>>;
  where?: InputMaybe<ItemsCounterBoolExp>;
};


export type Query_RootItemsCounterAggregateArgs = {
  distinctOn?: InputMaybe<Array<ItemsCounterSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ItemsCounterOrderBy>>;
  where?: InputMaybe<ItemsCounterBoolExp>;
};


export type Query_RootItemsCounterByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootMembershipEventArgs = {
  distinctOn?: InputMaybe<Array<MembershipEventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<MembershipEventOrderBy>>;
  where?: InputMaybe<MembershipEventBoolExp>;
};


export type Query_RootMembershipEventAggregateArgs = {
  distinctOn?: InputMaybe<Array<MembershipEventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<MembershipEventOrderBy>>;
  where?: InputMaybe<MembershipEventBoolExp>;
};


export type Query_RootMembershipEventByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootPopulationHistoryArgs = {
  distinctOn?: InputMaybe<Array<PopulationHistorySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<PopulationHistoryOrderBy>>;
  where?: InputMaybe<PopulationHistoryBoolExp>;
};


export type Query_RootPopulationHistoryAggregateArgs = {
  distinctOn?: InputMaybe<Array<PopulationHistorySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<PopulationHistoryOrderBy>>;
  where?: InputMaybe<PopulationHistoryBoolExp>;
};


export type Query_RootPopulationHistoryByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootSmithArgs = {
  distinctOn?: InputMaybe<Array<SmithSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<SmithOrderBy>>;
  where?: InputMaybe<SmithBoolExp>;
};


export type Query_RootSmithAggregateArgs = {
  distinctOn?: InputMaybe<Array<SmithSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<SmithOrderBy>>;
  where?: InputMaybe<SmithBoolExp>;
};


export type Query_RootSmithByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootSmithCertArgs = {
  distinctOn?: InputMaybe<Array<SmithCertSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<SmithCertOrderBy>>;
  where?: InputMaybe<SmithCertBoolExp>;
};


export type Query_RootSmithCertAggregateArgs = {
  distinctOn?: InputMaybe<Array<SmithCertSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<SmithCertOrderBy>>;
  where?: InputMaybe<SmithCertBoolExp>;
};


export type Query_RootSmithCertByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootSmithEventArgs = {
  distinctOn?: InputMaybe<Array<SmithEventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<SmithEventOrderBy>>;
  where?: InputMaybe<SmithEventBoolExp>;
};


export type Query_RootSmithEventAggregateArgs = {
  distinctOn?: InputMaybe<Array<SmithEventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<SmithEventOrderBy>>;
  where?: InputMaybe<SmithEventBoolExp>;
};


export type Query_RootSmithEventByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootTransferArgs = {
  distinctOn?: InputMaybe<Array<TransferSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<TransferOrderBy>>;
  where?: InputMaybe<TransferBoolExp>;
};


export type Query_RootTransferAggregateArgs = {
  distinctOn?: InputMaybe<Array<TransferSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<TransferOrderBy>>;
  where?: InputMaybe<TransferBoolExp>;
};


export type Query_RootTransferByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootTxCommentArgs = {
  distinctOn?: InputMaybe<Array<TxCommentSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<TxCommentOrderBy>>;
  where?: InputMaybe<TxCommentBoolExp>;
};


export type Query_RootTxCommentAggregateArgs = {
  distinctOn?: InputMaybe<Array<TxCommentSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<TxCommentOrderBy>>;
  where?: InputMaybe<TxCommentBoolExp>;
};


export type Query_RootTxCommentByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootUdHistoryArgs = {
  distinctOn?: InputMaybe<Array<UdHistorySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UdHistoryOrderBy>>;
  where?: InputMaybe<UdHistoryBoolExp>;
};


export type Query_RootUdHistoryAggregateArgs = {
  distinctOn?: InputMaybe<Array<UdHistorySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UdHistoryOrderBy>>;
  where?: InputMaybe<UdHistoryBoolExp>;
};


export type Query_RootUdHistoryByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootUdReevalArgs = {
  distinctOn?: InputMaybe<Array<UdReevalSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UdReevalOrderBy>>;
  where?: InputMaybe<UdReevalBoolExp>;
};


export type Query_RootUdReevalAggregateArgs = {
  distinctOn?: InputMaybe<Array<UdReevalSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UdReevalOrderBy>>;
  where?: InputMaybe<UdReevalBoolExp>;
};


export type Query_RootUdReevalByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootUniversalDividendArgs = {
  distinctOn?: InputMaybe<Array<UniversalDividendSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UniversalDividendOrderBy>>;
  where?: InputMaybe<UniversalDividendBoolExp>;
};


export type Query_RootUniversalDividendAggregateArgs = {
  distinctOn?: InputMaybe<Array<UniversalDividendSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UniversalDividendOrderBy>>;
  where?: InputMaybe<UniversalDividendBoolExp>;
};


export type Query_RootUniversalDividendByPkArgs = {
  id: Scalars['String']['input'];
};


export type Query_RootValidatorArgs = {
  distinctOn?: InputMaybe<Array<ValidatorSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ValidatorOrderBy>>;
  where?: InputMaybe<ValidatorBoolExp>;
};


export type Query_RootValidatorAggregateArgs = {
  distinctOn?: InputMaybe<Array<ValidatorSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ValidatorOrderBy>>;
  where?: InputMaybe<ValidatorBoolExp>;
};


export type Query_RootValidatorByPkArgs = {
  id: Scalars['String']['input'];
};

export type SmithCertAggregateBoolExpCount = {
  arguments?: InputMaybe<Array<SmithCertSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<SmithCertBoolExp>;
  predicate: IntComparisonExp;
};

export type SmithEventAggregateBoolExpCount = {
  arguments?: InputMaybe<Array<SmithEventSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<SmithEventBoolExp>;
  predicate: IntComparisonExp;
};

export type Subscription_Root = {
  __typename?: 'subscription_root';
  /** fetch data from the table: "account" */
  account: Array<Account>;
  /** fetch aggregated fields from the table: "account" */
  accountAggregate: AccountAggregate;
  /** fetch data from the table: "account" using primary key columns */
  accountByPk?: Maybe<Account>;
  /** fetch data from the table in a streaming manner: "account" */
  accountStream: Array<Account>;
  /** fetch data from the table: "block" */
  block: Array<Block>;
  /** fetch aggregated fields from the table: "block" */
  blockAggregate: BlockAggregate;
  /** fetch data from the table: "block" using primary key columns */
  blockByPk?: Maybe<Block>;
  /** fetch data from the table in a streaming manner: "block" */
  blockStream: Array<Block>;
  /** fetch data from the table: "call" */
  call: Array<Call>;
  /** fetch aggregated fields from the table: "call" */
  callAggregate: CallAggregate;
  /** fetch data from the table: "call" using primary key columns */
  callByPk?: Maybe<Call>;
  /** fetch data from the table in a streaming manner: "call" */
  callStream: Array<Call>;
  /** fetch data from the table: "cert" */
  cert: Array<Cert>;
  /** fetch aggregated fields from the table: "cert" */
  certAggregate: CertAggregate;
  /** fetch data from the table: "cert" using primary key columns */
  certByPk?: Maybe<Cert>;
  /** fetch data from the table: "cert_event" */
  certEvent: Array<CertEvent>;
  /** fetch aggregated fields from the table: "cert_event" */
  certEventAggregate: CertEventAggregate;
  /** fetch data from the table: "cert_event" using primary key columns */
  certEventByPk?: Maybe<CertEvent>;
  /** fetch data from the table in a streaming manner: "cert_event" */
  certEventStream: Array<CertEvent>;
  /** fetch data from the table in a streaming manner: "cert" */
  certStream: Array<Cert>;
  /** fetch data from the table: "change_owner_key" */
  changeOwnerKey: Array<ChangeOwnerKey>;
  /** fetch aggregated fields from the table: "change_owner_key" */
  changeOwnerKeyAggregate: ChangeOwnerKeyAggregate;
  /** fetch data from the table: "change_owner_key" using primary key columns */
  changeOwnerKeyByPk?: Maybe<ChangeOwnerKey>;
  /** fetch data from the table in a streaming manner: "change_owner_key" */
  changeOwnerKeyStream: Array<ChangeOwnerKey>;
  /** fetch data from the table: "event" */
  event: Array<Event>;
  /** fetch aggregated fields from the table: "event" */
  eventAggregate: EventAggregate;
  /** fetch data from the table: "event" using primary key columns */
  eventByPk?: Maybe<Event>;
  /** fetch data from the table in a streaming manner: "event" */
  eventStream: Array<Event>;
  /** fetch data from the table: "extrinsic" */
  extrinsic: Array<Extrinsic>;
  /** fetch aggregated fields from the table: "extrinsic" */
  extrinsicAggregate: ExtrinsicAggregate;
  /** fetch data from the table: "extrinsic" using primary key columns */
  extrinsicByPk?: Maybe<Extrinsic>;
  /** fetch data from the table in a streaming manner: "extrinsic" */
  extrinsicStream: Array<Extrinsic>;
  /** execute function "get_ud_history" which returns "ud_history" */
  getUdHistory: Array<UdHistory>;
  /** execute function "get_ud_history" and query aggregates on result of table type "ud_history" */
  getUdHistoryAggregate: UdHistoryAggregate;
  /** fetch data from the table: "identity" */
  identity: Array<Identity>;
  /** fetch aggregated fields from the table: "identity" */
  identityAggregate: IdentityAggregate;
  /** fetch data from the table: "identity" using primary key columns */
  identityByPk?: Maybe<Identity>;
  /** fetch data from the table in a streaming manner: "identity" */
  identityStream: Array<Identity>;
  /** fetch data from the table: "items_counter" */
  itemsCounter: Array<ItemsCounter>;
  /** fetch aggregated fields from the table: "items_counter" */
  itemsCounterAggregate: ItemsCounterAggregate;
  /** fetch data from the table: "items_counter" using primary key columns */
  itemsCounterByPk?: Maybe<ItemsCounter>;
  /** fetch data from the table in a streaming manner: "items_counter" */
  itemsCounterStream: Array<ItemsCounter>;
  /** fetch data from the table: "membership_event" */
  membershipEvent: Array<MembershipEvent>;
  /** fetch aggregated fields from the table: "membership_event" */
  membershipEventAggregate: MembershipEventAggregate;
  /** fetch data from the table: "membership_event" using primary key columns */
  membershipEventByPk?: Maybe<MembershipEvent>;
  /** fetch data from the table in a streaming manner: "membership_event" */
  membershipEventStream: Array<MembershipEvent>;
  /** fetch data from the table: "population_history" */
  populationHistory: Array<PopulationHistory>;
  /** fetch aggregated fields from the table: "population_history" */
  populationHistoryAggregate: PopulationHistoryAggregate;
  /** fetch data from the table: "population_history" using primary key columns */
  populationHistoryByPk?: Maybe<PopulationHistory>;
  /** fetch data from the table in a streaming manner: "population_history" */
  populationHistoryStream: Array<PopulationHistory>;
  /** fetch data from the table: "smith" */
  smith: Array<Smith>;
  /** fetch aggregated fields from the table: "smith" */
  smithAggregate: SmithAggregate;
  /** fetch data from the table: "smith" using primary key columns */
  smithByPk?: Maybe<Smith>;
  /** fetch data from the table: "smith_cert" */
  smithCert: Array<SmithCert>;
  /** fetch aggregated fields from the table: "smith_cert" */
  smithCertAggregate: SmithCertAggregate;
  /** fetch data from the table: "smith_cert" using primary key columns */
  smithCertByPk?: Maybe<SmithCert>;
  /** fetch data from the table in a streaming manner: "smith_cert" */
  smithCertStream: Array<SmithCert>;
  /** fetch data from the table: "smith_event" */
  smithEvent: Array<SmithEvent>;
  /** fetch aggregated fields from the table: "smith_event" */
  smithEventAggregate: SmithEventAggregate;
  /** fetch data from the table: "smith_event" using primary key columns */
  smithEventByPk?: Maybe<SmithEvent>;
  /** fetch data from the table in a streaming manner: "smith_event" */
  smithEventStream: Array<SmithEvent>;
  /** fetch data from the table in a streaming manner: "smith" */
  smithStream: Array<Smith>;
  /** fetch data from the table: "transfer" */
  transfer: Array<Transfer>;
  /** fetch aggregated fields from the table: "transfer" */
  transferAggregate: TransferAggregate;
  /** fetch data from the table: "transfer" using primary key columns */
  transferByPk?: Maybe<Transfer>;
  /** fetch data from the table in a streaming manner: "transfer" */
  transferStream: Array<Transfer>;
  /** fetch data from the table: "tx_comment" */
  txComment: Array<TxComment>;
  /** fetch aggregated fields from the table: "tx_comment" */
  txCommentAggregate: TxCommentAggregate;
  /** fetch data from the table: "tx_comment" using primary key columns */
  txCommentByPk?: Maybe<TxComment>;
  /** fetch data from the table in a streaming manner: "tx_comment" */
  txCommentStream: Array<TxComment>;
  /** fetch data from the table: "ud_history" */
  udHistory: Array<UdHistory>;
  /** fetch aggregated fields from the table: "ud_history" */
  udHistoryAggregate: UdHistoryAggregate;
  /** fetch data from the table: "ud_history" using primary key columns */
  udHistoryByPk?: Maybe<UdHistory>;
  /** fetch data from the table in a streaming manner: "ud_history" */
  udHistoryStream: Array<UdHistory>;
  /** fetch data from the table: "ud_reeval" */
  udReeval: Array<UdReeval>;
  /** fetch aggregated fields from the table: "ud_reeval" */
  udReevalAggregate: UdReevalAggregate;
  /** fetch data from the table: "ud_reeval" using primary key columns */
  udReevalByPk?: Maybe<UdReeval>;
  /** fetch data from the table in a streaming manner: "ud_reeval" */
  udReevalStream: Array<UdReeval>;
  /** fetch data from the table: "universal_dividend" */
  universalDividend: Array<UniversalDividend>;
  /** fetch aggregated fields from the table: "universal_dividend" */
  universalDividendAggregate: UniversalDividendAggregate;
  /** fetch data from the table: "universal_dividend" using primary key columns */
  universalDividendByPk?: Maybe<UniversalDividend>;
  /** fetch data from the table in a streaming manner: "universal_dividend" */
  universalDividendStream: Array<UniversalDividend>;
  /** fetch data from the table: "validator" */
  validator: Array<Validator>;
  /** fetch aggregated fields from the table: "validator" */
  validatorAggregate: ValidatorAggregate;
  /** fetch data from the table: "validator" using primary key columns */
  validatorByPk?: Maybe<Validator>;
  /** fetch data from the table in a streaming manner: "validator" */
  validatorStream: Array<Validator>;
};


export type Subscription_RootAccountArgs = {
  distinctOn?: InputMaybe<Array<AccountSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<AccountOrderBy>>;
  where?: InputMaybe<AccountBoolExp>;
};


export type Subscription_RootAccountAggregateArgs = {
  distinctOn?: InputMaybe<Array<AccountSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<AccountOrderBy>>;
  where?: InputMaybe<AccountBoolExp>;
};


export type Subscription_RootAccountByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootAccountStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<AccountStreamCursorInput>>;
  where?: InputMaybe<AccountBoolExp>;
};


export type Subscription_RootBlockArgs = {
  distinctOn?: InputMaybe<Array<BlockSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<BlockOrderBy>>;
  where?: InputMaybe<BlockBoolExp>;
};


export type Subscription_RootBlockAggregateArgs = {
  distinctOn?: InputMaybe<Array<BlockSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<BlockOrderBy>>;
  where?: InputMaybe<BlockBoolExp>;
};


export type Subscription_RootBlockByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootBlockStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<BlockStreamCursorInput>>;
  where?: InputMaybe<BlockBoolExp>;
};


export type Subscription_RootCallArgs = {
  distinctOn?: InputMaybe<Array<CallSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CallOrderBy>>;
  where?: InputMaybe<CallBoolExp>;
};


export type Subscription_RootCallAggregateArgs = {
  distinctOn?: InputMaybe<Array<CallSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CallOrderBy>>;
  where?: InputMaybe<CallBoolExp>;
};


export type Subscription_RootCallByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootCallStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<CallStreamCursorInput>>;
  where?: InputMaybe<CallBoolExp>;
};


export type Subscription_RootCertArgs = {
  distinctOn?: InputMaybe<Array<CertSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CertOrderBy>>;
  where?: InputMaybe<CertBoolExp>;
};


export type Subscription_RootCertAggregateArgs = {
  distinctOn?: InputMaybe<Array<CertSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CertOrderBy>>;
  where?: InputMaybe<CertBoolExp>;
};


export type Subscription_RootCertByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootCertEventArgs = {
  distinctOn?: InputMaybe<Array<CertEventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CertEventOrderBy>>;
  where?: InputMaybe<CertEventBoolExp>;
};


export type Subscription_RootCertEventAggregateArgs = {
  distinctOn?: InputMaybe<Array<CertEventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CertEventOrderBy>>;
  where?: InputMaybe<CertEventBoolExp>;
};


export type Subscription_RootCertEventByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootCertEventStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<CertEventStreamCursorInput>>;
  where?: InputMaybe<CertEventBoolExp>;
};


export type Subscription_RootCertStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<CertStreamCursorInput>>;
  where?: InputMaybe<CertBoolExp>;
};


export type Subscription_RootChangeOwnerKeyArgs = {
  distinctOn?: InputMaybe<Array<ChangeOwnerKeySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ChangeOwnerKeyOrderBy>>;
  where?: InputMaybe<ChangeOwnerKeyBoolExp>;
};


export type Subscription_RootChangeOwnerKeyAggregateArgs = {
  distinctOn?: InputMaybe<Array<ChangeOwnerKeySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ChangeOwnerKeyOrderBy>>;
  where?: InputMaybe<ChangeOwnerKeyBoolExp>;
};


export type Subscription_RootChangeOwnerKeyByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootChangeOwnerKeyStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<ChangeOwnerKeyStreamCursorInput>>;
  where?: InputMaybe<ChangeOwnerKeyBoolExp>;
};


export type Subscription_RootEventArgs = {
  distinctOn?: InputMaybe<Array<EventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<EventOrderBy>>;
  where?: InputMaybe<EventBoolExp>;
};


export type Subscription_RootEventAggregateArgs = {
  distinctOn?: InputMaybe<Array<EventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<EventOrderBy>>;
  where?: InputMaybe<EventBoolExp>;
};


export type Subscription_RootEventByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootEventStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<EventStreamCursorInput>>;
  where?: InputMaybe<EventBoolExp>;
};


export type Subscription_RootExtrinsicArgs = {
  distinctOn?: InputMaybe<Array<ExtrinsicSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ExtrinsicOrderBy>>;
  where?: InputMaybe<ExtrinsicBoolExp>;
};


export type Subscription_RootExtrinsicAggregateArgs = {
  distinctOn?: InputMaybe<Array<ExtrinsicSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ExtrinsicOrderBy>>;
  where?: InputMaybe<ExtrinsicBoolExp>;
};


export type Subscription_RootExtrinsicByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootExtrinsicStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<ExtrinsicStreamCursorInput>>;
  where?: InputMaybe<ExtrinsicBoolExp>;
};


export type Subscription_RootGetUdHistoryArgs = {
  args: GetUdHistoryArgs;
  distinctOn?: InputMaybe<Array<UdHistorySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UdHistoryOrderBy>>;
  where?: InputMaybe<UdHistoryBoolExp>;
};


export type Subscription_RootGetUdHistoryAggregateArgs = {
  args: GetUdHistoryArgs;
  distinctOn?: InputMaybe<Array<UdHistorySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UdHistoryOrderBy>>;
  where?: InputMaybe<UdHistoryBoolExp>;
};


export type Subscription_RootIdentityArgs = {
  distinctOn?: InputMaybe<Array<IdentitySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<IdentityOrderBy>>;
  where?: InputMaybe<IdentityBoolExp>;
};


export type Subscription_RootIdentityAggregateArgs = {
  distinctOn?: InputMaybe<Array<IdentitySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<IdentityOrderBy>>;
  where?: InputMaybe<IdentityBoolExp>;
};


export type Subscription_RootIdentityByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootIdentityStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<IdentityStreamCursorInput>>;
  where?: InputMaybe<IdentityBoolExp>;
};


export type Subscription_RootItemsCounterArgs = {
  distinctOn?: InputMaybe<Array<ItemsCounterSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ItemsCounterOrderBy>>;
  where?: InputMaybe<ItemsCounterBoolExp>;
};


export type Subscription_RootItemsCounterAggregateArgs = {
  distinctOn?: InputMaybe<Array<ItemsCounterSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ItemsCounterOrderBy>>;
  where?: InputMaybe<ItemsCounterBoolExp>;
};


export type Subscription_RootItemsCounterByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootItemsCounterStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<ItemsCounterStreamCursorInput>>;
  where?: InputMaybe<ItemsCounterBoolExp>;
};


export type Subscription_RootMembershipEventArgs = {
  distinctOn?: InputMaybe<Array<MembershipEventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<MembershipEventOrderBy>>;
  where?: InputMaybe<MembershipEventBoolExp>;
};


export type Subscription_RootMembershipEventAggregateArgs = {
  distinctOn?: InputMaybe<Array<MembershipEventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<MembershipEventOrderBy>>;
  where?: InputMaybe<MembershipEventBoolExp>;
};


export type Subscription_RootMembershipEventByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootMembershipEventStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<MembershipEventStreamCursorInput>>;
  where?: InputMaybe<MembershipEventBoolExp>;
};


export type Subscription_RootPopulationHistoryArgs = {
  distinctOn?: InputMaybe<Array<PopulationHistorySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<PopulationHistoryOrderBy>>;
  where?: InputMaybe<PopulationHistoryBoolExp>;
};


export type Subscription_RootPopulationHistoryAggregateArgs = {
  distinctOn?: InputMaybe<Array<PopulationHistorySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<PopulationHistoryOrderBy>>;
  where?: InputMaybe<PopulationHistoryBoolExp>;
};


export type Subscription_RootPopulationHistoryByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootPopulationHistoryStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<PopulationHistoryStreamCursorInput>>;
  where?: InputMaybe<PopulationHistoryBoolExp>;
};


export type Subscription_RootSmithArgs = {
  distinctOn?: InputMaybe<Array<SmithSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<SmithOrderBy>>;
  where?: InputMaybe<SmithBoolExp>;
};


export type Subscription_RootSmithAggregateArgs = {
  distinctOn?: InputMaybe<Array<SmithSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<SmithOrderBy>>;
  where?: InputMaybe<SmithBoolExp>;
};


export type Subscription_RootSmithByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootSmithCertArgs = {
  distinctOn?: InputMaybe<Array<SmithCertSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<SmithCertOrderBy>>;
  where?: InputMaybe<SmithCertBoolExp>;
};


export type Subscription_RootSmithCertAggregateArgs = {
  distinctOn?: InputMaybe<Array<SmithCertSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<SmithCertOrderBy>>;
  where?: InputMaybe<SmithCertBoolExp>;
};


export type Subscription_RootSmithCertByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootSmithCertStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<SmithCertStreamCursorInput>>;
  where?: InputMaybe<SmithCertBoolExp>;
};


export type Subscription_RootSmithEventArgs = {
  distinctOn?: InputMaybe<Array<SmithEventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<SmithEventOrderBy>>;
  where?: InputMaybe<SmithEventBoolExp>;
};


export type Subscription_RootSmithEventAggregateArgs = {
  distinctOn?: InputMaybe<Array<SmithEventSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<SmithEventOrderBy>>;
  where?: InputMaybe<SmithEventBoolExp>;
};


export type Subscription_RootSmithEventByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootSmithEventStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<SmithEventStreamCursorInput>>;
  where?: InputMaybe<SmithEventBoolExp>;
};


export type Subscription_RootSmithStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<SmithStreamCursorInput>>;
  where?: InputMaybe<SmithBoolExp>;
};


export type Subscription_RootTransferArgs = {
  distinctOn?: InputMaybe<Array<TransferSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<TransferOrderBy>>;
  where?: InputMaybe<TransferBoolExp>;
};


export type Subscription_RootTransferAggregateArgs = {
  distinctOn?: InputMaybe<Array<TransferSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<TransferOrderBy>>;
  where?: InputMaybe<TransferBoolExp>;
};


export type Subscription_RootTransferByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootTransferStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<TransferStreamCursorInput>>;
  where?: InputMaybe<TransferBoolExp>;
};


export type Subscription_RootTxCommentArgs = {
  distinctOn?: InputMaybe<Array<TxCommentSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<TxCommentOrderBy>>;
  where?: InputMaybe<TxCommentBoolExp>;
};


export type Subscription_RootTxCommentAggregateArgs = {
  distinctOn?: InputMaybe<Array<TxCommentSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<TxCommentOrderBy>>;
  where?: InputMaybe<TxCommentBoolExp>;
};


export type Subscription_RootTxCommentByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootTxCommentStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<TxCommentStreamCursorInput>>;
  where?: InputMaybe<TxCommentBoolExp>;
};


export type Subscription_RootUdHistoryArgs = {
  distinctOn?: InputMaybe<Array<UdHistorySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UdHistoryOrderBy>>;
  where?: InputMaybe<UdHistoryBoolExp>;
};


export type Subscription_RootUdHistoryAggregateArgs = {
  distinctOn?: InputMaybe<Array<UdHistorySelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UdHistoryOrderBy>>;
  where?: InputMaybe<UdHistoryBoolExp>;
};


export type Subscription_RootUdHistoryByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootUdHistoryStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<UdHistoryStreamCursorInput>>;
  where?: InputMaybe<UdHistoryBoolExp>;
};


export type Subscription_RootUdReevalArgs = {
  distinctOn?: InputMaybe<Array<UdReevalSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UdReevalOrderBy>>;
  where?: InputMaybe<UdReevalBoolExp>;
};


export type Subscription_RootUdReevalAggregateArgs = {
  distinctOn?: InputMaybe<Array<UdReevalSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UdReevalOrderBy>>;
  where?: InputMaybe<UdReevalBoolExp>;
};


export type Subscription_RootUdReevalByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootUdReevalStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<UdReevalStreamCursorInput>>;
  where?: InputMaybe<UdReevalBoolExp>;
};


export type Subscription_RootUniversalDividendArgs = {
  distinctOn?: InputMaybe<Array<UniversalDividendSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UniversalDividendOrderBy>>;
  where?: InputMaybe<UniversalDividendBoolExp>;
};


export type Subscription_RootUniversalDividendAggregateArgs = {
  distinctOn?: InputMaybe<Array<UniversalDividendSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UniversalDividendOrderBy>>;
  where?: InputMaybe<UniversalDividendBoolExp>;
};


export type Subscription_RootUniversalDividendByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootUniversalDividendStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<UniversalDividendStreamCursorInput>>;
  where?: InputMaybe<UniversalDividendBoolExp>;
};


export type Subscription_RootValidatorArgs = {
  distinctOn?: InputMaybe<Array<ValidatorSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ValidatorOrderBy>>;
  where?: InputMaybe<ValidatorBoolExp>;
};


export type Subscription_RootValidatorAggregateArgs = {
  distinctOn?: InputMaybe<Array<ValidatorSelectColumn>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ValidatorOrderBy>>;
  where?: InputMaybe<ValidatorBoolExp>;
};


export type Subscription_RootValidatorByPkArgs = {
  id: Scalars['String']['input'];
};


export type Subscription_RootValidatorStreamArgs = {
  batchSize: Scalars['Int']['input'];
  cursor: Array<InputMaybe<ValidatorStreamCursorInput>>;
  where?: InputMaybe<ValidatorBoolExp>;
};

export type TransferAggregateBoolExpCount = {
  arguments?: InputMaybe<Array<TransferSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<TransferBoolExp>;
  predicate: IntComparisonExp;
};

export type TxCommentAggregateBoolExpCount = {
  arguments?: InputMaybe<Array<TxCommentSelectColumn>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
  filter?: InputMaybe<TxCommentBoolExp>;
  predicate: IntComparisonExp;
};

export type LatestBlockSubscriptionVariables = Exact<{ [key: string]: never; }>;


export type LatestBlockSubscription = { __typename?: 'subscription_root', block: Array<{ __typename?: 'Block', id: string, height: number }> };

export type FirstBlockQueryVariables = Exact<{ [key: string]: never; }>;


export type FirstBlockQuery = { __typename?: 'query_root', block: Array<{ __typename?: 'Block', id: string, height: number, hash: any }> };

export type GenesisHashQueryVariables = Exact<{ [key: string]: never; }>;


export type GenesisHashQuery = { __typename?: 'query_root', block: Array<{ __typename?: 'Block', id: string, hash: any }> };

export type SmithCandidatesQueryVariables = Exact<{ [key: string]: never; }>;


export type SmithCandidatesQuery = { __typename?: 'query_root', smith: Array<{ __typename?: 'Smith', id: string, index: number, smithStatus?: SmithStatusEnum | null, lastForged?: number | null, forged: number, identity?: { __typename?: 'Identity', name: string, expireOn: number } | null }> };

export type SmithDetailsQueryVariables = Exact<{
  name: Scalars['String']['input'];
}>;


export type SmithDetailsQuery = { __typename?: 'query_root', identity: Array<{ __typename?: 'Identity', id: string, index: number, name: string, accountId?: string | null, status?: IdentityStatusEnum | null, smith?: { __typename?: 'Smith', smithStatus?: SmithStatusEnum | null, lastForged?: number | null, forged: number, smithCertIssued: Array<{ __typename?: 'SmithCert', id: string, receiver?: { __typename?: 'Smith', index: number, identity?: { __typename?: 'Identity', name: string } | null } | null }>, smithCertReceived: Array<{ __typename?: 'SmithCert', id: string, issuer?: { __typename?: 'Smith', index: number, identity?: { __typename?: 'Identity', name: string } | null } | null }> } | null }> };

export type TransfersQueryVariables = Exact<{
  pubkey: Scalars['String']['input'];
  limitT: Scalars['Int']['input'];
  limitU: Scalars['Int']['input'];
  limitB: Scalars['Int']['input'];
}>;


export type TransfersQuery = { __typename?: 'query_root', accountByPk?: { __typename?: 'Account', id: string, transfersIssued: Array<{ __typename?: 'Transfer', id: string, toId?: string | null, amount: any, timestamp: any, blockNumber: number, comment?: { __typename?: 'TxComment', id: string, remark: string, type?: CommentTypeEnum | null } | null }>, transfersReceived: Array<{ __typename?: 'Transfer', id: string, fromId?: string | null, amount: any, timestamp: any, blockNumber: number, comment?: { __typename?: 'TxComment', id: string, remark: string, type?: CommentTypeEnum | null } | null }>, identity?: { __typename?: 'Identity', id: string, name: string, status?: IdentityStatusEnum | null, udHistory?: Array<{ __typename?: 'UdHistory', id: string, timestamp: any, amount: any, blockNumber: number }> | null } | null, linkedIdentity?: { __typename?: 'Identity', id: string, name: string } | null, wasIdentity: Array<{ __typename?: 'ChangeOwnerKey', id: string, nextId?: string | null, identity?: { __typename?: 'Identity', name: string } | null }> } | null };

export type IdtyNameQueryVariables = Exact<{
  id: Scalars['Int']['input'];
}>;


export type IdtyNameQuery = { __typename?: 'query_root', identity: Array<{ __typename?: 'Identity', id: string, name: string, index: number, status?: IdentityStatusEnum | null, smith?: { __typename?: 'Smith', smithStatus?: SmithStatusEnum | null } | null }> };

export type IdtyNamesQueryVariables = Exact<{
  index?: InputMaybe<Array<Scalars['Int']['input']> | Scalars['Int']['input']>;
}>;


export type IdtyNamesQuery = { __typename?: 'query_root', identity: Array<{ __typename?: 'Identity', id: string, index: number, name: string, accountId?: string | null }> };

export type IdtySearchQueryVariables = Exact<{
  txt: Scalars['String']['input'];
  limit: Scalars['Int']['input'];
  status?: InputMaybe<Array<IdentityStatusEnum> | IdentityStatusEnum>;
}>;


export type IdtySearchQuery = { __typename?: 'query_root', identity: Array<{ __typename?: 'Identity', id: string, name: string, index: number }> };

export type IdtyGetQueryVariables = Exact<{
  name: Scalars['String']['input'];
}>;


export type IdtyGetQuery = { __typename?: 'query_root', identity: Array<{ __typename?: 'Identity', id: string }> };

export type IdtyDetailsQueryVariables = Exact<{
  name: Scalars['String']['input'];
}>;


export type IdtyDetailsQuery = { __typename?: 'query_root', identity: Array<{ __typename?: 'Identity', id: string, index: number, name: string, accountId?: string | null, accountRemovedId?: string | null, status?: IdentityStatusEnum | null, createdOn: number, lastChangeOn: number, expireOn: number, smith?: { __typename?: 'Smith', smithStatus?: SmithStatusEnum | null } | null, certIssued: Array<{ __typename?: 'Cert', id: string, isActive: boolean, updatedOn: number, expireOn: number, receiver?: { __typename?: 'Identity', name: string, status?: IdentityStatusEnum | null } | null }>, certReceived: Array<{ __typename?: 'Cert', id: string, isActive: boolean, updatedOn: number, expireOn: number, issuer?: { __typename?: 'Identity', name: string, status?: IdentityStatusEnum | null } | null }>, ownerKeyChange: Array<{ __typename?: 'ChangeOwnerKey', id: string, previousId?: string | null }>, membershipHistory: Array<{ __typename?: 'MembershipEvent', blockNumber: number, eventType?: EventTypeEnum | null }> }> };

export type CertDetailsQueryVariables = Exact<{
  issuer: Scalars['String']['input'];
  receiver: Scalars['String']['input'];
}>;


export type CertDetailsQuery = { __typename?: 'query_root', cert: Array<{ __typename?: 'Cert', id: string, isActive: boolean, createdOn: number, updatedOn: number, expireOn: number, certHistory: Array<{ __typename?: 'CertEvent', blockNumber: number, eventType?: EventTypeEnum | null }>, issuer?: { __typename?: 'Identity', index: number, status?: IdentityStatusEnum | null } | null, receiver?: { __typename?: 'Identity', index: number, status?: IdentityStatusEnum | null } | null }> };
