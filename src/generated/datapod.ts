export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  jsonb: { input: any; output: any; }
  point: { input: any; output: any; }
  timestamp: { input: any; output: any; }
};

export type AddTransactionResponse = {
  __typename?: 'AddTransactionResponse';
  message: Scalars['String']['output'];
  success: Scalars['Boolean']['output'];
};

export type DeleteProfileResponse = {
  __typename?: 'DeleteProfileResponse';
  message: Scalars['String']['output'];
  success: Scalars['Boolean']['output'];
};

export type GeolocInput = {
  latitude: Scalars['Float']['input'];
  longitude: Scalars['Float']['input'];
};

export type MigrateProfileResponse = {
  __typename?: 'MigrateProfileResponse';
  message: Scalars['String']['output'];
  success: Scalars['Boolean']['output'];
};

export type SocialInput = {
  type?: InputMaybe<Scalars['String']['input']>;
  url: Scalars['String']['input'];
};

/** Boolean expression to compare columns of type "String". All fields are combined with logical 'AND'. */
export type String_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['String']['input']>;
  _gt?: InputMaybe<Scalars['String']['input']>;
  _gte?: InputMaybe<Scalars['String']['input']>;
  /** does the column match the given case-insensitive pattern */
  _ilike?: InputMaybe<Scalars['String']['input']>;
  _in?: InputMaybe<Array<Scalars['String']['input']>>;
  /** does the column match the given POSIX regular expression, case insensitive */
  _iregex?: InputMaybe<Scalars['String']['input']>;
  _is_null?: InputMaybe<Scalars['Boolean']['input']>;
  /** does the column match the given pattern */
  _like?: InputMaybe<Scalars['String']['input']>;
  _lt?: InputMaybe<Scalars['String']['input']>;
  _lte?: InputMaybe<Scalars['String']['input']>;
  _neq?: InputMaybe<Scalars['String']['input']>;
  /** does the column NOT match the given case-insensitive pattern */
  _nilike?: InputMaybe<Scalars['String']['input']>;
  _nin?: InputMaybe<Array<Scalars['String']['input']>>;
  /** does the column NOT match the given POSIX regular expression, case insensitive */
  _niregex?: InputMaybe<Scalars['String']['input']>;
  /** does the column NOT match the given pattern */
  _nlike?: InputMaybe<Scalars['String']['input']>;
  /** does the column NOT match the given POSIX regular expression, case sensitive */
  _nregex?: InputMaybe<Scalars['String']['input']>;
  /** does the column NOT match the given SQL regular expression */
  _nsimilar?: InputMaybe<Scalars['String']['input']>;
  /** does the column match the given POSIX regular expression, case sensitive */
  _regex?: InputMaybe<Scalars['String']['input']>;
  /** does the column match the given SQL regular expression */
  _similar?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateProfileResponse = {
  __typename?: 'UpdateProfileResponse';
  message: Scalars['String']['output'];
  success: Scalars['Boolean']['output'];
};

/** ordering argument of a cursor */
export enum Cursor_Ordering {
  /** ascending ordering of the cursor */
  Asc = 'ASC',
  /** descending ordering of the cursor */
  Desc = 'DESC'
}

export type Jsonb_Cast_Exp = {
  String?: InputMaybe<String_Comparison_Exp>;
};

/** Boolean expression to compare columns of type "jsonb". All fields are combined with logical 'AND'. */
export type Jsonb_Comparison_Exp = {
  _cast?: InputMaybe<Jsonb_Cast_Exp>;
  /** is the column contained in the given json value */
  _contained_in?: InputMaybe<Scalars['jsonb']['input']>;
  /** does the column contain the given json value at the top level */
  _contains?: InputMaybe<Scalars['jsonb']['input']>;
  _eq?: InputMaybe<Scalars['jsonb']['input']>;
  _gt?: InputMaybe<Scalars['jsonb']['input']>;
  _gte?: InputMaybe<Scalars['jsonb']['input']>;
  /** does the string exist as a top-level key in the column */
  _has_key?: InputMaybe<Scalars['String']['input']>;
  /** do all of these strings exist as top-level keys in the column */
  _has_keys_all?: InputMaybe<Array<Scalars['String']['input']>>;
  /** do any of these strings exist as top-level keys in the column */
  _has_keys_any?: InputMaybe<Array<Scalars['String']['input']>>;
  _in?: InputMaybe<Array<Scalars['jsonb']['input']>>;
  _is_null?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['jsonb']['input']>;
  _lte?: InputMaybe<Scalars['jsonb']['input']>;
  _neq?: InputMaybe<Scalars['jsonb']['input']>;
  _nin?: InputMaybe<Array<Scalars['jsonb']['input']>>;
};

/** mutation root */
export type Mutation_Root = {
  __typename?: 'mutation_root';
  /** addTransaction */
  addTransaction?: Maybe<AddTransactionResponse>;
  /** deleteProfile */
  deleteProfile?: Maybe<DeleteProfileResponse>;
  /** migrateProfile */
  migrateProfile?: Maybe<MigrateProfileResponse>;
  /** updateProfile */
  updateProfile?: Maybe<UpdateProfileResponse>;
};


/** mutation root */
export type Mutation_RootAddTransactionArgs = {
  address: Scalars['String']['input'];
  comment: Scalars['String']['input'];
  hash: Scalars['String']['input'];
  id: Scalars['String']['input'];
  signature: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootDeleteProfileArgs = {
  address: Scalars['String']['input'];
  hash: Scalars['String']['input'];
  signature: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootMigrateProfileArgs = {
  addressNew: Scalars['String']['input'];
  addressOld: Scalars['String']['input'];
  hash: Scalars['String']['input'];
  signature: Scalars['String']['input'];
};


/** mutation root */
export type Mutation_RootUpdateProfileArgs = {
  address: Scalars['String']['input'];
  avatarBase64?: InputMaybe<Scalars['String']['input']>;
  city?: InputMaybe<Scalars['String']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  geoloc?: InputMaybe<GeolocInput>;
  hash: Scalars['String']['input'];
  signature: Scalars['String']['input'];
  socials?: InputMaybe<Array<SocialInput>>;
  title?: InputMaybe<Scalars['String']['input']>;
};

/** column ordering options */
export enum Order_By {
  /** in ascending order, nulls last */
  Asc = 'asc',
  /** in ascending order, nulls first */
  AscNullsFirst = 'asc_nulls_first',
  /** in ascending order, nulls last */
  AscNullsLast = 'asc_nulls_last',
  /** in descending order, nulls first */
  Desc = 'desc',
  /** in descending order, nulls first */
  DescNullsFirst = 'desc_nulls_first',
  /** in descending order, nulls last */
  DescNullsLast = 'desc_nulls_last'
}

/** Boolean expression to compare columns of type "point". All fields are combined with logical 'AND'. */
export type Point_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['point']['input']>;
  _gt?: InputMaybe<Scalars['point']['input']>;
  _gte?: InputMaybe<Scalars['point']['input']>;
  _in?: InputMaybe<Array<Scalars['point']['input']>>;
  _is_null?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['point']['input']>;
  _lte?: InputMaybe<Scalars['point']['input']>;
  _neq?: InputMaybe<Scalars['point']['input']>;
  _nin?: InputMaybe<Array<Scalars['point']['input']>>;
};

/** columns and relationships of "profiles" */
export type Profiles = {
  __typename?: 'profiles';
  /** cid of avatar */
  avatar?: Maybe<Scalars['String']['output']>;
  city?: Maybe<Scalars['String']['output']>;
  /** CID of the latest data from which this document comes from */
  data_cid?: Maybe<Scalars['String']['output']>;
  description?: Maybe<Scalars['String']['output']>;
  geoloc?: Maybe<Scalars['point']['output']>;
  /** CID of the latest index request that modified this document */
  index_request_cid: Scalars['String']['output'];
  /** ss58 address of profile owner */
  pubkey: Scalars['String']['output'];
  socials?: Maybe<Scalars['jsonb']['output']>;
  /** timestamp of the latest index request that modified this document */
  time: Scalars['timestamp']['output'];
  /** title of c+ profile */
  title?: Maybe<Scalars['String']['output']>;
};


/** columns and relationships of "profiles" */
export type ProfilesSocialsArgs = {
  path?: InputMaybe<Scalars['String']['input']>;
};

/** aggregated selection of "profiles" */
export type Profiles_Aggregate = {
  __typename?: 'profiles_aggregate';
  aggregate?: Maybe<Profiles_Aggregate_Fields>;
  nodes: Array<Profiles>;
};

/** aggregate fields of "profiles" */
export type Profiles_Aggregate_Fields = {
  __typename?: 'profiles_aggregate_fields';
  count: Scalars['Int']['output'];
  max?: Maybe<Profiles_Max_Fields>;
  min?: Maybe<Profiles_Min_Fields>;
};


/** aggregate fields of "profiles" */
export type Profiles_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Profiles_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Boolean expression to filter rows from the table "profiles". All fields are combined with a logical 'AND'. */
export type Profiles_Bool_Exp = {
  _and?: InputMaybe<Array<Profiles_Bool_Exp>>;
  _not?: InputMaybe<Profiles_Bool_Exp>;
  _or?: InputMaybe<Array<Profiles_Bool_Exp>>;
  avatar?: InputMaybe<String_Comparison_Exp>;
  city?: InputMaybe<String_Comparison_Exp>;
  data_cid?: InputMaybe<String_Comparison_Exp>;
  description?: InputMaybe<String_Comparison_Exp>;
  geoloc?: InputMaybe<Point_Comparison_Exp>;
  index_request_cid?: InputMaybe<String_Comparison_Exp>;
  pubkey?: InputMaybe<String_Comparison_Exp>;
  socials?: InputMaybe<Jsonb_Comparison_Exp>;
  time?: InputMaybe<Timestamp_Comparison_Exp>;
  title?: InputMaybe<String_Comparison_Exp>;
};

/** aggregate max on columns */
export type Profiles_Max_Fields = {
  __typename?: 'profiles_max_fields';
  /** cid of avatar */
  avatar?: Maybe<Scalars['String']['output']>;
  city?: Maybe<Scalars['String']['output']>;
  /** CID of the latest data from which this document comes from */
  data_cid?: Maybe<Scalars['String']['output']>;
  description?: Maybe<Scalars['String']['output']>;
  /** CID of the latest index request that modified this document */
  index_request_cid?: Maybe<Scalars['String']['output']>;
  /** ss58 address of profile owner */
  pubkey?: Maybe<Scalars['String']['output']>;
  /** timestamp of the latest index request that modified this document */
  time?: Maybe<Scalars['timestamp']['output']>;
  /** title of c+ profile */
  title?: Maybe<Scalars['String']['output']>;
};

/** aggregate min on columns */
export type Profiles_Min_Fields = {
  __typename?: 'profiles_min_fields';
  /** cid of avatar */
  avatar?: Maybe<Scalars['String']['output']>;
  city?: Maybe<Scalars['String']['output']>;
  /** CID of the latest data from which this document comes from */
  data_cid?: Maybe<Scalars['String']['output']>;
  description?: Maybe<Scalars['String']['output']>;
  /** CID of the latest index request that modified this document */
  index_request_cid?: Maybe<Scalars['String']['output']>;
  /** ss58 address of profile owner */
  pubkey?: Maybe<Scalars['String']['output']>;
  /** timestamp of the latest index request that modified this document */
  time?: Maybe<Scalars['timestamp']['output']>;
  /** title of c+ profile */
  title?: Maybe<Scalars['String']['output']>;
};

/** Ordering options when selecting data from "profiles". */
export type Profiles_Order_By = {
  avatar?: InputMaybe<Order_By>;
  city?: InputMaybe<Order_By>;
  data_cid?: InputMaybe<Order_By>;
  description?: InputMaybe<Order_By>;
  geoloc?: InputMaybe<Order_By>;
  index_request_cid?: InputMaybe<Order_By>;
  pubkey?: InputMaybe<Order_By>;
  socials?: InputMaybe<Order_By>;
  time?: InputMaybe<Order_By>;
  title?: InputMaybe<Order_By>;
};

/** select columns of table "profiles" */
export enum Profiles_Select_Column {
  /** column name */
  Avatar = 'avatar',
  /** column name */
  City = 'city',
  /** column name */
  DataCid = 'data_cid',
  /** column name */
  Description = 'description',
  /** column name */
  Geoloc = 'geoloc',
  /** column name */
  IndexRequestCid = 'index_request_cid',
  /** column name */
  Pubkey = 'pubkey',
  /** column name */
  Socials = 'socials',
  /** column name */
  Time = 'time',
  /** column name */
  Title = 'title'
}

/** Streaming cursor of the table "profiles" */
export type Profiles_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Profiles_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Profiles_Stream_Cursor_Value_Input = {
  /** cid of avatar */
  avatar?: InputMaybe<Scalars['String']['input']>;
  city?: InputMaybe<Scalars['String']['input']>;
  /** CID of the latest data from which this document comes from */
  data_cid?: InputMaybe<Scalars['String']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  geoloc?: InputMaybe<Scalars['point']['input']>;
  /** CID of the latest index request that modified this document */
  index_request_cid?: InputMaybe<Scalars['String']['input']>;
  /** ss58 address of profile owner */
  pubkey?: InputMaybe<Scalars['String']['input']>;
  socials?: InputMaybe<Scalars['jsonb']['input']>;
  /** timestamp of the latest index request that modified this document */
  time?: InputMaybe<Scalars['timestamp']['input']>;
  /** title of c+ profile */
  title?: InputMaybe<Scalars['String']['input']>;
};

export type Query_Root = {
  __typename?: 'query_root';
  /** fetch data from the table: "profiles" */
  profiles: Array<Profiles>;
  /** fetch aggregated fields from the table: "profiles" */
  profiles_aggregate: Profiles_Aggregate;
  /** fetch data from the table: "profiles" using primary key columns */
  profiles_by_pk?: Maybe<Profiles>;
  /** fetch data from the table: "user_fs" */
  user_fs: Array<User_Fs>;
  /** fetch data from the table: "user_fs" using primary key columns */
  user_fs_by_pk?: Maybe<User_Fs>;
};


export type Query_RootProfilesArgs = {
  distinct_on?: InputMaybe<Array<Profiles_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Profiles_Order_By>>;
  where?: InputMaybe<Profiles_Bool_Exp>;
};


export type Query_RootProfiles_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Profiles_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Profiles_Order_By>>;
  where?: InputMaybe<Profiles_Bool_Exp>;
};


export type Query_RootProfiles_By_PkArgs = {
  pubkey: Scalars['String']['input'];
};


export type Query_RootUser_FsArgs = {
  distinct_on?: InputMaybe<Array<User_Fs_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<User_Fs_Order_By>>;
  where?: InputMaybe<User_Fs_Bool_Exp>;
};


export type Query_RootUser_Fs_By_PkArgs = {
  pubkey: Scalars['String']['input'];
};

export type Subscription_Root = {
  __typename?: 'subscription_root';
  /** fetch data from the table: "profiles" */
  profiles: Array<Profiles>;
  /** fetch aggregated fields from the table: "profiles" */
  profiles_aggregate: Profiles_Aggregate;
  /** fetch data from the table: "profiles" using primary key columns */
  profiles_by_pk?: Maybe<Profiles>;
  /** fetch data from the table in a streaming manner: "profiles" */
  profiles_stream: Array<Profiles>;
  /** fetch data from the table: "user_fs" */
  user_fs: Array<User_Fs>;
  /** fetch data from the table: "user_fs" using primary key columns */
  user_fs_by_pk?: Maybe<User_Fs>;
  /** fetch data from the table in a streaming manner: "user_fs" */
  user_fs_stream: Array<User_Fs>;
};


export type Subscription_RootProfilesArgs = {
  distinct_on?: InputMaybe<Array<Profiles_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Profiles_Order_By>>;
  where?: InputMaybe<Profiles_Bool_Exp>;
};


export type Subscription_RootProfiles_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Profiles_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<Profiles_Order_By>>;
  where?: InputMaybe<Profiles_Bool_Exp>;
};


export type Subscription_RootProfiles_By_PkArgs = {
  pubkey: Scalars['String']['input'];
};


export type Subscription_RootProfiles_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<Profiles_Stream_Cursor_Input>>;
  where?: InputMaybe<Profiles_Bool_Exp>;
};


export type Subscription_RootUser_FsArgs = {
  distinct_on?: InputMaybe<Array<User_Fs_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  order_by?: InputMaybe<Array<User_Fs_Order_By>>;
  where?: InputMaybe<User_Fs_Bool_Exp>;
};


export type Subscription_RootUser_Fs_By_PkArgs = {
  pubkey: Scalars['String']['input'];
};


export type Subscription_RootUser_Fs_StreamArgs = {
  batch_size: Scalars['Int']['input'];
  cursor: Array<InputMaybe<User_Fs_Stream_Cursor_Input>>;
  where?: InputMaybe<User_Fs_Bool_Exp>;
};

/** Boolean expression to compare columns of type "timestamp". All fields are combined with logical 'AND'. */
export type Timestamp_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['timestamp']['input']>;
  _gt?: InputMaybe<Scalars['timestamp']['input']>;
  _gte?: InputMaybe<Scalars['timestamp']['input']>;
  _in?: InputMaybe<Array<Scalars['timestamp']['input']>>;
  _is_null?: InputMaybe<Scalars['Boolean']['input']>;
  _lt?: InputMaybe<Scalars['timestamp']['input']>;
  _lte?: InputMaybe<Scalars['timestamp']['input']>;
  _neq?: InputMaybe<Scalars['timestamp']['input']>;
  _nin?: InputMaybe<Array<Scalars['timestamp']['input']>>;
};

/** User filesystem root cid */
export type User_Fs = {
  __typename?: 'user_fs';
  /** CID of the user filesystem root */
  data_cid: Scalars['String']['output'];
  /** CID of the latest index request that modified this document */
  index_request_cid: Scalars['String']['output'];
  /** ss58 address of owner */
  pubkey: Scalars['String']['output'];
  /** timestamp of the latest index request that modified this document */
  time: Scalars['timestamp']['output'];
};

/** Boolean expression to filter rows from the table "user_fs". All fields are combined with a logical 'AND'. */
export type User_Fs_Bool_Exp = {
  _and?: InputMaybe<Array<User_Fs_Bool_Exp>>;
  _not?: InputMaybe<User_Fs_Bool_Exp>;
  _or?: InputMaybe<Array<User_Fs_Bool_Exp>>;
  data_cid?: InputMaybe<String_Comparison_Exp>;
  index_request_cid?: InputMaybe<String_Comparison_Exp>;
  pubkey?: InputMaybe<String_Comparison_Exp>;
  time?: InputMaybe<Timestamp_Comparison_Exp>;
};

/** Ordering options when selecting data from "user_fs". */
export type User_Fs_Order_By = {
  data_cid?: InputMaybe<Order_By>;
  index_request_cid?: InputMaybe<Order_By>;
  pubkey?: InputMaybe<Order_By>;
  time?: InputMaybe<Order_By>;
};

/** select columns of table "user_fs" */
export enum User_Fs_Select_Column {
  /** column name */
  DataCid = 'data_cid',
  /** column name */
  IndexRequestCid = 'index_request_cid',
  /** column name */
  Pubkey = 'pubkey',
  /** column name */
  Time = 'time'
}

/** Streaming cursor of the table "user_fs" */
export type User_Fs_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: User_Fs_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type User_Fs_Stream_Cursor_Value_Input = {
  /** CID of the user filesystem root */
  data_cid?: InputMaybe<Scalars['String']['input']>;
  /** CID of the latest index request that modified this document */
  index_request_cid?: InputMaybe<Scalars['String']['input']>;
  /** ss58 address of owner */
  pubkey?: InputMaybe<Scalars['String']['input']>;
  /** timestamp of the latest index request that modified this document */
  time?: InputMaybe<Scalars['timestamp']['input']>;
};

export type ProfileQueryVariables = Exact<{
  pubkey: Scalars['String']['input'];
}>;


export type ProfileQuery = { __typename?: 'query_root', profiles_by_pk?: { __typename?: 'profiles', index_request_cid: string, data_cid?: string | null, time: any, pubkey: string, title?: string | null, description?: string | null, avatar?: string | null, city?: string | null, geoloc?: any | null, socials?: any | null } | null };

export type ProfileLightQueryVariables = Exact<{
  pubkey: Scalars['String']['input'];
}>;


export type ProfileLightQuery = { __typename?: 'query_root', profiles_by_pk?: { __typename?: 'profiles', pubkey: string, title?: string | null } | null };

export type ProfileCountQueryVariables = Exact<{ [key: string]: never; }>;


export type ProfileCountQuery = { __typename?: 'query_root', profiles_aggregate: { __typename?: 'profiles_aggregate', aggregate?: { __typename?: 'profiles_aggregate_fields', count: number } | null } };

export type ProfileSearchQueryVariables = Exact<{
  part: Scalars['String']['input'];
}>;


export type ProfileSearchQuery = { __typename?: 'query_root', profiles: Array<{ __typename?: 'profiles', title?: string | null, pubkey: string }> };
