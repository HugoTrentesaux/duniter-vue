// MEMBER → Member

import { BN } from '@polkadot/util'
import { currentBlockHeight, tokenDecimals, tokenSymbol } from './global'
import type { u64 } from '@polkadot/types-codec'

// status coming from indexer are uppercase
export function formatStatus(txt: string) {
  return txt.charAt(0).toUpperCase() + txt.slice(1).toLowerCase()
}

export function balFmt(amount: u64 | number | BigInt): string {
  if (!tokenDecimals.value || !tokenSymbol.value) return amount.toString()
  const fmt = new Intl.NumberFormat()
  const [td, ts] = [tokenDecimals.value!, tokenSymbol.value!]
  const amountBN = new BN(amount.toString())
  const decimalDivisor = new BN(10).pow(new BN(td))
  const wholePart = amountBN.div(decimalDivisor)
  const localizedWholePart = fmt.format(wholePart.toNumber())
  const fractionalPart = amountBN.mod(decimalDivisor).toString().padEnd(2, '0')
  const decimalSeparator =
    fmt.formatToParts(1.1).find((part) => part.type === 'decimal')?.value || '.'
  const formattedAmount = `${localizedWholePart}${decimalSeparator}${fractionalPart}`
  return `${formattedAmount} ${ts}`
}

// remove first occurence of item in array
export function removeItem<T>(arr: T[], value: T) {
  const index = arr.indexOf(value)
  if (index > -1) {
    arr.splice(index, 1)
  }
  return arr
}

// display less chars of pubkey
export function shorter(txt: string) {
  return txt.slice(0, 4) + '...' + txt.slice(-4)
}

// normalize amount
export function normAmount(amount: number) {
  const a = amount * Math.pow(10, tokenDecimals.value)
  const b = Math.floor(a)
  return b
}

// estimate date of future block
export function estimateDate(future_block: number): Date {
  // number of expected milliseconds in a block
  const MILLIS_PER_BLOCK = 6_000
  // we do not actually mind if it is in the past
  const diff = future_block - currentBlockHeight.value
  // cheap estimate using current time
  return new Date(Date.now() + diff * MILLIS_PER_BLOCK)
}
