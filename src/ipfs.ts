import { createHelia } from 'helia'
import { bootstrap } from '@libp2p/bootstrap'
import { webTransport } from '@libp2p/webtransport'
import { webSockets } from '@libp2p/websockets'
import { webRTCDirect } from '@libp2p/webrtc'
import { createLibp2p } from 'libp2p'
import { gossipsub, type GossipsubEvents } from '@chainsafe/libp2p-gossipsub'
import { bitswap } from '@helia/block-brokers'
import { multiaddr, type Multiaddr } from '@multiformats/multiaddr'
import { type HeliaLibp2p } from 'helia'
import { type Libp2p, type PubSub } from '@libp2p/interface'
import { noise } from '@chainsafe/libp2p-noise'
import { yamux } from '@chainsafe/libp2p-yamux'
import { identify, type Identify } from '@libp2p/identify'
import { kadDHT, type KadDHT } from '@libp2p/kad-dht'
import { peerIdFromString } from '@libp2p/peer-id'
import type { AddrInfo } from '@chainsafe/libp2p-gossipsub/types'
import { getItem, sk } from './storage'
import { unixfs, type UnixFS } from '@helia/unixfs'
import { dagCbor, type DAGCBOR } from '@helia/dag-cbor'

export function getAllPeersFromStorage(): AddrInfo[] {
  const peers = getItem<any>(sk.datapodKnownPeers)
  return objToAddrInfo(peers)
}

export function objToAddrInfo(obj: any): AddrInfo[] {
  return Object.keys(obj).map((p) => ({
    id: peerIdFromString(p),
    addrs: obj[p].map(multiaddr)
  }))
}

export function allAddresses(): Multiaddr[] {
  return getAllPeersFromStorage()
    .map((p) => p.addrs)
    .flat()
}

export function dialpeers(p2p: DatapodIpfsStack) {
  const peers = getAllPeersFromStorage()
  for (const peer of peers) {
    p2p.libp2p.dial(peer.addrs)
  }
}

// type alias
type CustomServiceMap = {
  pubsub: PubSub<GossipsubEvents>
  identify: Identify
  aminoDHT: KadDHT
}

// create libp2p conf
async function getlibp2p(): Promise<Libp2p<CustomServiceMap>> {
  const peers = getAllPeersFromStorage()
  const addresses = peers.map((p) => p.addrs.map((pi) => pi.toString())).flat()

  return createLibp2p({
    peerDiscovery: [
      bootstrap({
        list: addresses
      })
    ],
    transports: [
      webTransport(), //
      webRTCDirect(), //
      webSockets() //
    ],
    services: {
      identify: identify(),
      pubsub: gossipsub({
        directPeers: peers
      }),
      aminoDHT: kadDHT({
        protocol: '/ipfs/kad/1.0.0'
      })
      // keychain: keychain()
      // TODO use Ğ1 key in the keychain as known peer
      // this will allow to peer with identities trusted in wot
    },
    connectionGater: {
      // prevent connection outside known peers
      denyDialMultiaddr: async (m: Multiaddr) => !addresses.includes(m.toString()),
      // denyDialMultiaddr: async () => false,
      // allow connecting to local peer
      denyOutboundConnection: async () => false
    },
    connectionManager: {
      // make sure we stay online
      // minConnections: 5
      // autoDialInterval: 1
    },
    // connectionEncryption: [noise()],
    connectionEncrypters: [noise()],
    streamMuxers: [yamux()]
  })
}

// create a full libp2p Helia node
function gethelia<T extends Libp2p>(libp2p: T): Promise<HeliaLibp2p<T>> {
  return createHelia({
    libp2p: libp2p,
    blockBrokers: [
      // trustlessGateway({ gateways: GATEWAYS }) // datapod gateways
      bitswap() // does not work at the moment in browser → TODO test it again
    ]
  })
}

export interface DatapodIpfsStack {
  libp2p: Libp2p<CustomServiceMap>
  pubsub: PubSub
  helia: HeliaLibp2p<Libp2p<CustomServiceMap>>
  heliafs: UnixFS
  heliadag: DAGCBOR
}

// get ipfs stack with helpers
export async function getIpfsStack(): Promise<DatapodIpfsStack> {
  // create libp2p conf
  const libp2p: Libp2p<CustomServiceMap> = await getlibp2p()
  /// expose pubsub
  const pubsub = libp2p.services.pubsub
  // create a Helia node
  const helia: HeliaLibp2p<Libp2p<CustomServiceMap>> = await gethelia(libp2p)

  /// expose fs interface for images for instance
  // currently only in memory
  // TODO use datastore-idb to keep things in browser indexed db
  // this would avoid re-fetching content when closing / re-opening
  const heliafs: UnixFS = unixfs(helia)

  /// expose dag cbor interface for compact object representation
  const heliadag: DAGCBOR = dagCbor(helia)

  return {
    libp2p: libp2p,
    pubsub: pubsub,
    helia: helia,
    heliafs: heliafs,
    heliadag: heliadag
  }
}
