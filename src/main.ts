import './sass/main.sass'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { initStateFromStorage, initStateFromApis } from './global'
import { web3Enable } from '@polkadot/extension-dapp'
import { initLocalStorage } from './storage'
import { initDatapod, initKuboRpc, initRpc, initSquid, initp2p } from './network'
import { ApolloClients } from '@vue/apollo-composable'
import {
  updateStorageFromConfigAsset,
  updateStorageFromConfigQueryString,
  updateStorageFromDefaultRemoteNetworkConfig
} from './config'

// initialize local storage before anything
initLocalStorage()

// app
const app = createApp(App)
app.use(router)
app.mount('#app')

// call async main
main()

// -----

// main function which initialize everything
async function main() {
  // ask browser extension for web3 support (async)
  web3Enable('duniter-vue')

  // we first wait to get config, which should be fast and infaillible
  await updateStorageFromConfigAsset()
  // also check remote network config the get changes if any
  // do not await since it's not strictly necessary
  // it would eventually be useful at next start
  // TODO allow user to customize remote network config endpoint
  updateStorageFromDefaultRemoteNetworkConfig('gdev')
  // check in URL if config file is defined
  const config = router.currentRoute.value.query.config
  if (config) {
    // if config is present in the URL we await
    await updateStorageFromConfigQueryString(config.toString())
  }

  // then we use the values to load storage
  initStateFromStorage()

  // asynchronous
  initApis()
  initP2p()
}

// initialize APIs, provide them, and use them to initialize app state
async function initApis() {
  const [duniterRpc, apolloSquid, apolloDatapod] = await Promise.all([
    initRpc(),
    initSquid(),
    initDatapod()
  ])
  // provide deps to to app
  app.provide('duniter-rpc', duniterRpc)
  app.provide(ApolloClients, { default: apolloSquid, squid: apolloSquid, datapod: apolloDatapod })
  app.provide('kubo', initKuboRpc())
  // initialize state from both api (async)
  initStateFromApis(duniterRpc, apolloSquid)
}

// provide datapod ipfs stack
async function initP2p() {
  const p2p = await initp2p()
  app.provide('p2p', p2p)
}
