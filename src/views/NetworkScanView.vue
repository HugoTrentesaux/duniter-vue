<script setup lang="ts">
import { computed, ref, type Ref } from 'vue'
import { getItem, sk } from '@/storage'
import { WsProvider, ApiPromise } from '@polkadot/api'
import {
  currentBlockHeight,
  duniterEndpoint,
  genHash,
  onSameNetwork,
  squidBlockchainInfo,
  squidCurrentBlockHeight,
  squidEndpoint,
  squidGenHash
} from '@/global'
import type { ApolloClient, FetchResult, NormalizedCacheObject } from '@apollo/client'
import { initApollo } from '@/network'
import type { LatestBlockSubscription } from '@/generated/squid'

// constants
const KNOWN_GENESIS = new Map([
  ['0xc184c4ccde8e771483bba7a01533d007a3e19a66d3537c7fd59c5d9e3550b6c3', 'gdev-800']
])
const CLOSE_ENOUGH_THRESHOLD = 2
const TOO_FAR_THRESHOLD = 10

// get endpoint list
const knownRpcs: Ref<string[]> = ref(getItem<string[]>(sk.rpcEndpoints))
const knownSquids: Ref<string[]> = ref(getItem<string[]>(sk.squidEndpoints))

// store info about endpoint
const rpcInfos: Map<string, RpcInfo> = new Map(knownRpcs.value.map((e) => [e, initRpcInfo(e)]))
const squidInfos: Map<string, SquidInfo> = new Map(
  knownSquids.value.map((e) => [e, initSquidInfo(e)])
)

// is squid close enough from duniter
const closeEnough = computed(
  () => Math.abs(currentBlockHeight.value - squidCurrentBlockHeight.value) < CLOSE_ENOUGH_THRESHOLD
)
// are they really too far
const tooFar = computed(
  () => Math.abs(currentBlockHeight.value - squidCurrentBlockHeight.value) > TOO_FAR_THRESHOLD
)

enum Status {
  Loading, // still loading
  Error, // error connecting
  Different, // different network than current
  Desync, // desynchronized
  Sync // synchronized
}

// === RPC ===
interface RpcInfo {
  api: ApiPromise | null
  status: Ref<Status>
  genesis: Ref<string | null>
  height: Ref<number | null>
  hash: Ref<string | null>
}
function initRpcInfo(e: string): RpcInfo {
  initRpcApi(e) // asynchronously
  return {
    api: null,
    status: ref(Status.Loading),
    genesis: ref(null),
    height: ref(null),
    hash: ref(null)
  }
}
async function initRpcApi(e: string) {
  let api = null
  try {
    const wsProvider = new WsProvider(e)
    api = await ApiPromise.create({ provider: wsProvider, throwOnConnect: true })
    await api.isReadyOrError
    const info = rpcInfos.get(e)
    if (info) {
      console.log('connected', e)
      info.api = api
      const gHash = api.genesisHash.toHex()
      info.genesis.value = gHash
      if (gHash == genHash.value) {
        info.status.value = Status.Desync
      } else {
        info.status.value = Status.Different
      }
      api.rpc.chain.subscribeNewHeads((header) => {
        const headHeight = header.number.toNumber()
        info.height.value = headHeight
        info.hash.value = header.hash.toString()
        if (info.status.value != Status.Different) {
          if (Math.abs(headHeight - currentBlockHeight.value) < CLOSE_ENOUGH_THRESHOLD) {
            info.status.value = Status.Sync
          } else {
            info.status.value = Status.Desync
          }
        }
      })
    }
  } catch (err) {
    console.log('failed to connect', e)
    api?.disconnect()
    const info = rpcInfos.get(e)
    if (info) {
      info.status.value = Status.Error
    }
  }
}

// === SQUID ===

interface SquidInfo {
  api: ApolloClient<NormalizedCacheObject> | null
  status: Ref<Status>
  genesis: Ref<string | null>
  height: Ref<number | null>
  hash: Ref<string | null>
}
function initSquidInfo(e: string): SquidInfo {
  initSquidApi(e) // asynchronously
  return {
    api: null,
    status: ref(Status.Loading),
    genesis: ref(null),
    height: ref(null),
    hash: ref(null)
  }
}
async function initSquidApi(e: string) {
  let apollo = null
  apollo = await initApollo(
    () => e,
    () => e.replace('http', 'ws')
  )
  const info = squidInfos.get(e)
  if (info) {
    console.log('initialized', e)
    info.api = apollo
    const gotGenesisHash = (gHash: string) => {
      info.genesis.value = gHash
      if (gHash == genHash.value) {
        info.status.value = Status.Desync
      } else {
        info.status.value = Status.Different
      }
    }
    const errHandler = (err: any) => {
      console.log('failed to connect', e, err)
      // apollo?.disconnect()
      info.status.value = Status.Error
    }
    const gotLatestBlock = (r: FetchResult<LatestBlockSubscription>) => {
      if (r.errors) {
        errHandler(r.errors)
        return
      }
      const headHeight = r.data?.block.at(0)?.height || 0
      info.height.value = headHeight
      if (info.status.value != Status.Different) {
        if (Math.abs(headHeight - currentBlockHeight.value) < CLOSE_ENOUGH_THRESHOLD) {
          info.status.value = Status.Sync
        } else {
          info.status.value = Status.Desync
        }
      }
    }
    squidBlockchainInfo(apollo, gotGenesisHash, gotLatestBlock, errHandler)
  }
}

function statusStr(status: Status): string {
  return Status[status].toLowerCase()
}
// return network name if known genesis
function getGenesis(hash: string | null): string {
  if (hash) {
    return KNOWN_GENESIS.get(hash) || hash.substring(0, 10)
  }
  return 'unknown'
}
function toolTip(status: Status): string {
  switch (status) {
    case Status.Desync:
      return 'same network but desynchronized compared to current node'
    case Status.Different:
      return 'node is on a different network'
    case Status.Error:
      return 'can not connect to this node'
    case Status.Loading:
      return 'waiting for connection'
    case Status.Sync:
      return 'this node is well synchronized with current node'
  }
}
</script>

<template>
  <section class="page">
    <h1>Network scan</h1>
    <h2>RPC endpoints</h2>
    <p>
      Current duniter endpoint <span class="mono"> {{ duniterEndpoint }}</span> <br />
      network <b>{{ getGenesis(genHash) }}</b> block <b>{{ currentBlockHeight }}</b>
    </p>
  </section>
  <table class="large squid">
    <thead>
      <tr>
        <th>endpoint</th>
        <th>genesis</th>
        <th>height</th>
        <th>hash</th>
      </tr>
    </thead>
    <tbody>
      <tr v-for="[endpoint, { status, genesis, height, hash }] in rpcInfos">
        <td :class="[statusStr(status.value)]" :title="toolTip(status.value)">
          <span v-if="endpoint == duniterEndpoint" title="current node">➡️ </span>{{ endpoint }}
        </td>
        <td>{{ getGenesis(genesis.value) }}</td>
        <td>{{ height }}</td>
        <td class="mono">{{ hash.value?.substring(0, 10) }}</td>
      </tr>
    </tbody>
  </table>

  <section class="page">
    <h2>Squid endpoints</h2>
    <p>
      Current squid endpoint <br /><span class="mono"> {{ squidEndpoint }}</span> <br />
      network <b :class="{ error: !onSameNetwork }">{{ getGenesis(squidGenHash) }}</b> block
      <b :class="{ desync: !closeEnough && !tooFar, sync: closeEnough, error: tooFar }">{{
        squidCurrentBlockHeight
      }}</b>
      <template v-if="tooFar"> (desynchronized)</template>
    </p>
  </section>
  <table class="large">
    <thead>
      <tr>
        <th>endpoint</th>
        <th>genesis</th>
        <th>height</th>
        <th>hash</th>
      </tr>
    </thead>
    <tbody>
      <tr v-for="[endpoint, { status, genesis, height, hash }] in squidInfos">
        <td :class="[statusStr(status.value)]" :title="toolTip(status.value)">
          <span v-if="endpoint == squidEndpoint" title="current node">➡️ </span>{{ endpoint }}
        </td>
        <td :class="{ error: !onSameNetwork }">{{ getGenesis(genesis.value) }}</td>
        <td>{{ height }}</td>
        <td class="mono">{{ hash.value?.substring(0, 10) }}</td>
      </tr>
    </tbody>
  </table>

  <section class="page">
    <br />
  </section>
</template>

<style scoped lang="sass">
.large
  flex: 1
  max-width: 1000px
  margin: auto
  padding: 5px

.loading
  color: gray
.sync
  color: green
.desync
  color: #b7e69e
.different
  color: orange
</style>
