import { ref, type Ref } from 'vue'

interface DistancePrecompute {
  height: number
  block: string
  referees_count: number
  member_count: number
  min_certs_for_referee: number
  results: Map<number, number>
}

// global var for distance estimate
export const distance_precompute: Ref<DistancePrecompute | null> = ref(null)

const URL = 'https://files.coinduf.eu/distance_precompute/latest_distance.json'

// not critical if it fails
fetch(URL)
  .then((v) => v.json())
  .then((v) => {
    let results = new Map<number, number>()
    for (const [key, val] of Object.entries(v.results)) {
      results.set(parseInt(key), parseInt(val as string))
    }
    const result: DistancePrecompute = {
      height: v.height,
      block: v.block,
      referees_count: v.referees_count,
      member_count: v.member_count,
      min_certs_for_referee: v.min_certs_for_referee,
      results
    }
    console.log(result)
    distance_precompute.value = result
  })
