export enum TxReason {
  issued,
  received,
  ud
}

export interface TxItem {
  type: TxReason
  address: null | string
  amount: BigInt
  timestamp: number
  blocknumber: number
  comment: null | TxComment
}

export interface TxComment {
  type: TxSource
  val: string
}

export enum TxSource {
  Ipfs,
  String
}
