// I did not find a proper ORM for localstorage, so I'm building a minimal one

import type { Ref } from 'vue'
import { removeItem } from './utils'

// storage prefix
const storagePrefix = 'duniter-vue-1_'
// storage key
export enum sk {
  defaultAccount,
  accountList,
  idtyList,
  defaultRpcEndpoint,
  userRpcEndpoint,
  rpcEndpoints,
  defaultSquidEndpoint,
  userSquidEndpoint,
  squidEndpoints,
  defaultDatapodEndpoint,
  userDatapodEndpoint,
  datapodEndpoints,
  datapodKnownPeers,
  defaultIpfsGateway,
  userIpfsGateway,
  ipfsGateways
}
// get storage key as text
function getsk(storageKey: sk) {
  return storagePrefix + sk[storageKey]
}
// get item with type
export function getItem<T>(storageKey: sk): T {
  const key = getsk(storageKey)
  const val = localStorage.getItem(key)
  if (!val) {
    throw Error('no such storage item for key')
  }
  return JSON.parse(val) as T
}
// set item with type
export function setItem<T>(storageKey: sk, val: T) {
  const key = getsk(storageKey)
  const value = JSON.stringify(val)
  localStorage.setItem(key, value)
}
// update item with given function
export function updateItem<T>(storageKey: sk, callback: (x: T) => T) {
  const item = getItem<T>(storageKey)
  const updated = callback(item)
  setItem<T>(storageKey, updated)
}

// initialize local storage with default value if not defined
function ifndefSet<T>(storageKey: sk, defValue: T) {
  if (!localStorage.getItem(getsk(storageKey))) {
    setItem(storageKey, defValue)
  }
}

// erase local storage
export function eraseLocalStorage() {
  for (const k in sk) {
    const v = sk[k]
    if (typeof v === 'string') {
      localStorage.setItem(storagePrefix + v, '')
    }
  }
}

// initialize local storage with default values if not defined
// in order to be able to complete them after
export function initLocalStorage() {
  ifndefSet(sk.defaultAccount, '')
  ifndefSet(sk.accountList, [])
  ifndefSet(sk.idtyList, [])
  ifndefSet(sk.defaultRpcEndpoint, '')
  ifndefSet(sk.userRpcEndpoint, null)
  ifndefSet(sk.rpcEndpoints, [])
  ifndefSet(sk.defaultSquidEndpoint, '')
  ifndefSet(sk.userSquidEndpoint, null)
  ifndefSet(sk.squidEndpoints, [])
  ifndefSet(sk.defaultDatapodEndpoint, '')
  ifndefSet(sk.userDatapodEndpoint, null)
  ifndefSet(sk.datapodEndpoints, [])
  ifndefSet(sk.defaultIpfsGateway, '')
  ifndefSet(sk.userIpfsGateway, null)
  ifndefSet(sk.ipfsGateways, [])
  ifndefSet(sk.datapodKnownPeers, {})
}

// add item to list
export function addToList<T>(list: Ref<T[]>, storageKey: sk, item: T) {
  if (list.value.includes(item)) {
    // nothing to do
    return
  }
  list.value.push(item)
  updateItem<T[]>(storageKey, (v) => {
    v.push(item)
    return v
  })
}

export function remFromList<T>(list: Ref<T[]>, storageKey: sk, item: T) {
  removeItem(list.value, item)
  updateItem<T[]>(storageKey, (v) => {
    return removeItem(v, item)
  })
}
