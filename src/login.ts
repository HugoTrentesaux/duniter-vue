import {
  loginAccount,
  loginIdtyId,
  loginIdtyVal,
  loginMshipVal,
  loginCertVal,
  loginIdtyName,
  loginAsSmith,
  loginAccountInfo
} from './global'
import type {
  FrameSystemAccountInfo,
  PalletCertificationIdtyCertMeta,
  PalletIdentityIdtyValue,
  SpMembershipMembershipData
} from '@polkadot/types/lookup'
import type { InjectedAccountWithMeta } from '@polkadot/extension-inject/types'
import type { ApiPromise } from '@polkadot/api'
import { type u32, Option } from '@polkadot/types-codec'
import { web3Accounts } from '@polkadot/extension-dapp'
import type { IdtyNameQuery } from '@/generated/squid'
import { getItem, setItem, sk } from './storage'
import { IdtyName } from '@/squid/wot.gql'
import { ref, type Ref } from 'vue'
import type { ApolloClient, NormalizedCacheObject } from '@apollo/client'

export const allAccounts: Ref<InjectedAccountWithMeta[]> = ref([])

// search config account in injected accounts and login with it
export async function tryConnectAccount(
  api: ApiPromise,
  apollo: ApolloClient<NormalizedCacheObject>
) {
  if (allAccounts.value.length == 0) allAccounts.value = await web3Accounts()
  const allAcc = allAccounts.value
  allAccounts.value = allAcc
  const defaultLogin = getItem<string>(sk.defaultAccount)
  const found = allAcc.find((a) => a.address == defaultLogin)
  if (found) useAccountAndFetchDetails(api, apollo, found)
}

// login with given account and fetch its details
export function useAccountAndFetchDetails(
  api: ApiPromise,
  apollo: ApolloClient<NormalizedCacheObject>,
  a: InjectedAccountWithMeta | null
) {
  loginAccount.value = a
  if (a) {
    setItem(sk.defaultAccount, a.address)
    fetchDetails(api, apollo, a.address)
  } else {
    cleanLoginAccountDetails()
  }
}

// clean global state
export function cleanLoginAccountDetails() {
  loginAccountInfo.value = null
  loginAsSmith.value = null
  loginCertVal.value = null
  loginIdtyId.value = null
  loginIdtyName.value = null
  loginIdtyVal.value = null
  loginMshipVal.value = null
}

// fetch details for given account and update login global variables
function fetchDetails(
  api: ApiPromise,
  apollo: ApolloClient<NormalizedCacheObject>,
  address: string
) {
  api.queryMulti<[FrameSystemAccountInfo, Option<u32>]>(
    [
      [api.query.system.account, address],
      [api.query.identity.identityIndexOf, address]
    ],
    ([r_accountInfo, r_idty]) => {
      loginAccountInfo.value = r_accountInfo
      const maybe_idty = r_idty
      if (maybe_idty.isSome) {
        const idty = maybe_idty.unwrap().toNumber()
        loginIdtyId.value = idty
        // query more details about identity
        api.queryMulti<
          [
            Option<PalletIdentityIdtyValue>,
            Option<SpMembershipMembershipData>,
            PalletCertificationIdtyCertMeta
          ]
        >(
          [
            [api.query.identity.identities, idty],
            [api.query.membership.membership, idty],
            [api.query.certification.storageIdtyCertMeta, idty]
          ],
          ([v, m, c]) => {
            if (v.isSome) {
              loginIdtyVal.value = v.unwrap()
            }
            if (m.isSome) {
              loginMshipVal.value = m.unwrap()
            }
            loginCertVal.value = c
          }
        )
        apollo
          .query<IdtyNameQuery>({
            query: IdtyName,
            variables: { id: idty }
          })
          .then((r) => {
            const idty = r.data.identity[0]
            loginIdtyName.value = idty.name
            loginAsSmith.value = idty.smith?.smithStatus || null
          })
      } else {
        loginIdtyId.value = null
        loginIdtyName.value = null
        loginIdtyVal.value = null
        loginAsSmith.value = null
      }
    }
  )
}
