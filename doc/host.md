# Host

An instance is available at [`ipns://duniter-vue.coinduf.eu`](https://duniter--vue-coinduf-eu.ipns.pagu.re/#/).
Hosting an instance of Duniter Panel lets you customize the default configuration.

## Build

If you do not trust build by another, do it yourself.

```sh
pnpm build
```

## Get from IPFS

If you do trust a build from someone else, look at the latest version. Some commits are tagged with `ipfs-<cid>`, which gives you the build dir for a specific commit. You can also resolve dnslink to use the version hosted at the above instance.

```sh
# resolve dnslink
$ ipfs resolve /ipns/duniter-vue.coinduf.eu
/ipfs/QmbMPj2MvArG15vH1UUN7rPCXxFXG7jL1j2s66Q2f9Rf96
# alternative technique using dig
$ dig +noall +answer _dnslink.duniter-vue.coinduf.eu TXT
_dnslink.duniter-vue.coinduf.eu. 300 IN TXT     "/ipfs/QmbMPj2MvArG15vH1UUN7rPCXxFXG7jL1j2s66Q2f9Rf96"
```

Once you have a hash, you can simply get the folder with:

```sh
$ ipfs get QmbMPj2MvArG15vH1UUN7rPCXxFXG7jL1j2s66Q2f9Rf96
```

And you can then serve this folder with any server like nginx.

## Edit default config

At the root of this folder, there is a file `config.json`. You can edit it to serve an app with custom default endpoints. You can also simply upload the modified file to IPFS and use an existing instance with `?config=QmY1bmrPrWiHRMh9SSz2WSayaUngaCi69xMvwRfT4fLePh` in URL querystring (replace the cid by yours). This allows to overwrite the instance's default config.
